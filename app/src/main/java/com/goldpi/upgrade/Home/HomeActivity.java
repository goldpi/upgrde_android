package com.goldpi.upgrade.Home;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.goldpi.materialui.R;
import com.goldpi.upgrade.DataModal.Authorization;
import com.goldpi.upgrade.PrefranceUtility.AccessTokenPrefrance;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.joanzapata.iconify.widget.IconTextView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class HomeActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RelativeLayout badgeLayout;
    private TextView itemMessagesBadgeTextView;
    private IconTextView iconButtonMessages;
    private MenuItem itemMessages;
    final Logger logger = LoggerFactory.getLogger(HomeActivity.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
       setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.drawable.upgrade_logo);
        NavigationDrawerFragment drawerFragmant;
        drawerFragmant = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.NavDrawerFragment);
        drawerFragmant.setUp(R.id.NavDrawerFragment, (DrawerLayout) findViewById(R.id.drawer), toolbar);
        CheckUser();
    }

    public void CheckUser()
    {
        Authorization SavedAuthorization= AccessTokenPrefrance.getCurrentUser(getBaseContext());
        logger.info("Access Token:-" + SavedAuthorization.getAccess_token());
        logger.info("Expired On:-" + SavedAuthorization.getExpireOn());
        logger.info("refresh Token:-" + SavedAuthorization.getRefresh_token());
        logger.info("Issued On:-" + SavedAuthorization.getIssuedOn());
        logger.info("Exipred in Second:-"+SavedAuthorization.getExpires_in());
        logger.info("Token Type" + SavedAuthorization.getToken_type());
        SimpleDateFormat df = new SimpleDateFormat("E, dd MMM HH:mm:ss z yyyy");
        String date = df.format(Calendar.getInstance().getTime());
        logger.info("Now Date "+ date);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        menu.findItem(R.id.action_Message).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_envelope)
                        .colorRes(R.color.colorWhite)
                        .actionBarSize());
        menu.findItem(R.id.action_Notification).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_bell)
                        .colorRes(R.color.colorWhite)
                        .actionBarSize());
        menu.findItem(R.id.action_Search).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_search)
                        .colorRes(R.color.colorWhite)
                        .actionBarSize());

        //http://stackoverflow.com/questions/17696486/actionbar-notification-count-badge-like-google-has
        itemMessages = menu.findItem(R.id.action_Message);
        badgeLayout = (RelativeLayout) itemMessages.getActionView();
        itemMessagesBadgeTextView = (TextView) badgeLayout.findViewById(R.id.badge_textView);
        itemMessagesBadgeTextView.setVisibility(View.VISIBLE); // initially hidden
        iconButtonMessages = (IconTextView) badgeLayout.findViewById(R.id.badge_icon_button);
        iconButtonMessages.setText("{fa-envelope}");
        iconButtonMessages.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        iconButtonMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemMessagesBadgeTextView.setVisibility(View.GONE); // initially hidden
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_Notification) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
