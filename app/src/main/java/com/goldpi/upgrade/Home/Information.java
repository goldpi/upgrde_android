package com.goldpi.upgrade.Home;

/**
 * Created by Digambar on 3/30/2016.
 */
public class Information {

   private String title;
    private String IconText;

    public String getTitle() {
        return title;
    }

    public void setTitle(String name) {
        this.title = name;
    }
    public String getIconText() {
        return IconText;
    }

    public void setIconText(String name) {
        this.IconText = name;
    }

}
