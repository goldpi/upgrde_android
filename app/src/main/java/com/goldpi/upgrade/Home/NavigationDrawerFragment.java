package com.goldpi.upgrade.Home;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goldpi.upgrade.Icon.Iconicfy;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.UIHelper.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment {

    public static  final String REF_FILE_NAME="Home";
    public static  final String KEY_USER_LEARNED_DRAWER="user_learned_drawer";
private ActionBarDrawerToggle mActionBarDrawerToggle;
    private DrawerLayout    mDrawerLayout;
    private boolean mUserLearnDrawer;
    private boolean mFromSaveInstanceState;
    private View containerView;
    RecyclerView navRecyclerview;
    private com.goldpi.upgrade.Home.DataAdapter adapter;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
     mUserLearnDrawer=Boolean.valueOf(readTFromPrefrances(getActivity(),KEY_USER_LEARNED_DRAWER,"false"));
        if(savedInstanceState!=null)
        {
            mFromSaveInstanceState=true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Iconicfy.SetIconify();
        //recyclerview to add menu item
        View NavdrawerLayout=inflater.inflate(R.layout.fragment_navigation_drawer,container,false);
        navRecyclerview = (RecyclerView) NavdrawerLayout.findViewById(R.id.NavDrawerRecyclerView);


        adapter=new DataAdapter(getActivity(),getData());
        navRecyclerview.setAdapter(adapter);
        navRecyclerview.addItemDecoration(
                new DividerItemDecoration(getActivity(), null));
        navRecyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        // Inflate the layout for this fragment
        return NavdrawerLayout;
    }
    public static List<Information> getData()
    {
        List<Information> data=new ArrayList<>();

        String[]titles={"Daily Current Affairs","Daily Pocket Lerner(DPL)","Online School Support","Aptitude Building"," Offline Worksheet","Visual Scroll Board","Innovative Dispay Board"};
        String[]IconText={"fa-newspaper-o","fa-leanpub","fa-bullhorn","fa-building","fa-child","fa-cube","fa-cutlery","fa-cloud"};
        for(int i=0;i<titles.length && i<IconText.length;i++)
        {

            Information current;
            current = new Information();
            current.setIconText(IconText[i]);
            current.setTitle(titles[i]);
            data.add(current);

        }

        return  data;

    }
    public  void setUp(int fragmentID, final DrawerLayout drawerLayout,Toolbar toolbar)
    {
        containerView =getActivity().findViewById(fragmentID);
        mDrawerLayout=drawerLayout;
        mActionBarDrawerToggle=
                new ActionBarDrawerToggle(getActivity(),drawerLayout,toolbar, R.string.OpenDrawer,R.string.CloseDrawer)
                {
                    @Override
                    public void onDrawerOpened(View drawerView) {
                           super.onDrawerOpened(drawerView);
                        if(!mUserLearnDrawer)
                        {
                            mUserLearnDrawer=true;
                            saveToPrefrances(getActivity(),KEY_USER_LEARNED_DRAWER,mUserLearnDrawer+"");
                        }

                        getActivity().invalidateOptionsMenu();
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {

                        super.onDrawerClosed(drawerView);

                        getActivity().invalidateOptionsMenu();
                    }
                };

        if(!mUserLearnDrawer && !mFromSaveInstanceState)
            mDrawerLayout.openDrawer(containerView);
        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle );
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                    mActionBarDrawerToggle.syncState();
            }
        });
    }

    public static void saveToPrefrances(Context context, String PrefranceName, String PrefreanceValue)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(REF_FILE_NAME,context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PrefranceName,PrefreanceValue);
        editor.apply();
    }

    public static String readTFromPrefrances(Context context, String PrefranceName, String defaultValue)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(REF_FILE_NAME, context.MODE_PRIVATE);

      return   sharedPreferences.getString(PrefranceName, defaultValue);

    }

}
