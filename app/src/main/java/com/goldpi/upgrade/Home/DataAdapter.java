package com.goldpi.upgrade.Home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.goldpi.materialui.R;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.EntypoModule;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.joanzapata.iconify.fonts.IoniconsModule;
import com.joanzapata.iconify.fonts.MaterialCommunityModule;
import com.joanzapata.iconify.fonts.MaterialModule;
import com.joanzapata.iconify.fonts.MeteoconsModule;
import com.joanzapata.iconify.fonts.SimpleLineIconsModule;
import com.joanzapata.iconify.fonts.TypiconsModule;
import com.joanzapata.iconify.fonts.WeathericonsModule;
import com.joanzapata.iconify.widget.IconTextView;

import java.util.Collections;
import java.util.List;

/**
 * Created by Digambar on 3/30/2016.
 */
public class DataAdapter extends RecyclerView.Adapter<DataAdapter.MyViewHolder> {
    private  LayoutInflater inflator;
    List<Information> data= Collections.emptyList();
    View view;
    public DataAdapter(Context context,List<Information> data)
    {
        Iconify
                .with(new FontAwesomeModule())
                .with(new EntypoModule())
                .with(new TypiconsModule())
                .with(new MaterialModule())
                .with(new MaterialCommunityModule())
                .with(new MeteoconsModule())
                .with(new WeathericonsModule())
                .with(new SimpleLineIconsModule())
                .with(new IoniconsModule());
        inflator=LayoutInflater.from(context);
        this.data=data;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
       view =inflator.inflate(R.layout.custom_row,parent,false);
        Log.d("Yusuf",view.toString());
        MyViewHolder holder=new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        Information current=data.get(position);
        holder.title.setText(current.getTitle());
        holder.Icon.setText("{"+current.getIconText()+"}");
        //holder.icon.setImageResource(current.iconId);

    }

    @Override
    public int getItemCount()
    {
        return data.size();
    }
    class MyViewHolder extends RecyclerView.ViewHolder
    {
     protected TextView title;
     protected IconTextView Icon;
        //ImageView icon;

        public MyViewHolder(View itemView) {
            super(itemView);
            title= (TextView) itemView.findViewById(R.id.listText);
            Icon= (IconTextView) itemView.findViewById(R.id.listIcon);
            //icon= (ImageView) itemView.findViewById(R.id.listIcon);
        }
    }
}
