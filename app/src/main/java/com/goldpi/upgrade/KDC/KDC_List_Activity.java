package com.goldpi.upgrade.KDC;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.goldpi.materialui.R;
import com.goldpi.upgrade.Landing.News.NewsModal;
import com.goldpi.upgrade.Listner.IOnRecyclerItemClickLister;
import com.goldpi.upgrade.UIHelper.DividerItemDecoration;
import com.goldpi.upgrade.UpgradeApplication;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;

import java.util.ArrayList;

public class KDC_List_Activity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener, IOnRecyclerItemClickLister {

    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private RecyclerView recycler_view_KDC;
    private YouTubePlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kdc__list_);
        recycler_view_KDC = (RecyclerView) findViewById(R.id.recycler_view_KDC_List);
        KDCAdapter adapter = new KDCAdapter(UpgradeApplication.getAppContext());
        adapter.setNews(UpgradeApplication.getWritableDatabase().readNews(), this);
        recycler_view_KDC.addItemDecoration(
                new DividerItemDecoration(this, R.drawable.itemdivider));
        recycler_view_KDC.setLayoutManager(new LinearLayoutManager(this));
        recycler_view_KDC.setAdapter(adapter);
        getYouTubePlayerProvider().initialize(DeveloperKey.DEVELOPER_KEY, this);
    }


    public void setupPlayer() {
        YouTubePlayerFragment youTubePlayerFragment =
                (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
        youTubePlayerFragment.initialize(DeveloperKey.DEVELOPER_KEY, this);

    }

    // get the item when click on kdc list
    @Override
    public void onItemClick(View view, int position) {
        TextView txtKDCID = (TextView) view.findViewById(R.id.KDCId);

        Toast.makeText(KDC_List_Activity.this, "KDC ID:-" + txtKDCID.getText(), Toast.LENGTH_SHORT).show();
        player.cueVideo(txtKDCID.getText().toString());
        player.play();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        if (!wasRestored) {
            youTubePlayer.cueVideo("nCgQDjiotG0");
            this.player = youTubePlayer;
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(getString(R.string.error_player), errorReason.toString());
            Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(DeveloperKey.DEVELOPER_KEY, this);
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
    }
}
