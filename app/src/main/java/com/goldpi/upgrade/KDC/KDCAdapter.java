package com.goldpi.upgrade.KDC;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Landing.Activity.VerticleSwipActivity;
import com.goldpi.upgrade.Landing.News.NewsModal;
import com.goldpi.upgrade.Listner.IOnRecyclerItemClickLister;
import com.goldpi.upgrade.Network.VolleySingleton;
import com.goldpi.upgrade.Utility.IConstants;
import com.goldpi.upgrade.Utility.StringTrimer;
import com.goldpi.upgrade.anim.AnimationUtils;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class KDCAdapter extends RecyclerView.Adapter<KDCAdapter.MyViewHolder> {
    final static Logger logger = LoggerFactory.getLogger(KDCAdapter.class);
    private static ArrayList<NewsModal> dataSet;
    private  LayoutInflater inflator;
    private View view;
    private int mPreviousPosition=0;
    private  Map<YouTubeThumbnailView, YouTubeThumbnailLoader> thumbnailViewToLoaderMap;
   private IOnRecyclerItemClickLister recyclerItemClickLister;
    private VolleySingleton mVolleySingleton;
    private ImageLoader mImageLoader;
    public KDCAdapter(Context contex) {
        thumbnailViewToLoaderMap = new HashMap<YouTubeThumbnailView, YouTubeThumbnailLoader>();
        inflator=LayoutInflater.from(contex);
        mVolleySingleton = VolleySingleton.getInstance();
        mImageLoader = mVolleySingleton.getImageLoader();
    }

    public void setNews(ArrayList<NewsModal> data,IOnRecyclerItemClickLister lister) {
        this.recyclerItemClickLister=lister;
        this.dataSet = data;
        //update the adapter to reflect the new set of news
        notifyDataSetChanged();
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {

        view =inflator.inflate(R.layout.kdcitem,parent,false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        holder.textKDCTitle.setText(dataSet.get(listPosition).getTitle());
        holder.textKDCDesc.setText(StringTrimer.Trim(dataSet.get(listPosition).getContent(), 50));
       // holder.imageViewIcon.setImageResource(R.drawable.profile_cover);
        holder.txtKDCId.setText("blB_X38YSxQ");

        YouTubeThumbnailLoader loader = thumbnailViewToLoaderMap.get(holder.Thumbnail);
        if (loader == null) {
            // 2) The view is already created, and is currently being initialized. We store the
            //    current videoId in the tag.
            holder.Thumbnail.setTag("blB_X38YSxQ");

        } else {
            // 3) The view is already created and already initialized. Simply set the right videoId
            //    on the loader.
            holder.Thumbnail.setImageResource(R.drawable.loading_thumbnail);
            loader.setVideo("blB_X38YSxQ");
        }
        //String ThumbURL=dataSet.get(listPosition).getImageUrl();
        //loadImages(ThumbURL,holder);
       // DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
       // holder.txtPostedOn.setText(dateFormat.format(dataSet.get(listPosition).getPostedOn()));
        holder.SetOnRecyclerItemClickListner(recyclerItemClickLister);
        if (listPosition > mPreviousPosition) {
            AnimationUtils.animateSunblind(holder, true);
        } else {
            AnimationUtils.animateSunblind(holder, false);
        }
        mPreviousPosition = listPosition;

    }
    private void loadImages(String urlThumbnail, final MyViewHolder holder) {
        if (!urlThumbnail.equals(IConstants.NA)) {
            mImageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    //holder.Thumbnail.setImageBitmap(response.getBitmap());
                    logger.info("Loading Image:-"+response.getRequestUrl());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.error(error.toString());
                }


            });
        }
    }
    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context mContext;

        //TextView txtPostedOn;
        TextView textKDCTitle;
        TextView textKDCDesc;
        TextView txtKDCId;
        IOnRecyclerItemClickLister miOnRecyclerItemClickLister;
        private YouTubeThumbnailView Thumbnail;
        public MyViewHolder(View itemView) {
            super(itemView);
            mContext=itemView.getContext();
            this.textKDCTitle = (TextView) itemView.findViewById(R.id.txtfragmentoneKDCTitle);
            this.textKDCDesc =(TextView)itemView.findViewById(R.id.txtfragmentoneKDCDescription);
            //this.txtPostedOn=(TextView)itemView.findViewById(R.id.txtfragmentoneNewsPostedOn);
            this.txtKDCId =(TextView)itemView.findViewById(R.id.KDCId);
            this.Thumbnail=(YouTubeThumbnailView)itemView.findViewById(R.id.thumbnail);
            itemView.setOnClickListener(this);
        }
public void SetOnRecyclerItemClickListner(IOnRecyclerItemClickLister _listner)
{
    this.miOnRecyclerItemClickLister=_listner;
}
        @Override
        public void onClick(View v) {
            //TextView KDCId=(TextView) v.findViewById(R.id.KDCId);
            miOnRecyclerItemClickLister.onItemClick(v,getPosition());

        }
    }
}
