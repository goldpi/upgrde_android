package com.goldpi.upgrade.UIHelper;

import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;

/**
 * Created by Digambar on 5/16/2016.
 */
public class myWebChromeClient extends WebChromeClient {

    private Bitmap mDefaultVideoPoster;
    private View mVideoProgressView;
    private FrameLayout customViewContainer;
    private View mCustomView;
    private WebView myWebView;
    private WebChromeClient.CustomViewCallback customViewCallback;
    @Override
    public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
        onShowCustomView(view, callback);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public void onShowCustomView(View view,CustomViewCallback callback) {

        // if a view already exists then immediately terminate the new one
        if (mCustomView != null) {
            callback.onCustomViewHidden();
            return;
        }
        mCustomView = view;
        myWebView.setVisibility(View.GONE);
        customViewContainer.setVisibility(View.VISIBLE);
        customViewContainer.addView(view);
        customViewCallback = callback;
    }

    @Override
    public View getVideoLoadingProgressView() {

        if (mVideoProgressView == null) {
       //   LayoutInflater inflater = LayoutInflater.from(KDCActivity.this);
          //  mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
        }
        return mVideoProgressView;
    }

    @Override
    public void onHideCustomView() {
        super.onHideCustomView();    //To change body of overridden methods use File | Settings | File Templates.
        if (mCustomView == null)
            return;

        myWebView.setVisibility(View.VISIBLE);
        customViewContainer.setVisibility(View.GONE);

        // Hide the custom view.
        mCustomView.setVisibility(View.GONE);

        // Remove the custom view from its container.
       customViewContainer.removeView(mCustomView);

        //customViewCallback.onCustomViewHidden();

        mCustomView = null;
    }
}
