package com.goldpi.upgrade.UIHelper;

import com.goldpi.upgrade.Landing.HomeSlider.SlicesModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Digambar on 5/25/2016.
 */
public class Shortner {

    public static  ArrayList<SlicesModel> SortAssending(ArrayList<SlicesModel> data)
    {
        Collections.sort(data, new Comparator<SlicesModel>() {
            @Override
            public int compare(SlicesModel lhs, SlicesModel rhs) {
                int SlidingOrderLhs = lhs.getSlidingOrder();
                int SlidingOrderrhs = rhs.getSlidingOrder();
                if (SlidingOrderLhs > SlidingOrderrhs) {
                    return 1;

                } else if (SlidingOrderLhs < SlidingOrderrhs) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });

           return data;
        }
    public static ArrayList<SlicesModel>SortDescending(ArrayList<SlicesModel> data)
    {

        Collections.sort(data, new Comparator<SlicesModel>() {
            @Override
            public int compare(SlicesModel lhs, SlicesModel rhs) {
                int SlidingOrderLhs = lhs.getSlidingOrder();
                int SlidingOrderrhs = rhs.getSlidingOrder();
                if (SlidingOrderLhs < SlidingOrderrhs) {
                    return 1;

                } else if (SlidingOrderLhs > SlidingOrderrhs) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });

        return data;
    }
    }



