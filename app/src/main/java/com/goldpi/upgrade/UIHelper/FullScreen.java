package com.goldpi.upgrade.UIHelper;

import android.os.Build;
import android.content.Context;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;


 /**
 * Created by Yusuf on 5/19/2016.
 */
public class FullScreen {

    public static void Show(AppCompatActivity mContext)
    {

        if (Build.VERSION.SDK_INT < 16) {

            mContext.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        }
        else {
            View decorView = mContext.getWindow().getDecorView();
            // Show Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_VISIBLE;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    public static void Hide(AppCompatActivity mContext)
    {

        // Hide Status Bar
        if (Build.VERSION.SDK_INT < 16) {
            mContext.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = mContext.getWindow().getDecorView();
            // Hide Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }
}
