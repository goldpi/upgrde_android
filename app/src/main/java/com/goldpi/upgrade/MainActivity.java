package com.goldpi.upgrade;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.daimajia.easing.Glider;
import com.daimajia.easing.Skill;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Icon.Iconicfy;

import com.goldpi.upgrade.KDC.KDC_List_Activity;
import com.goldpi.upgrade.KDC.VideoListDemoActivity;
import com.goldpi.upgrade.Landing.Activity.TabActivity;
import com.goldpi.upgrade.Landing.Articles.ArticleLoader;
import com.goldpi.upgrade.Landing.HomeSlider.SliderLoader;
import com.goldpi.upgrade.Landing.News.NewsLoader;
import com.goldpi.upgrade.Landing.ProductCategory.ProductCategoryLoader;
import com.goldpi.upgrade.Landing.Products.ProductLoader;
import com.goldpi.upgrade.Landing.RoleModel.RoleModelLoader;
import com.goldpi.upgrade.Network.VolleySingleton;
import com.goldpi.upgrade.Service.NotifyService;
import com.goldpi.upgrade.Utility.Utils;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.joanzapata.iconify.widget.IconButton;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private Toolbar toolbar;
    private IconButton btn;

    public MainActivity getInstance() {
        return this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        Iconicfy.SetIconify(); // to enable fa(fontawsome Icone)
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                Glider.glide(Skill.ExpoEaseIn, 1200, ObjectAnimator.ofFloat(findViewById(R.id.Logo), "translationY", 1000, 0))

        );
        //TODO check internet connection before execution the task

        if(UpgradeApplication.isNetworkAvailable())
        new LoadServerData("kl").execute();
        else
            Toast.makeText(MainActivity.this, "Please enable your internet connection.", Toast.LENGTH_SHORT).show();
        set.setDuration(1200);
        set.start();


        try {
            startService(new Intent(MainActivity.this, NotifyService.class));
        } catch (Exception ex) {

        }

        Utils.FullScreen(this);

    }

    public void onClick(View v) {
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        menu.findItem(R.id.action_Message).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_envelope)
                        .colorRes(R.color.colorPrimaryDark)
                        .actionBarSize());
        menu.findItem(R.id.action_Notification).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_bell)
                        .colorRes(R.color.colorPrimaryDark)
                        .actionBarSize());
        menu.findItem(R.id.action_Search).setIcon(
                new IconDrawable(this, FontAwesomeIcons.fa_search)
                        .colorRes(R.color.colorPrimaryDark)
                        .actionBarSize());


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_Notification) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    class LoadServerData extends AsyncTask<Void, Void, String> {

        LoadServerData(String data) {

        }

        @Override
        protected String doInBackground(Void... params) {
            SliderLoader.loadSlides(requestQueue);
            NewsLoader.loadNews(requestQueue);
            ProductCategoryLoader.loadProductCategory(requestQueue);
            ArticleLoader.loadArticle(requestQueue);
            ProductLoader.loadProduct(requestQueue);
            RoleModelLoader.loadRoleModel(requestQueue);

            return "Done";

        }

        @Override
        protected void onPostExecute(String aVoid) {
            getInstance().finish();
            Intent Login = new Intent(getBaseContext(), TabActivity.class);
           // Intent  Login=new Intent(getBaseContext(), KDC_List_Activity.class);
            //Intent  Login=new Intent(getBaseContext(),TabActivity.class);
            //Intent  Login=new Intent(getBaseContext(), KDCActivity.class);
            //Intent  Login=new Inten//t(getBaseContext(),VerticleSwipActivity.class);
            //Intent  Login=new Intent(getBaseContext(),TabActivity.class);
            //Intent  Login=new Intent(getBaseContext(),VerticleSwipActivity.class);
            //Intent page=new Intent(getBaseContext(), Viewfilpper.class);
            //Intent  Login=new Intent(getBaseContext(), ScienceProjectActivity.class);
            //Intent  Login=new Intent(getBaseContext(),NavSliderActivity.class);
            startActivity(Login);

        }
    }
}