package com.goldpi.upgrade.DB;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.goldpi.upgrade.Account.Registration.School.SchoolModel;
import com.goldpi.upgrade.Landing.Articles.ArticleModal;
import com.goldpi.upgrade.Landing.News.NewsModal;
import com.goldpi.upgrade.Landing.ProductCategory.ProductCategoryModel;
import com.goldpi.upgrade.Landing.Products.ProductModal;
import com.goldpi.upgrade.Landing.RoleModel.RoleModelModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Yusuf on 4/29/2016.
 */
public class DatabaseContext {

    final Logger logger = LoggerFactory.getLogger(DatabaseContext.class);
    private UpgradeDBHelper mHelper;
    private SQLiteDatabase mDatabase;

    public DatabaseContext(Context context) {
        mHelper = new UpgradeDBHelper(context);
        mDatabase = mHelper.getWritableDatabase();
    }

    //region ==================NEWS SECTION=========================================
    public void insertNews(ArrayList<NewsModal> NewsList, boolean clearPrevious) {
        if (clearPrevious) {
            deleteNews();
        }


        //create a sql prepared statement
        String sql = "INSERT INTO " + UpgradeDBHelper.TABLE_News + " VALUES (?,?,?,?,?,?);";
        //compile the statement and start a transaction
        SQLiteStatement statement = mDatabase.compileStatement(sql);
        mDatabase.beginTransaction();
        for (int i = 0; i < NewsList.size(); i++) {
            NewsModal currentNews = NewsList.get(i);
            statement.clearBindings();
            //for a given column index, simply bind the data to be put inside that index

            statement.bindString(1, Integer.toString(currentNews.getId()));
            statement.bindString(2, currentNews.getTitle());
            statement.bindString(3, currentNews.getImageUrl());
            statement.bindString(4, currentNews.getVideoUrl());
            statement.bindString(5, currentNews.getContent());
            statement.bindLong(6, currentNews.getPostedOn() == null ? -1 : currentNews.getPostedOn().getTime());
            statement.execute();
        }
        //set the transaction as successful and end the transaction
        logger.info("inserting entries " + NewsList.size() + new Date(System.currentTimeMillis()));
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();
    }

    public ArrayList<NewsModal> readNews() {
        ArrayList<NewsModal> listMovies = new ArrayList<>();

        //get a list of columns to be retrieved, we need all of them
        String[] columns = {UpgradeDBHelper.COLUMN_Id,
                UpgradeDBHelper.COLUMN_Description,
                UpgradeDBHelper.COLUMN_Title,
                UpgradeDBHelper.COLUMN_ImageThumb,
                UpgradeDBHelper.COLUMN_VideoUrl,
                UpgradeDBHelper.COLUMN_PostedOn
        };
        Cursor cursor = mDatabase.query(UpgradeDBHelper.TABLE_News, columns, null, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {

            logger.info(("loading entries " + cursor.getCount() + new Date(System.currentTimeMillis())));
            do {

                //create a new movie object and retrieve the data from the cursor to be stored in this movie object
                NewsModal news = new NewsModal();
                //each step is a 2 part process, find the index of the column first, find the data of that column using
                //that index and finally set our blank movie object to contain our data
                news.setId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Id)));
                news.setTitle(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Title)));
                long releaseDateMilliseconds = cursor.getLong(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_PostedOn));
                news.setPostedOn(releaseDateMilliseconds != -1 ? new Date(releaseDateMilliseconds) : null);
                news.setContent(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Description)));
                news.setImageUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_ImageThumb)));
                news.setVideoUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_VideoUrl)));
                //add the movie to the list of movie objects which we plan to return
                listMovies.add(news);
            }
            while (cursor.moveToNext());
        }
        return listMovies;
    }

    public NewsModal getSingleNews(int id) {
        NewsModal news = new NewsModal();

        String query = "SELECT * FROM " + UpgradeDBHelper.TABLE_News + " WHERE " + UpgradeDBHelper.COLUMN_Id + "=" + id + "";
        Cursor cursor = mDatabase.rawQuery(query, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        news.setId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Id)));
        news.setId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Id)));
        news.setTitle(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Title)));
        long releaseDateMilliseconds = cursor.getLong(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_PostedOn));
        news.setPostedOn(releaseDateMilliseconds != -1 ? new Date(releaseDateMilliseconds) : null);
        news.setContent(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Description)));
        news.setImageUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_ImageThumb)));
        news.setVideoUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_VideoUrl)));
        return news;
    }

    public void deleteNews() {
        mDatabase.delete(UpgradeDBHelper.TABLE_News, null, null);

    }

    public int getNewsCount() {

        int numberOfRows = (int) DatabaseUtils.queryNumEntries(mDatabase, UpgradeDBHelper.TABLE_News);
        return numberOfRows;
    }

    //endregion
    //region ==================PRODUCT SECTION=============================================

    public void insertProduct(ArrayList<ProductModal> ProductList, boolean clearPrevious) {
        if (clearPrevious) {
            deleteProduct();
        }


        //create a sql prepared statement
        String sql = "INSERT INTO " + UpgradeDBHelper.TABLE_Product + " VALUES (?,?,?,?,?,?,?,?,?);";
        //compile the statement and start a transaction
        SQLiteStatement statement = mDatabase.compileStatement(sql);
        mDatabase.beginTransaction();
        for (int i = 0; i < ProductList.size(); i++) {
            ProductModal currentproduct = ProductList.get(i);
            statement.clearBindings();
            //for a given column index, simply bind the data to be put inside that index

            statement.bindString(1, Integer.toString(currentproduct.getId()));
            statement.bindString(2, currentproduct.getName());
            statement.bindString(3, currentproduct.getImageUrl());
            statement.bindString(4, currentproduct.getVideoUrl());
            statement.bindString(5, currentproduct.getLongDesc());
            statement.bindString(6, currentproduct.getShortDesc());
            statement.bindString(7, Double.toString(currentproduct.getPrice()));
            statement.bindLong(8, currentproduct.getAddedon() == null ? -1 : currentproduct.getAddedon().getTime());
            statement.bindString(9, Integer.toString(currentproduct.getCategoryId()));
            statement.execute();


        }
        //set the transaction as successful and end the transaction
        logger.info("inserting entries " + ProductList.size() + new Date(System.currentTimeMillis()));
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();
    }


    public ArrayList<ProductModal> readProduct() {
        ArrayList<ProductModal> listProduct = new ArrayList<>();

        //get a list of columns to be retrieved, we need all of them
        String[] columns = {UpgradeDBHelper.COLUMN_Id,
                UpgradeDBHelper.COLUMN_NAME,
                UpgradeDBHelper.COLUMN_ImageThumb,
                UpgradeDBHelper.COLUMN_VideoUrl,
                UpgradeDBHelper.COLUMN_Description,
                UpgradeDBHelper.COLUMN_ShortDescription,
                UpgradeDBHelper.COLUMN_Price,
                UpgradeDBHelper.COLUMN_PostedOn,
                UpgradeDBHelper.COLUMN_CategoryID
        };
        Cursor cursor = mDatabase.query(UpgradeDBHelper.TABLE_Product, columns, null, null, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {

            logger.info(("loading entries " + cursor.getCount() + new Date(System.currentTimeMillis())));
            do {

                //create a new movie object and retrieve the data from the cursor to be stored in this movie object
                ProductModal productModal = new ProductModal();
                //each step is a 2 part process, find the index of the column first, find the data of that column using
                //that index and finally set our blank movie object to contain our data
                productModal.setId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Id)));
                productModal.setName(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_NAME)));
                productModal.setImageUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_ImageThumb)));
                productModal.setVideoUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_VideoUrl)));
                productModal.setLongDesc(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Description)));
                productModal.setShortDesc(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_ShortDescription)));
                productModal.setPrice(cursor.getDouble(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Price)));
                long releaseDateMilliseconds = cursor.getLong(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_PostedOn));
                productModal.setAddedon(releaseDateMilliseconds != -1 ? new Date(releaseDateMilliseconds) : null);
                productModal.setCategoryId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_CategoryID)));
                //add the movie to the list of movie objects which we plan to return
                listProduct.add(productModal);
            }
            while (cursor.moveToNext());
        }
        return listProduct;
    }

    public ArrayList<ProductModal> readProductbyCategory(int CategoryID) {
        ArrayList<ProductModal> listProduct = new ArrayList<>();

        //get a list of columns to be retrieved, we need all of them
     /*   String[] columns = {UpgradeDBHelper.COLUMN_Id,
                UpgradeDBHelper.COLUMN_NAME,
                UpgradeDBHelper.COLUMN_ImageThumb,
                UpgradeDBHelper.COLUMN_VideoUrl,
                UpgradeDBHelper.COLUMN_Description,
                UpgradeDBHelper.COLUMN_ShortDescription,
                UpgradeDBHelper.COLUMN_Price,
                UpgradeDBHelper.COLUMN_PostedOn,
                UpgradeDBHelper.COLUMN_CategoryID
        };
        Cursor cursor = mDatabase.query(UpgradeDBHelper.TABLE_Product, columns, null, null, null, null, null, null);
       */
        String query = "select * from " + UpgradeDBHelper.TABLE_Product + " where " + UpgradeDBHelper.COLUMN_CategoryID + "=" + CategoryID + "";
        Cursor cursor = mDatabase.rawQuery(query, null);
        if (cursor != null && cursor.moveToFirst()) {

            logger.info(("loading entries " + cursor.getCount() + new Date(System.currentTimeMillis())));
            do {

                //create a new movie object and retrieve the data from the cursor to be stored in this movie object
                ProductModal productModal = new ProductModal();
                //each step is a 2 part process, find the index of the column first, find the data of that column using
                //that index and finally set our blank movie object to contain our data
                productModal.setId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Id)));
                productModal.setName(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_NAME)));
                productModal.setImageUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_ImageThumb)));
                productModal.setVideoUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_VideoUrl)));
                productModal.setLongDesc(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Description)));
                productModal.setShortDesc(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_ShortDescription)));
                productModal.setPrice(cursor.getDouble(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Price)));
                long releaseDateMilliseconds = cursor.getLong(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_PostedOn));
                productModal.setAddedon(releaseDateMilliseconds != -1 ? new Date(releaseDateMilliseconds) : null);
                productModal.setCategoryId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_CategoryID)));
                //add the movie to the list of movie objects which we plan to return
                listProduct.add(productModal);
            }
            while (cursor.moveToNext());
        }
        return listProduct;
    }
    public void deleteProduct() {
        mDatabase.delete(UpgradeDBHelper.TABLE_Product, null, null);

    }
    //endregion
    //region ==================ROLE MODAL SECTION=========================================


    public void insertRoleModel(ArrayList<RoleModelModel> RoleModelList, boolean clearPrevious) {
        if (clearPrevious) {
            deleteRoleModel();
        }


        //create a sql prepared statement
        String sql = "INSERT INTO " + UpgradeDBHelper.TABLE_RoleModal + " VALUES (?,?,?,?,?,?);";
        //compile the statement and start a transaction
        SQLiteStatement statement = mDatabase.compileStatement(sql);
        mDatabase.beginTransaction();
        for (int i = 0; i < RoleModelList.size(); i++) {
            RoleModelModel RM = RoleModelList.get(i);
            statement.clearBindings();
            //for a given column index, simply bind the data to be put inside that index

            statement.bindString(1, Integer.toString(RM.getId()));
            statement.bindString(2, RM.getTitle());
            statement.bindString(3, RM.getImageUrl());
            statement.bindString(4, RM.getVideoUrl());
            statement.bindString(5, RM.getContent());
            statement.bindLong(6, RM.getPostedOn() == null ? -1 : RM.getPostedOn().getTime());
            statement.execute();
        }
        //set the transaction as successful and end the transaction
        logger.info("inserting entries " + RoleModelList.size() + new Date(System.currentTimeMillis()));
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();
    }

    public ArrayList<RoleModelModel> readRoleModel() {
        ArrayList<RoleModelModel> listRoleModel = new ArrayList<>();

        //get a list of columns to be retrieved, we need all of them
        String[] columns = {UpgradeDBHelper.COLUMN_Id,
                UpgradeDBHelper.COLUMN_Description,
                UpgradeDBHelper.COLUMN_Title,
                UpgradeDBHelper.COLUMN_ImageThumb,
                UpgradeDBHelper.COLUMN_VideoUrl,
                UpgradeDBHelper.COLUMN_PostedOn
        };
        Cursor cursor = mDatabase.query(UpgradeDBHelper.TABLE_RoleModal, columns, null, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {

            logger.info(("loading entries " + cursor.getCount() + new Date(System.currentTimeMillis())));
            do {

                //create a new movie object and retrieve the data from the cursor to be stored in this movie object
                RoleModelModel RM = new RoleModelModel();
                //each step is a 2 part process, find the index of the column first, find the data of that column using
                //that index and finally set our blank movie object to contain our data
                RM.setId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Id)));
                RM.setTitle(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Title)));
                long releaseDateMilliseconds = cursor.getLong(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_PostedOn));
                RM.setPostedOn(releaseDateMilliseconds != -1 ? new Date(releaseDateMilliseconds) : null);
                RM.setContent(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Description)));
                RM.setImageUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_ImageThumb)));
                RM.setVideoUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_VideoUrl)));
                //add the movie to the list of movie objects which we plan to return
                listRoleModel.add(RM);
            }
            while (cursor.moveToNext());
        }
        return listRoleModel;
    }

    public RoleModelModel getSingleRoleModel(int id) {
        RoleModelModel RoleModel = new RoleModelModel();

        String query = "SELECT * FROM " + UpgradeDBHelper.TABLE_RoleModal + " WHERE " + UpgradeDBHelper.COLUMN_Id + "=" + id + "";
        Cursor cursor = mDatabase.rawQuery(query, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        RoleModel.setId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Id)));
        RoleModel.setId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Id)));
        RoleModel.setTitle(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Title)));
        long releaseDateMilliseconds = cursor.getLong(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_PostedOn));
        RoleModel.setPostedOn(releaseDateMilliseconds != -1 ? new Date(releaseDateMilliseconds) : null);
        RoleModel.setContent(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Description)));
        RoleModel.setImageUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_ImageThumb)));
        RoleModel.setVideoUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_VideoUrl)));
        return RoleModel;
    }

    public void deleteRoleModel() {
        mDatabase.delete(UpgradeDBHelper.TABLE_RoleModal, null, null);

    }

    //endregion
    //region==================ARTICLE SECTION============================================

    // ==================inserting values into Article table===========
    public void insertArticles(ArrayList<ArticleModal> arrayArticles, boolean clearprevious) {
        if (clearprevious) {
            deleteArticles();
        }

        // prepare statement for insert command
        String sql = "insert into " + UpgradeDBHelper.TABLE_Article + " values (?,?,?,?,?,?);";
        //compile kthe statement and start the transaction
        SQLiteStatement Statement = mDatabase.compileStatement(sql);
        mDatabase.beginTransaction();
        for (int i = 0; i < arrayArticles.size(); i++) {
            ArticleModal currentArticle = arrayArticles.get(i);
            Statement.clearBindings();
            Statement.bindString(1, Integer.toString(currentArticle.getId()));
            Statement.bindString(2, currentArticle.getTitle());
            Statement.bindString(3, currentArticle.getContent());
            Statement.bindString(4, currentArticle.getImageUrl());
            Statement.bindString(5, currentArticle.getVideoUrl());
            Statement.bindLong(6, currentArticle.getPostedOn() == null ? -1 : currentArticle.getPostedOn().getTime());
            Statement.execute();
        }
        logger.info("inserting Articles" + arrayArticles.size() + new Date(System.currentTimeMillis()));
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();

    }

    public ArrayList<ArticleModal> readArticles() {
        ArrayList<ArticleModal> listArticles = new ArrayList<>();
        String[] columns = {
                UpgradeDBHelper.COLUMN_Id,
                UpgradeDBHelper.COLUMN_Title,
                UpgradeDBHelper.COLUMN_Description,
                UpgradeDBHelper.COLUMN_ImageThumb,
                UpgradeDBHelper.COLUMN_VideoUrl,
                UpgradeDBHelper.COLUMN_PostedOn
        };

        Cursor cursor = mDatabase.query(UpgradeDBHelper.TABLE_Article, columns, null, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            // logger.info(("loading entries " + cursor.getCount() + new Date(System.currentTimeMillis())));
            do {

                //create a new Article object and retrieve the data from the cursor to be stored in this movie object
                ArticleModal articleModal = new ArticleModal();
                //each step is a 2 part process, find the index of the column first, find the data of that column using
                //that index and finally set our blank movie object to contain our data
                articleModal.setId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Id)));
                articleModal.setTitle(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Title)));
                long releaseDateMilliseconds = cursor.getLong(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_PostedOn));
                articleModal.setPostedOn(releaseDateMilliseconds != -1 ? new Date(releaseDateMilliseconds) : null);
                articleModal.setContent(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Description)));
                articleModal.setImageUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_ImageThumb)));
                articleModal.setVideoUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_VideoUrl)));
                listArticles.add(articleModal);
            } while (cursor.moveToNext());
        }
        return listArticles;

    }

    //==========Single Column Selected====
    public ArticleModal getSingleArticle(int id) {
        ArticleModal articleModal = new ArticleModal();

        //select query
        String query = "select * from " + UpgradeDBHelper.TABLE_Article + "where " + UpgradeDBHelper.COLUMN_Id + "=" + id + "";
        Cursor cursor = mDatabase.rawQuery(query, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }

        articleModal.setId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Id)));
        articleModal.setTitle(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_NAME)));
        articleModal.setContent(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Description)));
        articleModal.setImageUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_ImageThumb)));
        articleModal.setVideoUrl(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_VideoUrl)));
        return articleModal;
    }

    public void deleteArticles() {
        try {
            mDatabase.delete(UpgradeDBHelper.TABLE_Article, null, null);
        } catch (SQLiteException ex) {
            if (ex.getMessage().contains("no such table")) {
                mDatabase.execSQL(UpgradeDBHelper.CREATE_TABLE_ARTICLE);
            }
        }

    }
//endregion
    //region ==================PRODUCT CATEGORY SECTION==============

    public void insertProductCategory(ArrayList<ProductCategoryModel> arrayproductCategory, boolean clearprevious) {
        if (clearprevious) {
            deleteProductCategory();
        }

        // prepare statement for insert command
        String sql = "insert into " + UpgradeDBHelper.TABLE_PRODUCTCATEGORY + " VALUES (?,?,?,?,?);";
        //compile kthe statement and start the transaction
        SQLiteStatement Statement = mDatabase.compileStatement(sql);
        mDatabase.beginTransaction();
        for (int i = 0; i < arrayproductCategory.size(); i++) {
            ProductCategoryModel currentpc = arrayproductCategory.get(i);
            Statement.clearBindings();
            Statement.bindString(1, Integer.toString(currentpc.getId()));
            Statement.bindString(2, currentpc.getTitle());
            Statement.bindString(3, currentpc.getDescripton());
            Statement.bindString(4, currentpc.getShortDescription());
            Statement.bindString(5, currentpc.getImage());
            Statement.execute();
        }
        logger.info("inserting Product Category" + arrayproductCategory.size() + new Date(System.currentTimeMillis()));
        mDatabase.setTransactionSuccessful();
        mDatabase.endTransaction();

    }

    public ArrayList<ProductCategoryModel> readProductCategory() {
        ArrayList<ProductCategoryModel> listProductCategory = new ArrayList<>();
        String[] columns = {
                UpgradeDBHelper.COLUMN_Id,
                UpgradeDBHelper.COLUMN_Title,
                UpgradeDBHelper.COLUMN_Description,
                UpgradeDBHelper.COLUMN_ShortDescription,
                UpgradeDBHelper.COLUMN_ImageThumb,

        };
        try {
            Cursor cursor = mDatabase.query(UpgradeDBHelper.TABLE_PRODUCTCATEGORY, columns, null, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                // logger.info(("loading entries " + cursor.getCount() + new Date(System.currentTimeMillis())));
                do {

                    //create a new Product Category object and retrieve the data from the cursor to be stored in this movie object
                    ProductCategoryModel productCategoryModel = new ProductCategoryModel();
                    //each step is a 2 part process, find the index of the column first, find the data of that column using
                    //that index and finally set our blank movie object to contain our data
                    productCategoryModel.setId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Id)));
                    productCategoryModel.setTitle(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Title)));
                    productCategoryModel.setDescripton(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Description)));
                    productCategoryModel.setShortDescription(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_ShortDescription)));
                    productCategoryModel.setImage(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_ImageThumb)));

                    listProductCategory.add(productCategoryModel);
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException ex) {
            if (ex.getMessage().contains("no such table")) {
                mDatabase.execSQL(UpgradeDBHelper.CREATE_TABLE_PRODUCT_CATEGORY);
            }
        }

        return listProductCategory;

    }

    public ProductCategoryModel getSingleProductCategory(int id) {
        ProductCategoryModel productCategoryModel = new ProductCategoryModel();
        try {
            //select query
            String query = "select * from " + UpgradeDBHelper.TABLE_PRODUCTCATEGORY + " where " + UpgradeDBHelper.COLUMN_Id + "=" + id + "";
            Cursor cursor = mDatabase.rawQuery(query, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }

            productCategoryModel.setId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Id)));
            productCategoryModel.setId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Id)));
            productCategoryModel.setTitle(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Title)));
            productCategoryModel.setDescripton(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Description)));
            productCategoryModel.setShortDescription(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_ShortDescription)));
            productCategoryModel.setImage(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_ImageThumb)));
        } catch (SQLiteException ex) {
            if (ex.getMessage().contains("no such table")) {
                mDatabase.execSQL(UpgradeDBHelper.CREATE_TABLE_PRODUCT_CATEGORY);
            }
        }
        return productCategoryModel;
    }

    public void deleteProductCategory() {
        mDatabase.delete(UpgradeDBHelper.TABLE_PRODUCTCATEGORY, null, null);

    }

    public void InsertSchool(ArrayList<SchoolModel> listSchool,boolean clearprevois) {
        if (clearprevois) {
            deleteSchool();

        }
        // prepare  insert statement
        String sqlstatement = "insert into " + UpgradeDBHelper.TABLE_SCHOOL + " values(?,?); ";
        SQLiteStatement statement = mDatabase.compileStatement(sqlstatement);
        mDatabase.beginTransaction();

        for (int i = 0; i < listSchool.size(); i++) {

            SchoolModel schoolModel = listSchool.get(i);
            statement.clearBindings();
            statement.bindString(1, Integer.toString(schoolModel.getSchoolId()));
            statement.bindString(2, schoolModel.getSchoolName());
            statement.execute();
        }

    }
    public ArrayList<SchoolModel> readSchool() {

        ArrayList<SchoolModel> listschool = new ArrayList<>();
        String[] columns = {
                UpgradeDBHelper.COLUMN_Id,
                UpgradeDBHelper.COLUMN_SCHOOLNAME,


        };
        try {
            Cursor cursor = mDatabase.query(UpgradeDBHelper.TABLE_SCHOOL, columns, null, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                // logger.info(("loading entries " + cursor.getCount() + new Date(System.currentTimeMillis())));
                do {

                    //create a new Product Category object and retrieve the data from the cursor to be stored in this movie object
                    SchoolModel schoolModel = new SchoolModel();
                    //each step is a 2 part process, find the index of the column first, find the data of that column using
                    //that index and finally set our blank movie object to contain our data
                    schoolModel.setSchoolId(cursor.getInt(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Id)));
                    schoolModel.setSchoolName(cursor.getString(cursor.getColumnIndex(UpgradeDBHelper.COLUMN_Title)));

                    listschool.add(schoolModel);
                } while (cursor.moveToNext());
            }
        } catch (SQLiteException ex) {
            if (ex.getMessage().contains("no such table")) {
                mDatabase.execSQL(UpgradeDBHelper.CREATE_TABLE_SCHOOL);
            }
        }

        return listschool;

    }

    public void deleteSchool() {
        mDatabase.delete(UpgradeDBHelper.TABLE_SCHOOL, null, null);

    }


    //endregion
    //region ===================DB HELPER==================================
    private static class UpgradeDBHelper extends SQLiteOpenHelper {


        //=====================TABLE For APP=======================
        public static final String TABLE_News = "NewsTB";
        public static final String TABLE_RoleModal = "RoleModalTB";
        public static final String TABLE_Product = "ProductsTb";
        public static final String TABLE_Article = "ArticleTB";
        public static final String TABLE_PRODUCTCATEGORY = "ProductCategoryTB";
        public static final String TABLE_SCHOOL="SchoolTB";
        //==================Common Column================
        public static final String COLUMN_Id = "DataId";
        public static final String COLUMN_Title = "Title";
        public static final String COLUMN_NAME = "Name";
        public static final String COLUMN_ImageThumb = "ImageThumb";
        public static final String COLUMN_VideoUrl = "Video";
        public static final String COLUMN_Description = "Description";
        public static final String COLUMN_ShortDescription = "ShortDescription";
        public static final String COLUMN_PostedOn = "PostedOn";
        //=======================Product Table Column=======================
        public static final String COLUMN_Price = "Price";
        public static final String COLUMN_CategoryID = "CategoryId";

        //=======================School Table Column=======================
        public static final String COLUMN_SCHOOLNAME="SchoolName";
        //=====================Create Table News =============================
        private static final String CREATE_TABLE_NEWS = "CREATE TABLE if not exists " + TABLE_News + " (" +
                COLUMN_Id + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_Title + " TEXT," +
                COLUMN_ImageThumb + " TEXT," +
                COLUMN_VideoUrl + " TEXT," +
                COLUMN_Description + " TEXT," +
                COLUMN_PostedOn + " TEXT" +
                ");";
        //=====================Create Table Role Modal =============================
        private static final String CREATE_TABLE_ROLEMODAL = "CREATE  TABLE if not exists " + TABLE_RoleModal + " (" +
                COLUMN_Id + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_Title + " TEXT," +
                COLUMN_ImageThumb + " TEXT," +
                COLUMN_VideoUrl + " TEXT," +
                COLUMN_Description + " TEXT," +
                COLUMN_PostedOn + " TEXT" +
                ");";
        //=====================Create Table Article =============================
        private static final String CREATE_TABLE_ARTICLE = "CREATE TABLE if not exists " + TABLE_Article + " (" +
                COLUMN_Id + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_Title + " TEXT," +
                COLUMN_Description + " TEXT," +
                COLUMN_ImageThumb + " TEXT," +
                COLUMN_VideoUrl + " TEXT ," +
                COLUMN_PostedOn + " TEXT" +
                ");";
        //=====================Create Table Product =============================
        private static final String CREATE_TABLE_PRODUCT = "CREATE TABLE if not exists " + TABLE_Product + " (" +
                COLUMN_Id + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_NAME + " TEXT," +
                COLUMN_ImageThumb + " TEXT," +
                COLUMN_VideoUrl + " TEXT," +
                COLUMN_Description + " TEXT," +
                COLUMN_ShortDescription+" TEXT,"+
                COLUMN_Price + " TEXT," +
                COLUMN_PostedOn + " TEXT," +
                COLUMN_CategoryID +" INTEGER"+
                ");";


        //========== Create Table Product Category
        private static final String CREATE_TABLE_PRODUCT_CATEGORY = "CREATE TABLE if not exists " + TABLE_PRODUCTCATEGORY
                + "(" + COLUMN_Id + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_Title + " TEXT," +
                COLUMN_ShortDescription + " TEXT," +
                COLUMN_Description + " TEXT," +
                COLUMN_ImageThumb + " TEXT" + ");";
        //==================== CREATE TABLE SCHOOL ===============
        private static final String CREATE_TABLE_SCHOOL="CREATE TABLE if not exists "+ TABLE_SCHOOL+"("+
                COLUMN_Id + " INTEGER PRIMARY KEY AUTOINCREMENT,"+COLUMN_SCHOOLNAME+ " TEXT" +
                ");";



        private static final String DB_NAME = "UpgradeDB";
        private static final int DB_VERSION = 1;
        final Logger logger = LoggerFactory.getLogger(UpgradeDBHelper.class);
        private Context appcontext;

        public UpgradeDBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
            appcontext = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(CREATE_TABLE_NEWS);
                logger.info("create news table executed");
                db.execSQL(CREATE_TABLE_ROLEMODAL);
                logger.info("create role modal table executed");
                db.execSQL(CREATE_TABLE_ARTICLE);
                logger.info("create article table executed");
                db.execSQL(CREATE_TABLE_PRODUCT);
                logger.info("create table product executed");
                db.execSQL(CREATE_TABLE_PRODUCT_CATEGORY);
                logger.info("create table product excecuted");
                db.execSQL(CREATE_TABLE_SCHOOL);
                logger.info("create table school created");
            } catch (SQLiteException exception) {
                logger.error(exception.toString());

            }
        }


        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                logger.info("upgrade table  executed");
                db.execSQL(" DROP TABLE " + TABLE_News + " IF EXISTS;");
                db.execSQL(" DROP TABLE " + TABLE_RoleModal + " IF EXISTS;");
                db.execSQL(" DROP TABLE " + TABLE_Article + " IF EXISTS;");
                db.execSQL(" DROP TABLE " + TABLE_Product + " IF EXISTS;");
                db.execSQL(" DROP TABLE " + TABLE_PRODUCTCATEGORY + " IF EXITS");
                db.execSQL(" DROP TABLE "+ TABLE_SCHOOL + " IF EXITS");
                onCreate(db);
            } catch (SQLiteException exception) {
                logger.error(exception.toString());
            }
        }
    }

    //endregion
}