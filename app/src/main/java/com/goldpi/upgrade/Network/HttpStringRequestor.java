package com.goldpi.upgrade.Network;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.StringRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yusuf on 4/23/2016.
 */
public class HttpStringRequestor extends StringRequest  {
    final Logger logger = LoggerFactory.getLogger(HttpStringRequestor.class);
    Context  getBaseContext;

    public HttpStringRequestor(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
               super(method, url, listener, errorListener);

    }



    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json; charset=utf-8");
        //Authorization SavedAuthorization= AccessTokenPrefrance.getCurrentUser(getBaseContext());
        //logger.info("Access Token:-" + SavedAuthorization.getAccess_token());
        headers.put("access_token", "application/json; charset=utf-8");
        return headers;
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {

        return super.getParams();
    }

    @Override
    public RetryPolicy getRetryPolicy()
    {
        DefaultRetryPolicy Policy= new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        return super.getRetryPolicy();
    }


}

