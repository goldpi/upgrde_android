package com.goldpi.upgrade.Network;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.RequestFuture;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;

/**
 * Created by Yusuf on 5/1/2016.
 */
public class HTTPJSONRequestor {
    final static Logger logger = LoggerFactory.getLogger(HTTPJSONRequestor.class);
    public static JSONArray requestJSON(RequestQueue requestQueue, String url) {
       JSONArray  response = null;
        RequestFuture<JSONArray> requestFuture = RequestFuture.newFuture();

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
                url,
                (String)null, requestFuture, requestFuture);
        request.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(request);

        try {
          int time=request.getTimeoutMs();
            response=requestFuture.get();
            logger.info("Web API Response:- "+response);
            //response = requestFuture.get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
           logger.info(e + "");
        } catch (ExecutionException e) {
            logger.info(e + "");
        }
        return response;
    }
}