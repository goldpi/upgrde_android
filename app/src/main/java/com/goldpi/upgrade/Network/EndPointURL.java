package com.goldpi.upgrade.Network;

/**
 * Created by Yusuf on 4/19/2016.
 * Edited by Imran imran@goldpi on 5/8/2016
 */
public class EndPointURL {

    //region Notification Server

    //region Production
     public static final String APP_NOTIFICATION_URL="http://notification.upgrde.net/signalr";
    //endregion
    //region Development
   // public static final String APP_NOTIFICATION_URL="http://notification.scoolfirst.com/signalr";
    //endregion

    //endregion

    //region BaseUrlOfApi

    //region ProductionUrl
     private static String BaseApiUrl="http://api.upgrde.net";
    //endregion

    //region Development Url

   // private static String BaseApiUrl="http://api.scoolfirst.com";


    //endregion

    //endregion

    // Login API URL
    // public static final String APP_LOGIN_URL = "http://api.upgrde.net/token";
    //Dev
    public static final String APP_LOGIN_URL = BaseApiUrl+"/token";

    // Login API URL PARAMETER
    public static final String Username="Username";
    public static final String Password="Password";
    public static final String GrantType="grant_type";

    //Refresh Token
    public static final String RefreshToken="refresh_token";

    public static final String APP_UserProfile_URL =BaseApiUrl+ "/token";


    //=================================News API URL=========================================
    public static final String APP_NEWS_URL=BaseApiUrl+"/api/News";

    //=================================Article API URL=========================================
    public static final String APP_COMPUTER_CREATIVE_URL =BaseApiUrl+"/api/ComputerCreative";
    //==================================Role Model URL===================================
    public static final String APP_ROLEMODEL_URL=BaseApiUrl+"/api/RoleModel";

    //=================================Product APT URL==========================================
    public static final String APP_PRODUCT_URL=BaseApiUrl+"/api/Product";//?page={page}&size={size}

    //================================Slider=======================================================
    public static final String APP_SLIDER_URL =BaseApiUrl+"/api/slider/";
    public static final String APP_SLIDER_ROLEMODEL=BaseApiUrl+"/api/slider/RoleModel";
    public static final String APP_SLIDER_COMPUTER_CREATIVE=BaseApiUrl+"/api/slider/ComputerNCreative";
    public static final String APP_SLIDER_PRODUCT_CATEGORY=BaseApiUrl+"/api/slider/ProductCategory";

    //=================================Product Category URL=====================================
    public static final String APP_PRODUCT_CATEGORY_URL =BaseApiUrl+"/api/ProductCategories";

    //================================== Registration Class URL=================================
    public static final String APP_CLASS_URL =BaseApiUrl+"/api/Class";

    //===================================Registration School Url===============================
    public static final String APP_SCHOOL_URL =BaseApiUrl+"/api/School";

    //======================class Api========================================
    public static final String APP_LOAD_CLASS_URL = BaseApiUrl+"/Class";
   //=======================Class Api Parameter==============================
    public static final String ClassId="ClassId";
    public static final String ClassName="ClassName";




}
