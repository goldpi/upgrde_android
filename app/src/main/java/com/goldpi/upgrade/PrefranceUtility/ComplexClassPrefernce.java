package com.goldpi.upgrade.PrefranceUtility;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * Created by Digambar on 5/28/2016.
 */
public class ComplexClassPrefernce {
    private static final Gson GSON = new Gson();
    public static ComplexClassPrefernce complexClassPrefernce;
    private Context context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    Type typeOfObject = new TypeToken<Object>() {
    }.getType();
    private ComplexClassPrefernce(Context context, String namePreferences, int mode) {
        this.context = context;
        if (namePreferences == null || namePreferences.equals("")) {
            namePreferences = "complex_preferences";
        }
        sharedPreferences = context.getSharedPreferences(namePreferences, mode);
        editor = sharedPreferences.edit();
    }

    public static ComplexClassPrefernce getComplexPreferences(Context context,
                                                           String namePreferences, int mode) {

//		if (complexPreferences == null) {
        complexClassPrefernce = new ComplexClassPrefernce(context,
                namePreferences, mode);
//		}

        return complexClassPrefernce;
    }

    public void putObject(String key, Object object) {
        if(object == null){
            throw new IllegalArgumentException("object is null");
        }

        if(key.equals("") || key == null){
            throw new IllegalArgumentException("key is empty or null");
        }

        editor.putString(key, GSON.toJson(object));
    }

    public void commit() {
        editor.commit();
    }

    public void clearObject() {
        editor.clear();
    }

    public boolean HasObbject(String key){
        return sharedPreferences.contains(key);
    }

    public <T> T getObject(String key, Class<T> a) {

        String gson = sharedPreferences.getString(key, null);
        if (gson == null) {
            return null;
        } else {
            try {
                return GSON.fromJson(gson, a);
            } catch (Exception e) {
                throw new IllegalArgumentException("Object storaged with key " + key + " is instanceof other class");
            }
        }
    }
}
