package com.goldpi.upgrade.PrefranceUtility;

import android.content.Context;

import com.goldpi.upgrade.Account.Class.ClassModel;

import static com.goldpi.upgrade.PrefranceUtility.KEY.AccessToken;

/**
 * Created by Digambar on 5/30/2016.
 */
public class AccessClassTokenPreferance {
    public static void setCurrentClass(ClassModel currentClass, Context ctx){
        ComplexClassPrefernce complexClassPrefernce = ComplexClassPrefernce.getComplexPreferences(ctx, "user_prefs", 0);
        complexClassPrefernce.putObject(AccessToken, currentClass); //Access token to be chnaged
        complexClassPrefernce.commit();
    }

    public static ClassModel getCurrentClass(Context ctx){
        ComplexClassPrefernce complexclassPreferences = ComplexClassPrefernce.getComplexPreferences(ctx, "user_prefs", 0);
        ClassModel currentClass = complexclassPreferences.getObject(AccessToken, ClassModel.class); //need to be changed complex
        return currentClass;
    }

    public static void clearCurrentClass( Context ctx){
        ComplexClassPrefernce complexClassPrefernce = ComplexClassPrefernce.getComplexPreferences(ctx, "user_prefs", 0);
        complexClassPrefernce.clearObject();
        complexClassPrefernce.commit();
    }

    public static boolean HasItemsInPrefrance(Context ctx){
        try{
            ComplexClassPrefernce complexClassPrefernce = ComplexClassPrefernce.getComplexPreferences(ctx, "user_prefs", 0);
            return complexClassPrefernce.HasObbject(AccessToken);
            // return true;
        }catch(Exception ex){
            return false;
        }
    }
}
