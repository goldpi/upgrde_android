package com.goldpi.upgrade.PrefranceUtility;

import android.content.Context;
import com.goldpi.upgrade.DataModal.Authorization;
import com.goldpi.upgrade.Landing.HomeSlider.SliderModal;

import java.util.ArrayList;


/**
 * Created by Yusuf on 5/23/2016.
 */
public class SliderPreference {

    public static void setSlider(SliderModal[] slideritem , Context ctx,String prefrance_key){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "Slider", 0);
        complexPreferences.putObject(prefrance_key, slideritem);
        complexPreferences.commit();
    }

    public static SliderModal[] getSlider(Context ctx,String prefrance_key){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "Slider", 0);
        SliderModal[] slideritem = complexPreferences.getObject(prefrance_key, SliderModal[].class);
        return slideritem;
    }

    public static void clearSlider( Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "Slider", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }

    public static boolean HasItemsInPrefrance(Context ctx,String prefrance_key){
        try{
            ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "Slider", 0);
            return complexPreferences.HasObbject(prefrance_key);
            // return true;
        }catch(Exception ex){
            return false;
        }
    }
}
