package com.goldpi.upgrade.PrefranceUtility;

import android.content.Context;

import com.goldpi.upgrade.DataModal.Authorization;

import static  com.goldpi.upgrade.PrefranceUtility.KEY.AccessToken;
/**
 * Created by Yusuf on 4/23/2016.
 */
public class AccessTokenPrefrance {

    public static void setCurrentUser(Authorization currentUser, Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        complexPreferences.putObject(AccessToken, currentUser);
        complexPreferences.commit();
    }

    public static Authorization getCurrentUser(Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        Authorization currentUser = complexPreferences.getObject(AccessToken, Authorization.class);
        return currentUser;
    }

    public static void clearCurrentUser( Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }

    public static boolean HasItemsInPrefrance(Context ctx){
        try{
            ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
            return complexPreferences.HasObbject(AccessToken);
           // return true;
        }catch(Exception ex){
            return false;
        }
    }
}
