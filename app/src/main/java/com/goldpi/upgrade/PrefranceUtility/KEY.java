package com.goldpi.upgrade.PrefranceUtility;

/**
 * Created by Yusuf on 4/22/2016.
 */
public class KEY {

    public static final String AccessToken="com.goldpi.upgrade.account.access_token";
    public static final String KEY_Slider_DASHBOARD="com.goldpi.upgrade.landing.slider_dashboard";
    public static final String KEY_SLIDER_ROLEMODEL="com.goldpi.upgrade.landing.slider_role_model";
    public static final String KEY_SLIDER_COMPUTER_CREATIVE="com.goldpi.upgrade.landing.slider_computer_creative";
    public static final String KEY_SLIDER_PRODUCT_CATEGORY="com.goldpi.upgrade.landing.slider_productcategory";
}
