package com.goldpi.upgrade.DataModal;

import java.util.Date;

/**
 * Created by Imran on 5/3/2016.
 */
public class NotificationModel {

    public NotificationModel(){

    }

    public NotificationModel(String id,String imageUrl,String title,String message,Date date,Boolean read){
        setId(id);
        setImageUrl(imageUrl);
        setTitle(title);
        setMessage(message);
        setDate(date);
        setRead(read);
    }
    private String Id ;

    private String ImageUrl;

    private String Title ;

    private String Message;

    private Date Date ;
    private boolean Read ;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getImageUrl() {
            return ImageUrl;
    }

    public boolean isRead() {
        return Read;
    }

    public java.util.Date getDate() {
        return Date;
    }

    public String getMessage() {
        return Message;
    }

    public String getTitle() {
        return Title;
    }

    public void setDate(java.util.Date date) {
        Date = date;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public void setRead(boolean read) {
        Read = read;
    }

    public void setTitle(String title) {
        Title = title;
    }

}
