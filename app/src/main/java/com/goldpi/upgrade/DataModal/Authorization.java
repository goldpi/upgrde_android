package com.goldpi.upgrade.DataModal;

import com.google.gson.annotations.SerializedName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Yusuf on 4/21/2016.
 */
public class Authorization {


    private  String access_token;
    private  String token_type;
    private  String expires_in;
    @SerializedName("userName")
    private  String Username;
    @SerializedName(".issued")
    private  Date IssuedOn;
    @SerializedName(".expires")
    private  Date ExpireOn;
    private String refresh_token;

    public Authorization()
    {

    }

    public Authorization(String access_token,String token_type,String expires_in,String userName,Date issued,Date expires,String refresh_token )
    {
        this.access_token=access_token;
        this.token_type=token_type;
        this.expires_in=expires_in;
        this.Username=userName;
        this.IssuedOn=issued;
        this.ExpireOn=expires;
        this.refresh_token=refresh_token;
    }

    public  void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
    public  String getAccess_token() {
        return access_token;
    }

    public  void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    public  String getToken_type() {
        return token_type;
    }

    public  void setExpires_in(String expires_in) {
        this.expires_in = expires_in;
    }

    public  String getExpires_in() {
        return expires_in;
    }

    public  void setUsername(String userName) {
        this.Username = userName;
    }

    public  String getUsername() {
        return Username;
    }


    public void setExpireOn(Date expireOn) {
        ExpireOn = expireOn;
    }

    public Date getExpireOn() {
        return ExpireOn;
    }

    public Date getIssuedOn() {
        return IssuedOn;
    }

    public void setIssuedOn(Date issuedOn) {
        IssuedOn = issuedOn;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }
}
