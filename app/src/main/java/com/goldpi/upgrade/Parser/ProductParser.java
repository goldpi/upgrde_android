package com.goldpi.upgrade.Parser;

import android.util.Log;

import com.goldpi.upgrade.Landing.Products.ProductModal;
import com.goldpi.upgrade.Utility.IConstants;
import com.goldpi.upgrade.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Digambar on 5/20/2016.
 */
public class ProductParser {
    public static ArrayList<ProductModal> parseProductJSON(JSONArray response) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        ArrayList<ProductModal> listProduct = new ArrayList<>();
        if (response != null && response.length() > 0) {
            try {
                // JSONArray arrayMovies = response.getJSONArray("");
                for (int i = 0; i < response.length(); i++) {
                    int id = -1;
                    int CategoryId = -1;
                    String Title = IConstants.NA;
                    String AddedOn = null;
                    double price = 0.0;
                    String shortDesp = IConstants.NA;
                    String LongDesp = IConstants.NA;
                    String Thumbnail = IConstants.NA;
                    String VideoURL = IConstants.NA;

                    JSONObject currentProduct = response.getJSONObject(i);


                    //get the id of the current movie
                    if (Utils.contains(currentProduct, "Id")) {
                        id = currentProduct.getInt("Id");
                    }
                    //get the title of the current movie
                    if (Utils.contains(currentProduct, "Title")) {
                        Title = currentProduct.getString("Title");
                    }
                    //get the title of the current movie
                    if (Utils.contains(currentProduct, "Image")) {
                        if (!currentProduct.getString("Image").isEmpty())
                            Thumbnail = currentProduct.getString("Image");
                        else
                            Thumbnail = IConstants.NA;
                    }
                    //get the Video of the current Product
                    if (Utils.contains(currentProduct, "Video")) {
                        if (!currentProduct.getString("Video").isEmpty())
                            VideoURL = currentProduct.getString("Video");
                        else
                            VideoURL = IConstants.NA;
                    }
                    if (Utils.contains(currentProduct, "ProductDisc")) {
                        LongDesp = currentProduct.getString("ProductDisc");
                    }
                    if (Utils.contains(currentProduct, "ShortDisc")) {
                        shortDesp = currentProduct.getString("ShortDisc");
                    }
                    if (Utils.contains(currentProduct, "Price")) {
                        price = currentProduct.getDouble("Price");
                    }
                    if (Utils.contains(currentProduct, "AddedOn")) {
                        AddedOn = currentProduct.getString("AddedOn");


                    }

                    if (Utils.contains(currentProduct, "CategoryId")) {
                        CategoryId = currentProduct.getInt("CategoryId");
                    }

                    ProductModal obj_product = new ProductModal();
                    obj_product.setId(id);
                    obj_product.setName(Title);
                    obj_product.setImageUrl(Thumbnail);
                    obj_product.setVideoUrl(VideoURL);
                    obj_product.setLongDesc(LongDesp);
                    obj_product.setShortDesc(shortDesp);
                    obj_product.setPrice(price);
                    Date date = null;
                    try {
                        date = dateFormat.parse(AddedOn);
                    } catch (ParseException e) {
                        //a parse exception generated here will store null in the release date, be sure to handle it
                        Log.i("Exception", e.getMessage());
                    }
                    obj_product.setCategoryId(CategoryId);
                    obj_product.setAddedon(date);


                    listProduct.add(obj_product);

                }

            } catch (JSONException e) {
                Log.i("Exception", e.getMessage());
            }
//                L.t(getActivity(), listMovies.size() + " rows fetched");
        }
        return listProduct;
    }
}