package com.goldpi.upgrade.Parser;

import com.goldpi.upgrade.Landing.ProductCategory.ProductCategoryModel;
import com.goldpi.upgrade.Utility.IConstants;
import com.goldpi.upgrade.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/23/2016.
 */
public class ProductCategoryParser {
    public static ArrayList<ProductCategoryModel> parseProductCategoryJSON(JSONArray response) {
        ArrayList<ProductCategoryModel> listProductCategory = new ArrayList<>();
        if (response != null && response.length() > 0) {
            try {
                for (int i = 0; i < response.length(); i++) {
                    int id = -1;
                    String Title = IConstants.NA;
                    String ShortDesc = IConstants.NA;
                    String Desc = IConstants.NA;
                    String ImageUrl = IConstants.NA;
                    JSONObject currentProductCategory = response.getJSONObject(i);
                    if (Utils.contains(currentProductCategory, "Id")) {

                        id = currentProductCategory.getInt("Id");
                    }
                    //get the Title of the current Product Category
                    if (Utils.contains(currentProductCategory, "Title")) {
                        Title = currentProductCategory.getString("Title");
                    }
                    //get the ShortDesc of the current Product Category
                    if (Utils.contains(currentProductCategory, "ShortDescription")) {

                        ShortDesc = currentProductCategory.getString("ShortDescription");
                    }
                    //get the Desc of the current Product Category
                    if (Utils.contains(currentProductCategory, "Description")) {

                        Desc = currentProductCategory.getString("Description");
                    }
                    //get the ImageUrl of the current Articles
                    if (Utils.contains(currentProductCategory, "Image")) {
                        if (!currentProductCategory.getString("Image").isEmpty()) {
                            ImageUrl = currentProductCategory.getString("Image");
                        } else {

                            ImageUrl = IConstants.NA;
                        }
                    }

                    //set values to the product Category
                    ProductCategoryModel pcm = new ProductCategoryModel();
                    pcm.setId(id);
                    pcm.setTitle(Title);
                    pcm.setDescripton(Desc);
                    pcm.setShortDescription(ShortDesc);
                    pcm.setImage(ImageUrl);


                    if (pcm.getId() > 0 && pcm.getTitle() != IConstants.NA) {
                        listProductCategory.add(pcm);
                    }
                }
            } catch (JSONException e) {

            }
        }
        return listProductCategory;

    }
}