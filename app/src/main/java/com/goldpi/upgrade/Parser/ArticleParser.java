package com.goldpi.upgrade.Parser;

import com.goldpi.upgrade.Landing.Articles.ArticleModal;
import com.goldpi.upgrade.Landing.News.NewsModal;
import com.goldpi.upgrade.Utility.IConstants;
import com.goldpi.upgrade.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Digambar on 5/11/2016.
 */
public class ArticleParser {
    public static ArrayList<ArticleModal> parseArticalJSON(JSONArray response) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        ArrayList<ArticleModal> listArticles = new ArrayList<>();
        if (response != null && response.length() > 0) {
            try {
                // JSONArray arrayMovies = response.getJSONArray("");
                for (int i = 0; i < response.length(); i++) {
                    int id = -1;
                    String Title = IConstants.NA;
                    String PostedOn =null;
                    String Description = IConstants.NA;
                    String Thumbnail = IConstants.NA;
                    String VideoURL = IConstants.NA;

                    JSONObject currentArticle = response.getJSONObject(i);


                    //get the id of the current movie
                    if (Utils.contains(currentArticle, "Id")) {
                        id = currentArticle.getInt("Id");
                    }
                    //get the title of the current movie
                    if (Utils.contains(currentArticle, "Title")) {
                        Title = currentArticle.getString("Title");
                    }

                    //get the title of the current movie
                    if (Utils.contains(currentArticle, "ImageUrl")) {
                        if(!currentArticle.getString("ImageUrl").isEmpty())
                            Thumbnail = currentArticle.getString("ImageUrl");
                        else
                            Thumbnail=IConstants.NA;
                    }

                    //get the title of the current movie
                    if (Utils.contains(currentArticle, "VideoUrl")) {
                        if(!currentArticle.getString("VideoUrl").isEmpty())
                            VideoURL = currentArticle.getString("VideoUrl");
                        else
                            VideoURL = IConstants.NA;
                    }

                    //get the title of the current movie
                    if (Utils.contains(currentArticle, "Content")) {
                        Description = currentArticle.getString("Content");
                    }

                    //get the date in theaters for the current movie
                    if (Utils.contains(currentArticle, "On")) {
                        PostedOn = currentArticle.getString("On");


                    }


                   ArticleModal obj_Article = new ArticleModal();
                    obj_Article.setId(id);
                    obj_Article.setTitle(Title);
                    Date date = null;
                    try {
                        date = dateFormat.parse(PostedOn);
                    } catch (ParseException e) {
                        //a parse exception generated here will store null in the release date, be sure to handle it
                    }
                    obj_Article.setPostedOn(date);
                    obj_Article.setContent(Description);
                    obj_Article.setImageUrl(Thumbnail);
                    obj_Article.setVideoUrl(VideoURL);

                    if (id != -1 && !Title.equals(IConstants.NA)) {
                        listArticles.add(obj_Article);
                    }
                }

            } catch (JSONException e) {

            }
//                L.t(getActivity(), listArticles.size() + " rows fetched");
        }
        return listArticles;
    }
}
