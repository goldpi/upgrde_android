package com.goldpi.upgrade.Parser;

import android.util.Log;

import com.goldpi.upgrade.Landing.HomeSlider.SlicesModel;
import com.goldpi.upgrade.Landing.HomeSlider.SliderModal;
import com.goldpi.upgrade.Utility.IConstants;
import com.goldpi.upgrade.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/24/2016.
 */
public class SliderParser {

    static final String data = "[{\"Title\":\"Android Dashboard\",\"Slices\":[{\"Title\":\"Upgrade Dashboard\",\"Description\":null,\"Image\":\"http://static.upgrde.net/source/logo/Slider/img_role_model.png\",\"SlidingOrder\":0},{\"Title\":\"What is Lorem Ipsum?\",\"Description\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry.\",\"Image\":\"https://placeimg.com/750/357/tech\",\"SlidingOrder\":2},{\"Title\":\"Why do we use it?\",\"Description\":\"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.\",\"Image\":\"https://placeimg.com/750/357/nature\",\"SlidingOrder\":1}]},{\"Title\":\"Android RoleModel\",\"Slices\":[{\"Title\":\"Where can I get some?\",\"Description\":\"There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.\",\"Image\":\"https://placeimg.com/750/357/nature\",\"SlidingOrder\":0},{\"Title\":\"Where does it come from?\",\"Description\":\"Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.\",\"Image\":\"https://placeimg.com/750/357/arch\",\"SlidingOrder\":0},{\"Title\":\"The standard Lorem Ipsum passage, used since the 1500s\",\"Description\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. \",\"Image\":\"https://placeimg.com/750/357/animals\",\"SlidingOrder\":0}]},{\"Title\":\"Android Computer & Creative\",\"Slices\":[]},{\"Title\":\"Android Product Category\",\"Slices\":[]}]";

    public static ArrayList<SliderModal> parseSliderJSON(JSONArray response) {

        ArrayList<SliderModal> listslider = new ArrayList<>();
        for (int i = 0; i < response.length(); i++) {
            try {
                String Title = IConstants.NA;
                JSONObject jsonSlider = response.getJSONObject(i);
                SliderModal sliderItem = new SliderModal();
                if (Utils.contains(jsonSlider, "Title"))
                    Title = jsonSlider.getString("Title");

                JSONArray slicesarray = jsonSlider.getJSONArray("Slices");
                ArrayList<SlicesModel> slices = new ArrayList<SlicesModel>();
                for (int j = 0; j < slicesarray.length(); j++) {
                    String SliceTitle = IConstants.NA;
                    String Description = IConstants.NA;
                    String Image = IConstants.NA;
                    int SlidingOrder = -1;
                    JSONObject jsonslice = slicesarray.getJSONObject(j);

                    SlicesModel slicesModel = new SlicesModel();
                    if (Utils.contains(jsonslice, "Title")) {
                        SliceTitle = jsonslice.getString("Title");
                    }
                    if (Utils.contains(jsonslice, "Description")) {
                        Description = jsonslice.getString("Description");
                    }
                    if (Utils.contains(jsonslice, "Image"))
                        if (!jsonslice.getString("Image").isEmpty()) {
                            Image = jsonslice.getString("Image");
                        }

                    if (Utils.contains(jsonslice, "SlidingOrder")) {

                        SlidingOrder = jsonslice.getInt("SlidingOrder");
                    }
                    slicesModel.setTitle(SliceTitle);
                    slicesModel.setDescription(Description);
                    slicesModel.setImage(Image);
                    slicesModel.setSlidingOrder(SlidingOrder);
                    slices.add(slicesModel);

                }

                sliderItem.setSlices(slices);
                sliderItem.setTitle(Title);
                listslider.add(sliderItem);


            } catch (Exception ex) {
                Log.d("Parsor", ex.getMessage());

            }
        }

        return listslider;
    }

}
