package com.goldpi.upgrade.Parser;

import com.goldpi.upgrade.Account.Registration.School.SchoolModel;
import com.goldpi.upgrade.Utility.IConstants;
import com.goldpi.upgrade.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/28/2016.
 */
public class SchoolParser {
    public static ArrayList<SchoolModel> parseSchoolJSON(JSONArray response) {

        ArrayList<SchoolModel> listSchool = new ArrayList<>();
        if (response != null && response.length() > 0) {
            try {
                // JSONArray arrayMovies = response.getJSONArray("");
                for (int i = 0; i < response.length(); i++) {
                    int id = -1;
                    String Name = IConstants.NA;

                    JSONObject currentSchool = response.getJSONObject(i);
                    //get the id of the current movie
                    if (Utils.contains(currentSchool, "Id")) {
                        id = currentSchool.getInt("Id");
                    }
                    //get the title of the current movie
                    if (Utils.contains(currentSchool, "Name")) {
                        Name = currentSchool.getString("Name");
                    }


                    SchoolModel obj_sc = new SchoolModel();
                    obj_sc.setSchoolId(id);
                    obj_sc.setSchoolName(Name);
                        listSchool.add(obj_sc);

                }

            } catch (JSONException e) {

            }
//                L.t(getActivity(), listMovies.size() + " rows fetched");
        }
        return listSchool;
    }
}
