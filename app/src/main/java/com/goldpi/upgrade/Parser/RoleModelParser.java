package com.goldpi.upgrade.Parser;


import com.goldpi.upgrade.Landing.RoleModel.RoleModelModel;
import com.goldpi.upgrade.Utility.IConstants;
import com.goldpi.upgrade.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Digambar on 5/20/2016.
 */
public class RoleModelParser {
    public static ArrayList<RoleModelModel> parseRoleModelJSON(JSONArray response) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        ArrayList<RoleModelModel> listRoleModel = new ArrayList<>();
        if (response != null && response.length() > 0) {
            try {
                // JSONArray arrayMovies = response.getJSONArray("");
                for (int i = 0; i < response.length(); i++) {
                    int id = -1;
                    String Title = IConstants.NA;
                    String PostedOn =null;
                    String Description = IConstants.NA;
                    String Thumbnail = IConstants.NA;
                    String VideoURL = IConstants.NA;
                    JSONObject currentRoleModel = response.getJSONObject(i);
                    //get the id of the current movie
                    if (Utils.contains(currentRoleModel, "Id")) {
                        id = currentRoleModel.getInt("Id");
                    }
                    //get the title of the current movie
                    if (Utils.contains(currentRoleModel, "Title")) {
                        Title = currentRoleModel.getString("Title");
                    }

                    //get the title of the current movie
                    if (Utils.contains(currentRoleModel, "ImageUrl")) {
                        if(!currentRoleModel.getString("ImageUrl").isEmpty())
                            Thumbnail = currentRoleModel.getString("ImageUrl");
                        else
                            Thumbnail=IConstants.NA;
                    }

                    //get the title of the current movie
                    if (Utils.contains(currentRoleModel, "VideoUrl")) {
                        if(!currentRoleModel.getString("VideoUrl").isEmpty())
                            VideoURL = currentRoleModel.getString("VideoUrl");
                        else
                            VideoURL = IConstants.NA;
                    }

                    //get the title of the current movie
                    if (Utils.contains(currentRoleModel, "Content")) {
                        Description = currentRoleModel.getString("Content");
                    }

                    //get the date in theaters for the current movie
                    if (Utils.contains(currentRoleModel, "On")) {
                        PostedOn = currentRoleModel.getString("On");


                    }


                    RoleModelModel obj_rm = new RoleModelModel();
                    obj_rm.setId(id);
                    obj_rm.setTitle(Title);
                    Date date = null;
                    try {
                        date = dateFormat.parse(PostedOn);
                    } catch (ParseException e) {
                        //a parse exception generated here will store null in the release date, be sure to handle it
                    }
                    obj_rm.setPostedOn(date);
                    obj_rm.setContent(Description);
                    obj_rm.setImageUrl(Thumbnail);
                    obj_rm.setVideoUrl(VideoURL);

                    if (obj_rm.getId()>0 && obj_rm.getTitle()!=IConstants.NA) {
                        listRoleModel.add(obj_rm);
                    }
                }

            } catch (JSONException e) {

            }
//                L.t(getActivity(), listMovies.size() + " rows fetched");
        }
        return listRoleModel;
    }
}
