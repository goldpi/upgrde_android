package com.goldpi.upgrade.Parser;


import com.goldpi.upgrade.Landing.News.NewsModal;
import com.goldpi.upgrade.Utility.IConstants;
import com.goldpi.upgrade.Utility.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Yusuf on 5/1/2016.
 */
public class NewsParser {
    public static ArrayList<NewsModal> parseNewsJSON(JSONArray response) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        ArrayList<NewsModal> listMovies = new ArrayList<>();
        if (response != null && response.length() > 0) {
            try {

               // JSONArray arrayMovies = response.getJSONArray("");
                for (int i = 0; i < response.length(); i++) {
                    int id = -1;
                    String Title = IConstants.NA;
                    String PostedOn =null;
                    String Description = IConstants.NA;
                    String Thumbnail = IConstants.NA;
                    String VideoURL = IConstants.NA;

                    JSONObject currentNews = response.getJSONObject(i);


                    //get the id of the current movie
                    if (Utils.contains(currentNews, "Id")) {
                        id = currentNews.getInt("Id");
                    }
                    //get the title of the current movie
                    if (Utils.contains(currentNews, "Title")) {
                        Title = currentNews.getString("Title");
                    }

                    //get the title of the current movie
                    if (Utils.contains(currentNews, "ImageUrl")) {
                        if(!currentNews.getString("ImageUrl").isEmpty())
                          Thumbnail = currentNews.getString("ImageUrl");
                        else
                            Thumbnail=IConstants.NA;
                    }

                    //get the title of the current movie
                    if (Utils.contains(currentNews, "VideoUrl")) {
                        if(!currentNews.getString("VideoUrl").isEmpty())
                        VideoURL = currentNews.getString("VideoUrl");
                        else
                            VideoURL = IConstants.NA;
                    }

                    //get the title of the current movie
                    if (Utils.contains(currentNews, "Content")) {
                        Description = currentNews.getString("Content");
                    }

                    //get the date in theaters for the current movie
                    if (Utils.contains(currentNews, "On")) {
                        PostedOn = currentNews.getString("On");


                    }


                    NewsModal obj_news = new NewsModal();
                    obj_news.setId(id);
                    obj_news.setTitle(Title);
                    Date date = null;
                    try {
                        date = dateFormat.parse(PostedOn);
                    } catch (ParseException e) {
                        //a parse exception generated here will store null in the release date, be sure to handle it
                    }
                    obj_news.setPostedOn(date);
                    obj_news.setContent(Description);
                    obj_news.setImageUrl(Thumbnail);
                    obj_news.setVideoUrl(VideoURL);

                    if (id != -1 && !Title.equals(IConstants.NA)) {
                        listMovies.add(obj_news);
                    }
                }

            } catch (JSONException e) {

            }
//                L.t(getActivity(), listMovies.size() + " rows fetched");
        }
        return listMovies;
    }
}
