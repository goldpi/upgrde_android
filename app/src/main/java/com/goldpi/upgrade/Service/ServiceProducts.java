package com.goldpi.upgrade.Service;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;

import com.goldpi.upgrade.Landing.Products.ProductModal;
import com.goldpi.upgrade.Listner.ILoadedProductsListener;
import com.goldpi.upgrade.Task.TaskLoadProducts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/23/2016.
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class ServiceProducts extends JobService implements ILoadedProductsListener{
    final Logger logger = LoggerFactory.getLogger(ServiceProducts.class);
    private JobParameters jobParameters;
    private ArrayList<ProductModal> productModal;
    @Override
    public void onProductLoaded(ArrayList<ProductModal> listproduct)
    {
        this.productModal=listproduct;
        jobFinished(jobParameters,false);
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        this.jobParameters = params;
        logger.info(this.jobParameters.toString());
        new TaskLoadProducts(this).execute();
        logger.info("Task to Load Product Started");
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
