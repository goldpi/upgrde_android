package com.goldpi.upgrade.Service;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;

import com.goldpi.upgrade.Landing.Articles.ArticleModal;
import com.goldpi.upgrade.Listner.ILoadedArticlesListener;
import com.goldpi.upgrade.Task.TaskLoadArticles;

import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/11/2016.
 */

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class ServiceArticles extends JobService implements ILoadedArticlesListener {
    final org.slf4j.Logger logger = LoggerFactory.getLogger(ServiceArticles.class);
    private JobParameters jobParameters;



    @Override
    public boolean onStartJob(JobParameters params) {
        this.jobParameters=params;
        logger.info(this.jobParameters.toString());
        new TaskLoadArticles(this).execute();
        logger.info("Task to be startde");
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
    @Override
    public void onArticlesLoaded(ArrayList<ArticleModal> listArticles) {
        jobFinished(jobParameters, false);

    }

}
