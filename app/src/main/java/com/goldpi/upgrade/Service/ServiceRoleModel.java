package com.goldpi.upgrade.Service;

import com.goldpi.upgrade.Landing.RoleModel.RoleModelModel;
import com.goldpi.upgrade.Listner.ILoadedRoleModelListener;
import com.goldpi.upgrade.Task.TaskLoadRoleModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import me.tatarka.support.job.JobParameters;
/**
 * Created by Digambar on 5/20/2016.
 */
public class ServiceRoleModel extends me.tatarka.support.job.JobService implements ILoadedRoleModelListener {

    final Logger logger = LoggerFactory.getLogger(ServiceRoleModel.class);
    private JobParameters jobParameters;
    private ArrayList<RoleModelModel> listNews;

    @Override
    public boolean onStartJob(JobParameters jobParameters) {

        this.jobParameters = jobParameters;
        logger.info(this.jobParameters.toString());
        new TaskLoadRoleModel(this).execute();
        logger.info("Task to Load News Started");
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {

        return false;
    }


    @Override
    public void onRoleModelLoaded(ArrayList<RoleModelModel> listRoleModel) {
        this.listNews =listRoleModel;
        // logger.info("OnLoadNews Event",listNews);
        jobFinished(jobParameters, false);
    }



}
