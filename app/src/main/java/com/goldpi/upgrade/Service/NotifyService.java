package com.goldpi.upgrade.Service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;

import android.support.v4.app.NotificationCompat;
import android.util.Log;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.DataModal.Authorization;
import com.goldpi.upgrade.DataModal.NotificationModel;

import com.goldpi.upgrade.MainActivity;
import com.goldpi.upgrade.Network.EndPointURL;
import com.goldpi.upgrade.PrefranceUtility.AccessTokenPrefrance;
import com.goldpi.upgrade.UpgradeApplication;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;


import microsoft.aspnet.signalr.client.ConnectionState;
import microsoft.aspnet.signalr.client.LogLevel;
import microsoft.aspnet.signalr.client.SignalRFuture;
import microsoft.aspnet.signalr.client.hubs.HubConnection;
import microsoft.aspnet.signalr.client.hubs.HubProxy;
import microsoft.aspnet.signalr.client.hubs.SubscriptionHandler1;
import microsoft.aspnet.signalr.client.transport.ClientTransport;
import microsoft.aspnet.signalr.client.transport.ServerSentEventsTransport;
import microsoft.aspnet.signalr.client.Logger;

import static com.goldpi.upgrade.Network.EndPointURL.APP_LOGIN_URL;

/*
 * Written by Imran Ali, imran@goldpi.com
 * Handles Refresh Token
 * Handles Notification
 * Handles SignalR
 */



public class NotifyService extends Service {

    //region Variables

    final static String ACTION = "NotifyServiceAction";
   // final static String STOP_SERVICE_BROADCAST_KEY="StopServiceBroadcastKey";
    //final static int RQS_STOP_SERVICE = 1;
    final Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    private int increment=1;
    private boolean running=false;
    private boolean trying=false;

    HubConnection hubConnection;
    HubProxy notificationHub;

    private boolean thisRunning =false;


    //endregion

    Logger logger = new Logger() {

        @Override
        public void log(String message, LogLevel level) {
           //Log.e(level.name(),message);
        }
    };

    private final String myBlog = "http://app.upgrde.net";

    @Override
    public void onCreate() {
        //notifyServiceReceiver = new NotifyServiceReceiver();
        super.onCreate();
    }
    
    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        /*if(!thisRunning)
        {  return 0;}
        thisRunning =true;*/
        int result = super.onStartCommand(intent, flags, startId);
        InitializeService();
        return result;
    }

    private void InitializeService(){
        //region Code
        final  int time=8*60*1000;
        running= StartSignlaRService();
        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                //region runnable code
                Log.i("Task","Running Sheduler");
                Log.i("Debug Value- Running",running?"true":"false");
                if(!running){
                    running= StartSignlaRService();
                    Log.i("New Value- Running",running?"true":"false");
                }

                    try{
                        Authorization Login = AccessTokenPrefrance.getCurrentUser(UpgradeApplication.getAppContext());
                        String token = Login.getAccess_token();
                        Date date = Calendar.getInstance().getTime();
                        Date exp=Login.getExpireOn();

                        if (date.compareTo(Login.getExpireOn()) ==-1 || date.compareTo(Login.getExpireOn()) == 0) {
                            Log.i("Task","Inside RefeshToken code block");
                            Log.i("Debug Check-trying",  trying?"true":"false");
                            if(!trying) {
                                trying =true;
                                hubConnection.stop();
                                RefreshToken(Login.getRefresh_token());
                                trying=false;
                            }
                        }
                        else {
                            if(date.compareTo(Login.getExpireOn()) ==1 || date.compareTo(Login.getExpireOn()) == 0){
                                AccessTokenPrefrance.clearCurrentUser(UpgradeApplication.getAppContext());
                            }

                        }
                    }catch(Exception e){

                        if (hubConnection!=null)
                            if(hubConnection.getState()== ConnectionState.Connected)
                        hubConnection.stop();
                    }
                finally {
                        handler.postDelayed(this,time);
                    }
            //endregion
            }
            
        };
       r.run();
        //endregion
    }

    private boolean StartSignlaRService() {
        //region Code
        try{
            if(!AccessTokenPrefrance.HasItemsInPrefrance(UpgradeApplication.getAppContext())){
                Log.i("User", "Nothing Found");
                return false;
            }

        //region Fetch Auth
        Authorization Login = AccessTokenPrefrance.getCurrentUser(UpgradeApplication.getAppContext());
        String token = Login.getAccess_token();
        //endregion
        if (token != null && token.length() > 0)
        {
            //region IntialzeConnectionAndHub
            hubConnection = new HubConnection(EndPointURL.APP_NOTIFICATION_URL, "access_token=" + token, true, logger);
            notificationHub = hubConnection.createHubProxy("notificationHub");
            ClientTransport clientTransport = new ServerSentEventsTransport(hubConnection.getLogger());
            //endregion
            //region TryConnect
            if(UpgradeApplication.isNetworkAvailable()){
                SignalRFuture<Void> signalRFuture = hubConnection.start(clientTransport);
                try {
                    signalRFuture.get();
                } catch (InterruptedException | ExecutionException e) {
                    Log.e("SimpleSignalR", e.toString());
                    return false;
                }
            }else{
                return false;
            }

            //endregion
            //region ConnectionEventBindings
            hubConnection.connected(new Runnable() {
                @Override
                public void run() {
                    Log.e("Signalr", "Connected");
                }
            });
            hubConnection.closed(new Runnable() {
                @Override
                public void run() {
                    Log.d("signalr", "DISCONNECTED!");
                    if (hubConnection.getState().toString() == "Disconnected") {
                        new reconnectPulling().execute();
                    }
                }
            });
            //endregion
            
            //region SubscribeHubClienFunction
            notificationHub.on("notification",
                    new SubscriptionHandler1<NotificationModel[]>() {
                        @Override
                        public void run(final NotificationModel[] message) {
                            //region RaiseNotification
                            if (message.length > 0) {
                                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(myBlog));
                                NotificationManager notificationManager =
                                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                Notification notification = null;
                                if (Build.VERSION.SDK_INT > 21) {
                                    //region NotificationSDK21Above
                                    TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
                                    stackBuilder.addParentStack(MainActivity.class);
                                    stackBuilder.addNextIntent(myIntent);
                                    PendingIntent pendingIntent
                                            = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                                    NotificationCompat.Builder builder =
                                            new NotificationCompat.Builder(getApplicationContext())
                                                    .setSmallIcon(R.drawable.messenger_bubble_large_blue, 0)
                                                    .setContentTitle("New Notification")
                                                    .setContentText("New Content for you!")
                                                    .setTicker("Mewaaw")
                                                    .setSound(uri)
                                                    .setNumber(increment++);
                                    NotificationCompat.InboxStyle inbox = new NotificationCompat.InboxStyle();
                                    inbox.setBigContentTitle("Notification");
                                    for (NotificationModel item : message) {
                                        inbox.addLine(item.getTitle() + ":" + item.getMessage());
                                    }
                                    builder.setStyle(inbox);
                                    builder.setContentIntent(pendingIntent);
                                    notification = builder.build();
                                    notificationManager.notify(0, notification);
                                    //endregion
                                } else {
                                    //region NotificationForLowerAPI
                                    for (NotificationModel item : message) {
                                        PendingIntent pendingIntent
                                                = PendingIntent.getActivity(getBaseContext(),
                                                0, myIntent,
                                                Intent.FLAG_ACTIVITY_NEW_TASK);
                                        notification =
                                                new Notification.Builder(getApplicationContext())
                                                        .setContentTitle(item.getTitle())
                                                        .setContentText(item.getMessage()).setSmallIcon(R.drawable.messenger_bubble_large_blue)
                                                        .setContentIntent(pendingIntent)
                                                        .setSound(uri)
                                                        .setAutoCancel(true)
                                                        .build();

                                        notification.flags = notification.flags
                                                | Notification.FLAG_ONGOING_EVENT;
                                        notification.flags |= Notification.FLAG_AUTO_CANCEL;

                                        notificationManager.notify(increment++, notification);

                                    }
                                    //endregion
                                }
                            }
                            //endregion
                        }
                    }
                    , NotificationModel[].class);
            //endregion
        }
        }
        catch (Exception ex)
        {
            return false;
        }
        return true;
        //endregion
    }

    private boolean RefreshToken(final String Token){
        //region Code
        Log.i("Task", "trying refresh");
        if(!AccessTokenPrefrance.HasItemsInPrefrance(UpgradeApplication.getAppContext()))
            return false;
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, APP_LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try
                        {
                            Gson gson = new GsonBuilder().setDateFormat("E, dd MMM yyyy HH:mm:ss z").create();  //.setDateFormat(df6).create();
                            Authorization NewAuthorization = gson.fromJson(response, Authorization.class);
                            AccessTokenPrefrance.clearCurrentUser(UpgradeApplication.getAppContext());
                            AccessTokenPrefrance.setCurrentUser(NewAuthorization, UpgradeApplication.getAppContext());
                            if(NewAuthorization!=null) {
                                trying=false;
                                running=StartSignlaRService();
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.i("ex", ex.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("ex", error.toString());
                    }
                })
        {


            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put("grant_type", "refresh_token");
                params.put("refresh_token", Token);
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        return true;
        //endregion
    }


    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    private class reconnectPulling extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            if(UpgradeApplication.isNetworkAvailable()){
            try {
                Authorization Login= AccessTokenPrefrance.getCurrentUser(UpgradeApplication.getAppContext());
                for (int i = 0; i < 5; i++) {
                    try {
                        if (hubConnection.getState().toString() != "Connected") {
                            hubConnection.start();
                            Log.d("signalr", "Reconnect To Server...");
                            Thread.sleep(5000);
                        }
                    } catch (InterruptedException e) {
                        Thread.interrupted();
                    }
                }

            } catch (Exception ex) {

            }finally {
                return null;
            }
        }
        return null;
        }
    }


}
