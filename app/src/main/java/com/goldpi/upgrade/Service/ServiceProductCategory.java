package com.goldpi.upgrade.Service;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;

import com.goldpi.upgrade.Landing.ProductCategory.ProductCategoryModel;
import com.goldpi.upgrade.Listner.ILoadedProductCategoryListener;
import com.goldpi.upgrade.Task.TaskLoadProductCategory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/23/2016.
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class ServiceProductCategory extends JobService implements ILoadedProductCategoryListener{
    final Logger logger = LoggerFactory.getLogger(ServiceProductCategory.class);
    private JobParameters jobParameters;
    private ArrayList<ProductCategoryModel> pe;
    @Override
    public void onProductCategoryLoaded(ArrayList<ProductCategoryModel> listproductCategory) {
        this.pe=listproductCategory;
        jobFinished(jobParameters,false);
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        this.jobParameters = params;
        logger.info(this.jobParameters.toString());
        new TaskLoadProductCategory(this).execute();
        logger.info("Task to Load Product Category Started");
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
