package com.goldpi.upgrade.Service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.goldpi.upgrade.Landing.News.NewsModal;
import com.goldpi.upgrade.Listner.ILoadedNewsListener;
import com.goldpi.upgrade.Task.TaskLoadNews;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import me.tatarka.support.job.JobParameters;
import me.tatarka.support.job.JobService;

/**
 * Created by Yusuf on 5/2/2016.
 */
public class ServiceNews extends JobService implements ILoadedNewsListener {

    final Logger logger = LoggerFactory.getLogger(ServiceNews.class);
    private JobParameters jobParameters;

    @Override
    public boolean onStartJob(JobParameters jobParameters) {

        this.jobParameters = jobParameters;
        logger.info(this.jobParameters.toString());
        new TaskLoadNews(this).execute();
        logger.info("Task to Load News Started");
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {

        return false;
    }


    @Override
    public void onNewsLoaded(ArrayList<NewsModal> listNews) {
       // logger.info("OnLoadNews Event",listNews);
        jobFinished(jobParameters, false);
    }


}
