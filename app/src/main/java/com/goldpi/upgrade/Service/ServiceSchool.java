package com.goldpi.upgrade.Service;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;

import com.goldpi.upgrade.Account.Registration.School.SchoolModel;
import com.goldpi.upgrade.Listner.ILoadedSchool;
import com.goldpi.upgrade.Task.TaskLoadSchool;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/29/2016.
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class ServiceSchool extends JobService implements ILoadedSchool{
    final Logger logger = LoggerFactory.getLogger(ServiceSchool.class);
    private JobParameters jobParameters;
    private ArrayList<SchoolModel>listschool;
    @Override
    public void onSchoolLoaded(ArrayList<SchoolModel> listschool) {
        this.listschool=listschool;
        jobFinished(jobParameters,false);
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        this.jobParameters=params;
        logger.info(this.jobParameters.toString());
        new TaskLoadSchool(this).execute();
        logger.info("Task To load School Started");
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }
}
