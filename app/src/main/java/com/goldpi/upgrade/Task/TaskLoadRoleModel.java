package com.goldpi.upgrade.Task;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;
import com.goldpi.upgrade.Landing.RoleModel.RoleModelLoader;
import com.goldpi.upgrade.Landing.RoleModel.RoleModelModel;
import com.goldpi.upgrade.Listner.ILoadedRoleModelListener;
import com.goldpi.upgrade.Network.VolleySingleton;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/20/2016.
 */
public class TaskLoadRoleModel  extends AsyncTask<Void, Void, ArrayList<RoleModelModel>> {
    private ILoadedRoleModelListener mLoadedRoleModelListener;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;


    public TaskLoadRoleModel(ILoadedRoleModelListener listNews) {

        this.mLoadedRoleModelListener = listNews;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected ArrayList<RoleModelModel> doInBackground(Void... params) {
        ArrayList<RoleModelModel> listNews = RoleModelLoader.loadRoleModel(requestQueue);
        return listNews;
    }

    @Override
    protected void onPostExecute(ArrayList<RoleModelModel> listNews) {
        if (mLoadedRoleModelListener != null) {
            mLoadedRoleModelListener.onRoleModelLoaded(listNews);
        }
    }
}
