package com.goldpi.upgrade.Task;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;
import com.goldpi.upgrade.Account.Registration.School.SchoolLoader;
import com.goldpi.upgrade.Account.Registration.School.SchoolModel;
import com.goldpi.upgrade.Listner.ILoadedSchool;
import com.goldpi.upgrade.Network.VolleySingleton;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/29/2016.
 */
public class TaskLoadSchool extends AsyncTask<Void,Void,ArrayList<SchoolModel>> {
    private ILoadedSchool iLoadedSchool;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;

    public TaskLoadSchool(ILoadedSchool loadedSchool)
    {
        this.iLoadedSchool=loadedSchool;
        this.volleySingleton=VolleySingleton.getInstance();
        requestQueue=volleySingleton.getRequestQueue();

    }
    @Override
    protected ArrayList<SchoolModel> doInBackground(Void... params) {
        ArrayList<SchoolModel> list= SchoolLoader.loadSchool(requestQueue);
        return  list;

    }
    @Override
    protected void onPostExecute(ArrayList<SchoolModel> listschool)
    {

        if(iLoadedSchool!=null)
        {
            iLoadedSchool.onSchoolLoaded(listschool);


        }
    }
}
