package com.goldpi.upgrade.Task;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;
import com.goldpi.upgrade.Landing.Articles.ArticleLoader;
import com.goldpi.upgrade.Landing.Articles.ArticleModal;
import com.goldpi.upgrade.Listner.ILoadedArticlesListener;
import com.goldpi.upgrade.Network.VolleySingleton;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/11/2016.
 */
public class TaskLoadArticles extends AsyncTask<Void,Void,ArrayList<ArticleModal>> {

    private ILoadedArticlesListener mILoadedArticlesListener;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;

    public TaskLoadArticles(ILoadedArticlesListener listArticles)
    {
        this.mILoadedArticlesListener=listArticles;
        volleySingleton=VolleySingleton.getInstance();
        requestQueue=volleySingleton.getRequestQueue();

    }
    @Override
    protected ArrayList<ArticleModal> doInBackground(Void... params) {
        ArrayList<ArticleModal> listArticle= null;
        listArticle = ArticleLoader.loadArticle(requestQueue);
        return listArticle;
    }
    protected void onPostExecute(ArrayList<ArticleModal> listArticles) {
        if (mILoadedArticlesListener != null) {
            mILoadedArticlesListener.onArticlesLoaded(listArticles);
        }
    }
}
