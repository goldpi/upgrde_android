package com.goldpi.upgrade.Task;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;

import com.goldpi.upgrade.Landing.ProductCategory.ProductCategoryLoader;
import com.goldpi.upgrade.Landing.ProductCategory.ProductCategoryModel;
import com.goldpi.upgrade.Listner.ILoadedProductCategoryListener;
import com.goldpi.upgrade.Network.VolleySingleton;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/23/2016.
 */
public class TaskLoadProductCategory extends AsyncTask<Void,Void,ArrayList<ProductCategoryModel>> {
    private ILoadedProductCategoryListener iLoadedProductCategoryListener;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    public TaskLoadProductCategory(ILoadedProductCategoryListener iLoadedProductCategoryListener)
    {

        this.iLoadedProductCategoryListener=iLoadedProductCategoryListener;
        volleySingleton=VolleySingleton.getInstance();
        requestQueue=volleySingleton.getRequestQueue();
    }
    @Override
    protected ArrayList<ProductCategoryModel> doInBackground(Void... params) {
        ArrayList<ProductCategoryModel> listproductCategory= null;
        listproductCategory = ProductCategoryLoader.loadProductCategory(requestQueue);
        return listproductCategory;
    }
    @Override
    protected void onPostExecute(ArrayList<ProductCategoryModel> listproduct)
    {
        if (iLoadedProductCategoryListener != null) {
            iLoadedProductCategoryListener.onProductCategoryLoaded(listproduct);
        }
    }
}
