package com.goldpi.upgrade.Task;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;
import com.goldpi.upgrade.Landing.Products.ProductLoader;
import com.goldpi.upgrade.Landing.Products.ProductModal;
import com.goldpi.upgrade.Listner.ILoadedProductsListener;
import com.goldpi.upgrade.Network.VolleySingleton;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/20/2016.
 */
public class TaskLoadProducts extends AsyncTask<Void,Void,ArrayList<ProductModal>> {
    private ILoadedProductsListener miloadedproductlistener;
    private VolleySingleton mvolleysingleton;

    private RequestQueue mrequestquaue;
    public TaskLoadProducts()
    {
        String data;
    }
    public TaskLoadProducts(String data)
    {

    }

    public TaskLoadProducts(ILoadedProductsListener listproduct)
    {
        this.miloadedproductlistener=listproduct;
        mvolleysingleton=VolleySingleton.getInstance();
        mrequestquaue=mvolleysingleton.getRequestQueue();
    }

    @Override
    protected ArrayList<ProductModal> doInBackground(Void... params) {
        ArrayList<ProductModal> listproduct= ProductLoader.loadProduct(mrequestquaue);
        return listproduct;
    }

    @Override
    protected void onPostExecute(ArrayList<ProductModal> listproduct)
    {
        if (miloadedproductlistener != null) {
            miloadedproductlistener.onProductLoaded(listproduct);
        }
    }
}
