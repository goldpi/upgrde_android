package com.goldpi.upgrade.Task;

import android.os.AsyncTask;

import com.android.volley.RequestQueue;
import com.goldpi.upgrade.Landing.News.NewsLoader;
import com.goldpi.upgrade.Landing.News.NewsModal;
import com.goldpi.upgrade.Listner.ILoadedNewsListener;
import com.goldpi.upgrade.Network.VolleySingleton;

import java.util.ArrayList;

/**
 * Created by Yusuf on 5/1/2016.
 */
public class TaskLoadNews extends AsyncTask<Void, Void, ArrayList<NewsModal>> {
    private ILoadedNewsListener mLoadedNewsListener;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;


    public TaskLoadNews(ILoadedNewsListener listNews) {

        this.mLoadedNewsListener = listNews;
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
    }

    @Override
    protected ArrayList<NewsModal> doInBackground(Void... params) {
        ArrayList<NewsModal> listNews = NewsLoader.loadNews(requestQueue);
        return listNews;
    }

    @Override
    protected void onPostExecute(ArrayList<NewsModal> listNews) {
        if (mLoadedNewsListener != null) {
            mLoadedNewsListener.onNewsLoaded(listNews);
        }
    }
}
