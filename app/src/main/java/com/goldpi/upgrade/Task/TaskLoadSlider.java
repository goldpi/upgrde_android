package com.goldpi.upgrade.Task;

import android.os.AsyncTask;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.goldpi.upgrade.Landing.HomeSlider.SliderLoader;
import com.goldpi.upgrade.Landing.HomeSlider.SliderModal;
import com.goldpi.upgrade.Network.VolleySingleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Created by Yusuf on 5/24/2016.
 */
public class TaskLoadSlider extends AsyncTask<Void, Void, ArrayList<SliderModal>> {

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private String mSliderUrl;
    public TaskLoadSlider(String SliderURL) {
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        mSliderUrl=SliderURL;
    }
    @Override
    protected ArrayList<SliderModal> doInBackground(Void... params) {
        ArrayList<SliderModal> Slider = SliderLoader.loadSlides(requestQueue);
        return Slider;
    }

    @Override
    protected void onPostExecute(ArrayList<SliderModal> sliderModals) {
        Logger logger = LoggerFactory.getLogger(TaskLoadSlider.class);
        logger.info(sliderModals.toArray().toString());
    }
}
