package com.goldpi.upgrade.Account.Registration.School;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Digambar on 5/28/2016.
 */
public class SchoolModel implements Parcelable{
    private int SchoolId;
    private String SchoolName;

    public SchoolModel(Parcel in) {
        SchoolId = in.readInt();
        SchoolName = in.readString();
    }
    public SchoolModel()
    {

    }

    public static final Creator<SchoolModel> CREATOR = new Creator<SchoolModel>() {
        @Override
        public SchoolModel createFromParcel(Parcel in) {
            return new SchoolModel(in);
        }

        @Override
        public SchoolModel[] newArray(int size) {
            return new SchoolModel[size];
        }
    };

    public int getSchoolId() {
        return SchoolId;
    }

    public String getSchoolName() {
        return SchoolName;
    }

    public void setSchoolName(String schoolName) {
        SchoolName = schoolName;
    }

    public void setSchoolId(int schoolId) {
        SchoolId = schoolId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(SchoolId);
        dest.writeString(SchoolName);
    }
}
