package com.goldpi.upgrade.Account;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.goldpi.upgrade.Icon.Iconicfy;
import com.goldpi.materialui.R;
import com.joanzapata.iconify.widget.IconButton;

public class AccountLandlingActivity extends AppCompatActivity {
     IconButton btnOpneLoginActivity;
     IconButton btnOpneSignUpActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_landling);
        Iconicfy.SetIconify();

         btnOpneLoginActivity = (IconButton) findViewById(R.id.btn_Open_login);
        btnOpneLoginActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Login = new Intent(getBaseContext(), LoginActivity.class);
                startActivity(Login);
            }
        });

        btnOpneSignUpActivity = (IconButton) findViewById(R.id.btn_Open_SignUp);
        btnOpneSignUpActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent  Login=new Intent(getBaseContext(),SignUpActivity.class);
                startActivity(Login);
            }
        });

    }


}
