package com.goldpi.upgrade.Account;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.DataModal.Authorization;
import com.goldpi.upgrade.Home.HomeActivity;
import com.goldpi.upgrade.Icon.Iconicfy;
import com.goldpi.upgrade.Network.HttpStringRequestor;
import com.goldpi.upgrade.PrefranceUtility.AccessTokenPrefrance;
import com.goldpi.upgrade.Service.NotifyService;
import com.goldpi.upgrade.UIHelper.FullScreen;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.joanzapata.iconify.widget.IconButton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.goldpi.upgrade.Network.EndPointURL.APP_LOGIN_URL;
import static com.goldpi.upgrade.Network.EndPointURL.GrantType;
import static com.goldpi.upgrade.Network.EndPointURL.Password;
import static com.goldpi.upgrade.Network.EndPointURL.Username;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,Response.Listener,Response.ErrorListener {
    //create instance of Logger to use to logging the application activity
    final Logger logger = LoggerFactory.getLogger(LoginActivity.class);

    @Bind(R.id.input_email_LoginActivity) EditText mUsername;
    @Bind(R.id.input_password_LoginActivity)EditText mPassword;
    @Bind(R.id.btn_login_LoginActivity) IconButton mbtnLogin;
   // @Bind(R.id.lblerrormsg)TextView merrormsg;
    private Toolbar toolbar;
    private TextView mforgetPasswordLink;
    private TextView msignUpLink;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        Iconicfy.SetIconify();
        setToolbar();
        FullScreen.Hide(this);

        //BindControl();




    }


    public void authentication()
    {
        HttpStringRequestor stringRequest =new HttpStringRequestor(Request.Method.POST,APP_LOGIN_URL,this,this) {

            @Override
            protected Map<String,String> getParams() {
                Map<String,String> params = new HashMap<String, String>();
                params.put(Username,mUsername.getText().toString());
                params.put(Password,mPassword.getText().toString());
                params.put(GrantType, "password");
                return params;
            }

        };
    }

    public void DoLogin()
    {
      //  final String username = editTextUsername.getText().toString().trim();
      //  final String password = editTextPassword.getText().toString().trim();
       // final String email = editTextEmail.getText().toString().trim();

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, APP_LOGIN_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        logger.info(response);


                        try
                        {

                            Gson gson = new GsonBuilder().setDateFormat("E, dd MMM yyyy HH:mm:ss z").create();  //.setDateFormat(df6).create();
                            Authorization NewAuthorization = gson.fromJson(response, Authorization.class);
                            AccessTokenPrefrance.setCurrentUser(NewAuthorization, getBaseContext());
                            if(NewAuthorization!=null) {
                                Intent HomeScreen = new Intent(LoginActivity.this, HomeActivity.class);
                                startActivity(HomeScreen);

                                Intent service= new Intent(LoginActivity.this, NotifyService.class);
                                LoginActivity.this.startService(service);

                            }
                        }
                        catch (Exception ex)
                        {
                            logger.info(ex.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        logger.info(error.toString());

                    }
                })
        {


            @Override
            protected Map<String,String> getParams(){
                Map<String,String> params = new HashMap<String, String>();
                params.put(Username,mUsername.getText().toString());
                params.put(Password,mPassword.getText().toString());
                params.put(GrantType, "password");
                return params;
            }

        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }



    public void setToolbar()
    {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Login");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_guest, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_Notification) {

            return true;
        }
        if(id==android.R.id.home)
        {
            NavUtils.navigateUpFromSameTask(this);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
@OnClick({R.id.btn_login_LoginActivity,R.id.link_forgetPassword_LoginActivity,R.id.link_signup__LoginActivity})
    public void onClick(View v) {
        int id=v.getId();
        switch(id)
        {
            case R.id.btn_login_LoginActivity:
            {
                logger.info("Login button clicked");
               DoLogin();
                //authentication();
                break;
            }
            case R.id.link_forgetPassword_LoginActivity :
            {

                Intent intentForgetPassword=new Intent(getBaseContext(),ForgetPasswordActivity.class);
                startActivity(intentForgetPassword);
                break;
            }
            case R.id.link_signup__LoginActivity:
            {
                this.finish();
                Intent intentSignup=new Intent(getBaseContext(),SignUpActivity.class);
                startActivity(intentSignup);
                break;
            }
            default:
            {
                break;
            }
        }
    }

    @Override
    public void onResponse(Object response) {


String res=response.toString();
        logger.info(res);

        try
        {

            Gson gson = new GsonBuilder().setDateFormat("E, dd MMM yyyy HH:mm:ss").create();  //.setDateFormat(df6).create();
            Authorization NewAuthorization = gson.fromJson(res, Authorization.class);
            AccessTokenPrefrance.setCurrentUser(NewAuthorization, getBaseContext());
            if(NewAuthorization!=null) {
                Intent HomeScreen = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(HomeScreen);
            }
        }
        catch (Exception ex)
        {
            logger.info(ex.toString());
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        logger.info(error.toString());
    }
}
