package com.goldpi.upgrade.Account.Registration;

/**
 * Created by Digambar on 5/27/2016.
 */
public class RegisterModel {
    private String Name;
    private String Phone;
    private String Email;
    private int SchoolId;
    private int ClassId;
    private String Gender;

    public RegisterModel()
    {

    }
    public RegisterModel(String Name,String Phone, String Email,int SchoolId,int ClassId,String Gender)
    {

        this.Name=Name;
        this.Phone=Phone;
        this.Email=Email;
        this.SchoolId= SchoolId;
        this.ClassId=ClassId;
        this.Gender=Gender;
    }
    public String getName() {
        return Name;
    }

    public String getPhone() {
        return Phone;
    }

    public String getEmail() {
        return Email;
    }

    public int getClassName() {
        return ClassId;
    }

    public int getSchoolName() {
        return SchoolId;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public void setClassName(String className) {
        ClassId = ClassId;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public void setSchoolName(String schoolName) {
        SchoolId = SchoolId;
    }

    public String getGender() {
        return Gender;
    }


    public void setEmail(String email) {
        Email = email;
    }

}
