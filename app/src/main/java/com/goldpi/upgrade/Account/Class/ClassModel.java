package com.goldpi.upgrade.Account.Class;

/**
 * Created by Digambar on 5/28/2016.
 */
public class ClassModel {
    private int Id;
    private String ClassName;

    public int getId() {
        return Id;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public void setId(int id) {
        Id = id;
    }
}
