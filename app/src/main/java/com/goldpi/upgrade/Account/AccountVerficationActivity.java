package com.goldpi.upgrade.Account;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.goldpi.materialui.R;
import com.goldpi.upgrade.Icon.Iconicfy;
import com.joanzapata.iconify.widget.IconButton;

public class AccountVerficationActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private IconButton mbtnSubmitVerificationCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verfication);
        Iconicfy.SetIconify();
        setToolbar();
        BindControl();

    }
    public void setToolbar()
    {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Account Verification");
    }

    public void BindControl()
    {
        mbtnSubmitVerificationCode=(IconButton)findViewById(R.id.btn_SubmitVerificationCode_AccountVerificationActivity);
        mbtnSubmitVerificationCode.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_guest, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_Notification) {
            return true;
        }
        if(id==android.R.id.home)
        {
            NavUtils.navigateUpFromSameTask(this);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) { int id=v.getId();
        switch(id)
        {
            case R.id.btn_SubmitVerificationCode_AccountVerificationActivity :
            {

                Intent intentNewPassword=new Intent(getBaseContext(),NewPasswordActivity.class);
                startActivity(intentNewPassword);
                break;
            }

            default:
            {
                break;
            }
        }

    }
}
