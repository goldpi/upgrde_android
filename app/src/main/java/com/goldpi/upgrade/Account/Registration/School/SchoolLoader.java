package com.goldpi.upgrade.Account.Registration.School;

import com.android.volley.RequestQueue;
import com.goldpi.upgrade.Network.HTTPJSONRequestor;
import com.goldpi.upgrade.Parser.SchoolParser;
import com.goldpi.upgrade.UpgradeApplication;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static com.goldpi.upgrade.Network.EndPointURL.APP_COMPUTER_CREATIVE_URL;

/**
 * Created by Digambar on 5/28/2016.
 */
public class SchoolLoader {
    final static Logger logger = LoggerFactory.getLogger(SchoolLoader.class);
    public static ArrayList<SchoolModel> loadSchool (RequestQueue requestQueue) {
        JSONArray response= HTTPJSONRequestor.requestJSON(requestQueue, APP_COMPUTER_CREATIVE_URL);
        logger.info("load School", response);
        ArrayList<SchoolModel> listSchool= SchoolParser.parseSchoolJSON(response);
        if(listSchool.size()>0)
        {
            UpgradeApplication.getWritableDatabase().InsertSchool(listSchool, true);
        }
        else
        {
           listSchool=UpgradeApplication.getWritableDatabase().readSchool();
        }

        return listSchool;

    }
}
