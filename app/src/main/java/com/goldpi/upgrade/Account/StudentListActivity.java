package com.goldpi.upgrade.Account;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.goldpi.materialui.R;

import java.util.ArrayList;
import java.util.List;

public class StudentListActivity extends AppCompatActivity {
    RecyclerView mRecyclerView;
    MyRecyclerAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list);


        // Initialize recycler view
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        MyRecyclerAdapter adapter = new MyRecyclerAdapter(getBaseContext(), getStudents());
        mRecyclerView.setAdapter(adapter);



    }

    public List<Students> getStudents()
    {
        List<Students> studentList=new ArrayList<>();
        for (int i=1;i<=100; i++) {
            Students newStudent=new Students();
            newStudent.setID(i);
            newStudent.setName("Student Name :- "+i);
            newStudent.setCity("City :-"+i);
            studentList.add(newStudent);
        }


        return studentList;
    }
}
