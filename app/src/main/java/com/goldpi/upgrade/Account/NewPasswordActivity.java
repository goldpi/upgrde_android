package com.goldpi.upgrade.Account;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.goldpi.upgrade.Home.HomeActivity;
import com.goldpi.upgrade.Icon.Iconicfy;
import com.goldpi.materialui.R;
import com.joanzapata.iconify.widget.IconButton;
import com.joanzapata.iconify.widget.IconTextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewPasswordActivity extends AppCompatActivity implements View.OnClickListener{
    @Bind(R.id.btn_Submit_Password_NewassswordActivity)IconButton mSubmitNewPass;
    @Bind(R.id.lbl_Operation_Message_forget_password_Activity) IconTextView mOperationMessage;
    @Bind(R.id.txt_Password_NewPasswordActivity)EditText mNewPassword;
    @Bind(R.id.txt_Re_Password_NewPasswordActivity)EditText mRe_TypeNewPassword;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);
        Iconicfy.SetIconify();
        setToolbar();
        ButterKnife.bind(this);
        //BindControl();

    }

    public void BindControl()
    {
        mSubmitNewPass.setOnClickListener(this);
    }

    public void setToolbar()
    {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Set New Password");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_guest, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_Notification) {
            return true;
        }
        if(id==android.R.id.home)
        {
            NavUtils.navigateUpFromSameTask(this);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    @OnClick(R.id.btn_Submit_Password_NewassswordActivity)
    public void onClick(View v) {
int id=v.getId();
        switch (id)
        {
            case R.id.btn_Submit_Password_NewassswordActivity:
            {

                Intent intentSignup=new Intent(getBaseContext(), HomeActivity.class);
                startActivity(intentSignup);
                break;
            }
        }
    }
}
