package com.goldpi.upgrade.Account;

/**
 * Created by Yusuf on 4/1/2016.
 */
public class Students {
    private String Name;
    private int Id;
    private String City;

    public void setName(String name)
    {
        this.Name=name;
    }
    public String getName()
    {
        return this.Name;
    }

   public void setID(int  id)
    {
        this.Id=id;
    }
    public int getId()
    {
        return this.Id;
    }

    public void setCity(String city)
    {
        this.City=city;
    }
    public String getCity()
    {
        return this.City;
    }
}
