package com.goldpi.upgrade.Account;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Account.Class.ClassModel;
import com.goldpi.upgrade.Account.Registration.School.SchoolModel;
import com.goldpi.upgrade.Home.HomeActivity;
import com.goldpi.upgrade.Icon.Iconicfy;
import com.goldpi.upgrade.Listner.ILoadedSchool;
import com.goldpi.upgrade.PrefranceUtility.AccessClassTokenPreferance;
import com.goldpi.upgrade.Service.NotifyService;
import com.goldpi.upgrade.Task.TaskLoadSchool;
import com.goldpi.upgrade.UpgradeApplication;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.joanzapata.iconify.widget.IconButton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.goldpi.upgrade.Network.EndPointURL.APP_LOAD_CLASS_URL;
import static com.goldpi.upgrade.Network.EndPointURL.ClassId;
import static com.goldpi.upgrade.Network.EndPointURL.ClassName;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, Response.Listener,Response.ErrorListener,ILoadedSchool{
    final Logger logger = LoggerFactory.getLogger(SignUpActivity.class);
    @Bind(R.id.ddlClass_SignUpActivity) Spinner mClasses;
    @Bind(R.id.ddlBoard_SignUpActivity) Spinner mSchool; // may be error
    @Bind(R.id.btn_Submit_SignupActivity) IconButton mbtnSubmit;
    @Bind(R.id.link_login_SignupActivity) TextView mLoginLink;
    //@Bind(R.id.U)
    private Toolbar toolbar;
    String firstItem;
    private ArrayList<String> school;
    private static final String School_List = "School";
    private ArrayList<SchoolModel> schoollist = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Iconicfy.SetIconify();
        setToolbar();
        ButterKnife.bind(this);
        schoollist=new ArrayList<>();
       // ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.Classes, R.layout.spinner_item);
      //  mClasses.setAdapter(adapter);
       // ArrayAdapter Boardadapter = ArrayAdapter.createFromResource(this, R.array.Boards, R.layout.spinner_item);
       //mSchool.setAdapter(Boardadapter);
//error section
        if (savedInstanceState != null) {
            logger.info("Loading Product Category from Saved Instance State");

            //if this fragment starts after a rotation or configuration change, load the existing news from a parcelable class
            // mproductList = savedInstanceState.getParcelableArrayList(PRODUCT_CATEGORY);
            schoollist = savedInstanceState.getParcelableArrayList(School_List);
            if (schoollist.isEmpty()) {
                //load Product Category from web server
                new TaskLoadSchool(this).execute();
            }

        } else {
            //if this fragment starts for the first time, load the list of Product Category from the database
            schoollist = UpgradeApplication.getWritableDatabase().readSchool();
            //if the database is empty, trigger an AsycnTask to download Product Category list from the web
            if (schoollist.isEmpty()) {
                //load Product Category from web server

                new TaskLoadSchool(this).execute();
            }
        }
        //update your Adapter to containg the retrieved news
      //  mProductAdapter.setProductCategory(mproductList);
       // mSchool.
        mSchool.setAdapter(new ArrayAdapter<String>(SignUpActivity.this,android.R.layout.simple_spinner_dropdown_item,school ));


    }

    public void setToolbar()
    {
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sign Up");
    }

    //============================== Class List Section=====================================
    public void ClassList()
    {

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, APP_LOAD_CLASS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        logger.info(response);


                        try
                        {

                            Gson gson = new GsonBuilder().create();  //.setDateFormat(df6).create();
                            ClassModel classModel = gson.fromJson(response, ClassModel.class);
                            AccessClassTokenPreferance.setCurrentClass(classModel, getBaseContext());
                            if(classModel!=null) {
                                Intent HomeScreen = new Intent(SignUpActivity.this, HomeActivity.class);
                                startActivity(HomeScreen);

                                Intent service= new Intent(SignUpActivity.this, NotifyService.class);
                                SignUpActivity.this.startService(service);

                            }
                        }
                        catch (Exception ex)
                        {
                            logger.info(ex.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        logger.info(error.toString());

                    }

                }) {


            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(ClassId,ClassId); //error suspected code

                params.put(ClassName, ClassName);
             //   params.put(GrantType, "password");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }

    @Override
    public void onResponse(Object response) {
        String res=response.toString();
        logger.info(res);

        try
        {

            Gson gson = new GsonBuilder().create();  //.setDateFormat(df6).create();
            ClassModel classModel = gson.fromJson(res, ClassModel.class);
            AccessClassTokenPreferance.setCurrentClass(classModel, getBaseContext());
            if(classModel!=null) {
                Intent HomeScreen = new Intent(SignUpActivity.this, HomeActivity.class);
                startActivity(HomeScreen);
            }
        }
        catch (Exception ex)
        {
            logger.info(ex.toString());
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        logger.info(error.toString());
    }


    @Override
    public void onSchoolLoaded(ArrayList<SchoolModel> listschool) {

    }


    // ====================Registration Section===========================
/*
    public void Registration()
    {

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, APP_LOAD_CLASS_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        logger.info(response);


                        try
                        {

                            Gson gson = new GsonBuilder().create();  //.setDateFormat(df6).create();
                            RegisterModel registerModel = gson.fromJson(response, RegisterModel.class);
                            AccessRegistrationPreferance.setCurrentRegistration(registerModel, getBaseContext());
                            if(registerModel!=null) {
                                Intent HomeScreen = new Intent(SignUpActivity.this, HomeActivity.class);
                                startActivity(HomeScreen);

                                Intent service= new Intent(SignUpActivity.this, NotifyService.class);
                                SignUpActivity.this.startService(service);

                            }
                        }
                        catch (Exception ex)
                        {
                            logger.info(ex.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        logger.info(error.toString());

                    }

                }) {

            // code to be reviewd

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put(ClassId,ClassId); //error suspected code

                params.put(ClassName, ClassName);
                //   params.put(GrantType, "password");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }

    @Override
    public void onResponse(Object response) {
        String res=response.toString();
        logger.info(res);

        try
        {

            Gson gson = new GsonBuilder().create();  //.setDateFormat(df6).create();
            RegisterModel registerModel = gson.fromJson(res, RegisterModel.class);
            AccessRegistrationPreferance.setCurrentRegistration(registerModel, getBaseContext());
            if(registerModel!=null) {
                Intent HomeScreen = new Intent(SignUpActivity.this, HomeActivity.class);
                startActivity(HomeScreen);
            }
        }
        catch (Exception ex)
        {
            logger.info(ex.toString());
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        logger.info(error.toString());
    }


    @Override
    public void onRegisterLoaded(ArrayList<RegisterModel> listregistration) {

    }
*/

    //================= Menu Section==============================
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_guest, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_Notification) {
            return true;
        }
        if(id==android.R.id.home)
        {
            NavUtils.navigateUpFromSameTask(this);
        }

        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.link_login_SignupActivity,R.id.btn_Submit_SignupActivity})
    public void onClick(View v) {
        int id=v.getId();
        switch(id)
        {
            case R.id.link_login_SignupActivity :
            {

                Intent intentLogin=new Intent(getBaseContext(), LoginActivity.class);
                startActivity(intentLogin);
                break;
            }
            case R.id.btn_Submit_SignupActivity:
            {


                break;
            }
            default:
            {
                break;
            }
        }
    }





}

