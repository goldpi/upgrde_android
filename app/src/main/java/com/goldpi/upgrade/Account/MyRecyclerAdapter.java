package com.goldpi.upgrade.Account;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.goldpi.materialui.R;

import java.util.List;

/**
 * Created by Yusuf on 4/1/2016.
 */
public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.CustomViewHolder> {
    private List<Students> mstudentList;
    private Context mContext;
    public MyRecyclerAdapter(Context context, List<Students> studentList) {
        this.mContext=context;
        this.mstudentList=studentList;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_list_view, null);

        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        Students studentsDetail=mstudentList.get(position);
holder.textId.setText(Integer.toString(studentsDetail.getId()));
        holder.textName.setText(studentsDetail.getName());
        holder.textCity.setText(studentsDetail.getCity());
    }

    @Override
    public int getItemCount() {
        return (null != mstudentList ? mstudentList.size() : 0);
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView textId;
        protected TextView textName;
        protected TextView textCity;
        public CustomViewHolder(View itemView) {
            super(itemView);
            this.textId= (TextView) itemView.findViewById(R.id.studentID);
            this.textName= (TextView) itemView.findViewById(R.id.studentName);
            this.textCity= (TextView) itemView.findViewById(R.id.studentCity);
        }
    }
}
