package com.goldpi.upgrade;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.goldpi.upgrade.DB.DatabaseContext;
import com.goldpi.upgrade.Landing.News.NewsModal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Yusuf on 4/13/2016.
 */
public class UpgradeApplication extends Application {

    public static final String KEY_YouTubeAPI="AIzaSyBbV5pSgX7PqdKeNdabULZZ15ab2T4TKvU";
    private static UpgradeApplication sInstance;
    public static Logger logger;
    private static DatabaseContext mDC;

    public static UpgradeApplication getInstance() {
        return sInstance;
    }

    public static Context getAppContext() {
        return sInstance.getApplicationContext();
    }

    public synchronized static DatabaseContext getWritableDatabase() {
        if (mDC == null) {
            mDC = new DatabaseContext(getAppContext());
        }
        return mDC;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        logger=LoggerFactory.getLogger("Upgrade");
        sInstance = this;
        mDC = new DatabaseContext(this);

        //https://www.simplifiedcoding.net/login-with-facebook-android-studio-using-facebook-sdk-4/
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        printHashKey();

    }

    public void printHashKey() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.goldpi.upgrade", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    public static void StartAllService(){

    }

    public static boolean isNetworkAvailable() {
        Log.d("INTERNET_CONNECTION","Cheking Internet");
        ConnectivityManager connectivityManager
                = (ConnectivityManager) sInstance.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
