package com.goldpi.upgrade.Landing.HomeSlider;

import java.util.ArrayList;

import java.util.ArrayList;

/**
 * Created by Yusuf on 5/6/2016.
 */
public class SliderModal {
    //@SerializedName("userName")
    private String Title;
    private ArrayList<SlicesModel> Slices;
public SliderModal()
{

}
  
    public SliderModal(String title,ArrayList<SlicesModel> Slices)
    {

        this.Title=title;
        this.Slices=Slices;
    }

    public String getTitle() {
        return Title;
    }
    public void setTitle(String title) {
        Title = title;
    }

    public void setSlices(ArrayList<SlicesModel> slices) {
        Slices = slices;
    }

    public ArrayList<SlicesModel> getSlices() {
        return Slices;
    }
}

