package com.goldpi.upgrade.Landing.Activity;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.bluejamesbond.text.DocumentView;
import com.bluejamesbond.text.style.TextAlignment;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Landing.News.NewsModal;
import com.goldpi.upgrade.Network.VolleySingleton;
import com.goldpi.upgrade.UpgradeApplication;
import com.goldpi.upgrade.Utility.IConstants;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.Bind;
import butterknife.ButterKnife;

//import com.goldpi.upgrade.Network.VolleySingleton;

public class NewsDetailActivity extends AppCompatActivity{
    NewsModal News;
    CollapsingToolbarLayout collapsingToolbarLayout;
    Toolbar mtoolbar;
  //  @Bind(R.id.txtNewsDescription)TextView NewsDetail;
    @Bind(R.id.txtNewsTitle)TextView NewsTitle;
    @Bind(R.id.NewsImage) ImageView NewsImage;
   //@Bind(R.id.docDescription) DocumentView Description;
    private VolleySingleton mVolleySingleton;
    private ImageLoader mImageLoader;
    final static Logger logger = LoggerFactory.getLogger(NewsDetailActivity.class);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        int NewsID =Integer.parseInt(getIntent().getExtras().getString("NewsID"));
        News = UpgradeApplication.getWritableDatabase().getSingleNews(NewsID);
        ButterKnife.bind(this);
        setToolbar();
        setupCollapsingToolbarLayout();
       // NewsDetail.setText(News.getContent());
        NewsTitle.setText(News.getTitle());

        mVolleySingleton = VolleySingleton.getInstance();
        mImageLoader = mVolleySingleton.getImageLoader();
         if(News.getImageUrl().equals(IConstants.NA))
            NewsImage.setVisibility(View.GONE);
         else
            loadImages(News.getImageUrl());


LinearLayout NewsContainer=(LinearLayout)findViewById(R.id.layoutNewsContainer);
      //DocumentView
        DocumentView     Description= new DocumentView(this, DocumentView.PLAIN_TEXT);  // Support plain text
        Description.getDocumentLayoutParams().setTextAlignment(TextAlignment.JUSTIFIED);
        Description.getDocumentLayoutParams().setTextColor(R.color.blue_grey_700);
        Description.getDocumentLayoutParams().setInsetPaddingLeft(10F);
        Description.getDocumentLayoutParams().setInsetPaddingRight(10F);
        Description.setText(News.getContent()); // Set to `true` to enable justification

        Description.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        NewsContainer.addView(Description);

    }


    private void setupCollapsingToolbarLayout(){

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        if(collapsingToolbarLayout != null){
            collapsingToolbarLayout.setTitle(mtoolbar.getTitle());
            //collapsingToolbarLayout.setCollapsedTitleTextColor(0xED1C24);
            //collapsingToolbarLayout.setExpandedTitleColor(0xED1C24);
        }
    }

    public void setToolbar()
    {
        mtoolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mtoolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(new IconDrawable(this, FontAwesomeIcons.fa_arrow_left)
                .colorRes(R.color.white)
                .actionBarSize());
        ab.setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_Notification) {

            return true;
        }
        if(id==android.R.id.home)
        {
            //NavUtils.navigateUpFromSameTask(this);
            this.finish();
        }

        return super.onOptionsItemSelected(item);
    }


    private void loadImages(String urlThumbnail) {
        if (!urlThumbnail.equals(IConstants.NA)) {
            mImageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                  NewsImage.setImageBitmap(response.getBitmap());
                    logger.info("Loading Image:-"+response.getRequestUrl());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.error(error.toString());
                }


            });
        }
    }
}
