package com.goldpi.upgrade.Landing.HomeSlider;

import com.android.volley.RequestQueue;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.goldpi.upgrade.Network.VolleySingleton;
import com.goldpi.upgrade.PrefranceUtility.SliderPreference;
import com.goldpi.upgrade.UpgradeApplication;

import java.util.ArrayList;

import static com.goldpi.upgrade.PrefranceUtility.KEY.KEY_Slider_DASHBOARD;

/**
 * Created by Yusuf on 5/6/2016.
 */
public class Slider {

    static final String AndroidDashboard = "Android Dashboard";
    static SliderLayout mSlider;
    static private VolleySingleton volleySingleton;
    static private RequestQueue requestQueue;

    public static void Start(SliderLayout layout, ViewPagerEx.OnPageChangeListener listener, String SliderName) {
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        mSlider = layout;

        ArrayList<SliderModal> ListSlider = null;
        SliderModal[] getSliderlist = SliderPreference.getSlider(UpgradeApplication.getAppContext(), KEY_Slider_DASHBOARD);
        if (getSliderlist!=null) {

            for (SliderModal b : getSliderlist) {
                if (b.getTitle().equals(SliderName)) {
                    for (SlicesModel slice : b.getSlices()) {
                        TextSliderView tv1 = new TextSliderView(UpgradeApplication.getAppContext());
                        tv1.description(slice.getDescription());
                        tv1.image(slice.getImage());
                        tv1.Title(slice.getTitle());
                        mSlider.addSlider(tv1);
                    }
                }
            }
            mSlider.setPresetTransformer(SliderLayout.Transformer.Stack);
            mSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            mSlider.setCustomAnimation(new DescriptionAnimation());
            mSlider.setDuration(4000);
            mSlider.addOnPageChangeListener(listener);

        }

    }

}
