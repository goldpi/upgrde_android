package com.goldpi.upgrade.Landing.Activity;


import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.bluejamesbond.text.DocumentView;
import com.bluejamesbond.text.style.TextAlignment;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Landing.News.NewsModal;
import com.goldpi.upgrade.Network.VolleySingleton;
import com.goldpi.upgrade.UpgradeApplication;
import com.goldpi.upgrade.Utility.IConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.Locale;
import butterknife.Bind;
import butterknife.ButterKnife;
import fr.castorflex.android.verticalviewpager.VerticalViewPager;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

public class VerticleSwipActivity extends AppCompatActivity {


    private static final float MIN_SCALE = 0.75f;
    private static final float MIN_ALPHA = 0.75f;
    ArrayList<NewsModal> data=new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verticle_swip);


        //get index of News Item
        int id=getIntent().getExtras().getInt("NewsID");
        //get ArrayList of News Item
        data=getIntent().getParcelableArrayListExtra("News");

        VerticalViewPager verticalViewPager = (VerticalViewPager) findViewById(R.id.verticalviewpager);
        DummyAdapter adapter=new DummyAdapter(getSupportFragmentManager());

        adapter.SetData(data);
        //adapter.getItem(1);
        verticalViewPager.setAdapter(adapter);
        //Set the current item of News Pages
        verticalViewPager.setCurrentItem(id);
       // verticalViewPager.setPageMargin(getResources().getDimensionPixelSize(R.dimen.activity_vertical_margin));
       // verticalViewPager.setPageMarginDrawable(new ColorDrawable(getResources().getColor(android.R.color.holo_green_dark)));

       /* if(id!=0) {
            NewsModal News = UpgradeApplication.getWritableDatabase().getSingleNews(id);
            if (News!=null)
            {


            }

       }*/


        verticalViewPager.setAdapter(adapter);
        verticalViewPager.setCurrentItem(1);
        //verticalViewPager.setPageMargin(getResources().getDimensionPixelSize(R.dimen.pagemargin));
        verticalViewPager.setPageMarginDrawable(new ColorDrawable(getResources().getColor(android.R.color.holo_green_dark)));
        verticalViewPager.setPageTransformer(true, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View view, float position) {
                int pageWidth = view.getWidth();
                int pageHeight = view.getHeight();

                if (position < -1) { // [-Infinity,-1)
                    // This page is way off-screen to the left.
                    view.setAlpha(0);

                } else if (position <= 1) { // [-1,1]
                    // Modify the default slide transition to shrink the page as well
                    float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                    float vertMargin = pageHeight * (1 - scaleFactor) / 2;
                    float horzMargin = pageWidth * (1 - scaleFactor) / 2;
                    if (position < 0) {
                        view.setTranslationY(vertMargin - horzMargin / 2);
                    } else {
                        view.setTranslationY(-vertMargin + horzMargin / 2);
                    }

                    // Scale the page down (between MIN_SCALE and 1)
                    view.setScaleX(scaleFactor);
                    view.setScaleY(scaleFactor);

                    // Fade the page relative to its size.
                    view.setAlpha(MIN_ALPHA +
                            (scaleFactor - MIN_SCALE) /
                                    (1 - MIN_SCALE) * (1 - MIN_ALPHA));

                } else { // (1,+Infinity]
                    // This page is way off-screen to the right.
                    view.setAlpha(0);
                }
            }
        });



    }

    public void SetShowCase()
    {
        new MaterialShowcaseView.Builder(this)
               //.setTarget(verticalViewPager)
                .setDismissText("GOT IT")
                .setContentText("This is dashboard")
                .setDelay(300) // optional but starting animations immediately in onCreate can make them choppy
                        //.singleUse("z") // provide a unique ID used to ensure it is only shown once
                .show();
    }

    public class DummyAdapter extends FragmentPagerAdapter {

        ArrayList<NewsModal> mdata;
        public DummyAdapter(android.support.v4.app.FragmentManager fm)
        {

        super(fm);

        }

        public  void SetData( ArrayList<NewsModal> data)
        {
            mdata=data;
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {


            return PlaceholderFragment.newInstance(position,mdata);
        }

        @Override
        public int getCount() {

            return mdata.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return "PAGE 1";
                case 1:
                    return "PAGE 2";
                case 2:
                    return "PAGE 3";
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends android.support.v4.app.Fragment  {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static final String ARG_SECTION_NAME = "section_Name";
        private static ArrayList<NewsModal> mNewsList;
        private static final String STATE_NEWS = "state_news";
        @Bind(R.id.txtNewsTitle)TextView NewsTitle;
        @Bind(R.id.NewsImage)
        ImageView NewsImage;
        private VolleySingleton mVolleySingleton;
        private ImageLoader mImageLoader;
        final static Logger logger = LoggerFactory.getLogger(PlaceholderFragment.class);
        private static Intent intent;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber,ArrayList<NewsModal> data) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            mNewsList=data;
            fragment.setArguments(args);

            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_verticalswip, container, false);
            ButterKnife.bind(this,rootView);
            mVolleySingleton = VolleySingleton.getInstance();
            mImageLoader = mVolleySingleton.getImageLoader();


            NewsModal current=mNewsList.get(getArguments().getInt(ARG_SECTION_NUMBER));
            TextView textView = (TextView) rootView.findViewById(R.id.textview);
            //textView.setText(current.getContent());

            LinearLayout NewsContainer=(LinearLayout)rootView.findViewById(R.id.layoutNewsContainer);
            //DocumentView
            DocumentView Description= new DocumentView(UpgradeApplication.getAppContext(), DocumentView.PLAIN_TEXT);  // Support plain text
            Description.getDocumentLayoutParams().setTextAlignment(TextAlignment.JUSTIFIED);
            Description.getDocumentLayoutParams().setTextColor(R.color.blue_grey_700);
            Description.getDocumentLayoutParams().setInsetPaddingLeft(10F);
            Description.getDocumentLayoutParams().setInsetPaddingRight(10F);
            Description.setText(current.getContent()); // Set to `true` to enable justification
            Description.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));
            NewsContainer.addView(Description);
            NewsTitle.setText(current.getTitle());
            if(current.getImageUrl().equals(IConstants.NA))
                NewsImage.setVisibility(View.GONE);
            else
                loadImages(current.getImageUrl());
            return rootView;
        }



        private void loadImages(String urlThumbnail) {
            if (!urlThumbnail.equals(IConstants.NA)) {
                mImageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        NewsImage.setImageBitmap(response.getBitmap());
                        logger.info("Loading Image:-"+response.getRequestUrl());
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.error(error.toString());
                    }


                });
            }
        }
    }
}
