package com.goldpi.upgrade.Landing.News;

import com.android.volley.RequestQueue;
import com.goldpi.upgrade.Network.HTTPJSONRequestor;
import com.goldpi.upgrade.Parser.NewsParser;
import com.goldpi.upgrade.UpgradeApplication;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static com.goldpi.upgrade.Network.EndPointURL.APP_NEWS_URL;
/**
 * Created by Yusuf on 5/1/2016.
 */
public class NewsLoader {
    final static Logger logger = LoggerFactory.getLogger(NewsLoader.class);
    //Load News
    //
    public static ArrayList<NewsModal> loadNews(RequestQueue requestQueue) {
     JSONArray response = HTTPJSONRequestor.requestJSON(requestQueue, APP_NEWS_URL);
        logger.info("Load News",response);
        ArrayList<NewsModal> listMovies = NewsParser.parseNewsJSON(response);
        if(listMovies.size()>0)
        UpgradeApplication.getWritableDatabase().insertNews(listMovies, true);
        else
            listMovies= UpgradeApplication.getWritableDatabase().readNews();
        return listMovies;
    }


}
