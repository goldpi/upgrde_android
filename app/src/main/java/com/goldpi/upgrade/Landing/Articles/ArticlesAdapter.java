package com.goldpi.upgrade.Landing.Articles;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Network.VolleySingleton;
import com.goldpi.upgrade.Utility.StringTrimer;
import com.goldpi.upgrade.anim.AnimationUtils;

import java.util.List;


public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.MyViewHolder> {

    private List<ArticleModal> dataSet;
    private LayoutInflater inflator;
    private View view;
    private int mPreviousPosition = 0;

    private VolleySingleton mVolleySingleton;
    private ImageLoader mImageLoader;

    public ArticlesAdapter(Context contex) {

        inflator = LayoutInflater.from(contex);
        mVolleySingleton = VolleySingleton.getInstance();
        mImageLoader = mVolleySingleton.getImageLoader();
    }


    public void setArticle(List<ArticleModal> data) {

        this.dataSet = data;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view = inflator.inflate(R.layout.article_item, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int Position) {

        holder.textArticleTitle.setText(dataSet.get(Position).getTitle());
        holder.textArticleDescription.setText(StringTrimer.Trim(dataSet.get(Position).getContent(),100));
        holder.txtArticleId.setText(Integer.toString(dataSet.get(Position).getId()));


        if (Position > mPreviousPosition) {
            AnimationUtils.animateSunblind(holder, true);
        } else {
            AnimationUtils.animateSunblind(holder, false);
        }
        mPreviousPosition = Position;
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        Context mContext;
        ImageView Thumbnail;
        TextView txtPostedOn;
        TextView textArticleTitle;
        ImageView imageViewIcon;
        TextView textArticleDescription;
        TextView txtArticleId;

        public MyViewHolder(View itemView) {
            super(itemView);

            this.txtArticleId = (TextView) itemView.findViewById(R.id.txtArticleID);
            this.textArticleTitle = (TextView) itemView.findViewById(R.id.txtFrgmFiveTitle);
            this.Thumbnail = (ImageView) itemView.findViewById(R.id.imgFrgmFiveImage);
            this.textArticleDescription = (TextView) itemView.findViewById(R.id.txtFrgmFiveDescription);
        }
    }
}

