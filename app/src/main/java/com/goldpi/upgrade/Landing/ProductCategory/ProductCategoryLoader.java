package com.goldpi.upgrade.Landing.ProductCategory;

import com.android.volley.RequestQueue;
import com.goldpi.upgrade.Network.HTTPJSONRequestor;
import com.goldpi.upgrade.Parser.ProductCategoryParser;
import com.goldpi.upgrade.UpgradeApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static com.goldpi.upgrade.Network.EndPointURL.APP_PRODUCT_CATEGORY_URL;

/**
 * Created by Digambar on 5/23/2016.
 */
public class ProductCategoryLoader {
    final static Logger logger = LoggerFactory.getLogger(ProductCategoryLoader.class);
    //Load Product Category
    //
    public static ArrayList<ProductCategoryModel> loadProductCategory(RequestQueue requestQueue)  {
        JSONArray response = HTTPJSONRequestor.requestJSON(requestQueue, APP_PRODUCT_CATEGORY_URL);
        logger.info("Load Product Category",response);
        ArrayList<ProductCategoryModel> listProductCategory = ProductCategoryParser.parseProductCategoryJSON(response);
        if(listProductCategory.size()>0)
            UpgradeApplication.getWritableDatabase().insertProductCategory(listProductCategory, true);
        else
            listProductCategory= UpgradeApplication.getWritableDatabase().readProductCategory();
        return listProductCategory;
    }
}
