package com.goldpi.upgrade.Landing.HomeSlider;

import android.support.v7.widget.LinearSmoothScroller;

import com.android.volley.RequestQueue;
import com.goldpi.upgrade.Landing.News.NewsModal;
import com.goldpi.upgrade.Network.HTTPJSONRequestor;
import com.goldpi.upgrade.Network.VolleySingleton;
import com.goldpi.upgrade.Parser.NewsParser;
import com.goldpi.upgrade.Parser.SliderParser;
import com.goldpi.upgrade.PrefranceUtility.SliderPreference;
import com.goldpi.upgrade.UpgradeApplication;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import static com.goldpi.upgrade.Network.EndPointURL.APP_NEWS_URL;
import static com.goldpi.upgrade.PrefranceUtility.KEY.KEY_SLIDER_COMPUTER_CREATIVE;
import static com.goldpi.upgrade.PrefranceUtility.KEY.KEY_Slider_DASHBOARD;
import static com.goldpi.upgrade.Network.EndPointURL.APP_SLIDER_URL;

/**
 * Created by Yusuf on 5/6/2016.
 */
public class SliderLoader {

//region API Testing Json Data
    public static final String LocalData = " [\n" +
            "    {\n" +
            "      \"Title\": \"Mercury to appear as a dot on solar disc\",\n" +
            "      \"Description\": \"Several parts of India on May 9, will witness a rare astronomical phenomenon that occurs only 13 times in a century.\",\n" +
            "      \"ImageURL\": \"http://lorempixel.com/image_output/sports-q-c-800-450-5.jpg\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"Title\": \"AYUSH Ministry rails against global study on homeopathy\",\n" +
            "      \"Description\": \"ustralia's top medical research body‘s “findings are contrary to findings and conclusions of homeopathy in India\\\", the AYUSH Ministry told the Lok Sabha.\",\n" +
            "      \"ImageURL\": \"http://lorempixel.com/image_output/people-q-c-1024-786-3.jpg\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"Title\": \"Air India pays pending dues to pilots\",\n" +
            "      \"Description\": \"The Air India management today paid pending flying allowance dues to over 800 pilots.\",\n" +
            "      \"ImageURL\": \"http://cdnimg.visualizeus.com/thumbs/5b/e9/black,wallpaper-5be920f6c31acaabcb5ebec27d50ae30_h.jpg?ts=93246\"\n" +
            "    }\n" +
            "  ]";

    //endregion


    final static Logger logger = LoggerFactory.getLogger(SliderLoader.class);


    public static ArrayList<SliderModal> loadSlides(RequestQueue requestQueue) {
        ArrayList<SliderModal> mlistSlider = new ArrayList<>();
        SliderModal[] getSliderlist;
        JSONArray response = HTTPJSONRequestor.requestJSON(requestQueue, APP_SLIDER_URL);
        if(response!=null)
            mlistSlider = SliderParser.parseSliderJSON(response);

        if (mlistSlider.size() > 0) {
            SliderPreference.clearSlider(UpgradeApplication.getAppContext());
            getSliderlist = mlistSlider.toArray(new SliderModal[mlistSlider.size()]);
            SliderPreference.setSlider(getSliderlist, UpgradeApplication.getAppContext(), KEY_Slider_DASHBOARD);
        }

        return mlistSlider;
    }
}
