package com.goldpi.upgrade.Landing.Activity;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.goldpi.materialui.R;
import com.goldpi.upgrade.Landing.Products.ProductsAdapter;
import com.goldpi.upgrade.UIHelper.DividerItemDecoration;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

public class ScienceProjectActivity extends AppCompatActivity {
     ProductsAdapter mProductAdapter;
     RecyclerView recyclerViewProduct;
     CollapsingToolbarLayout collapsingToolbarLayout;
    Toolbar mtoolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_science_project);

recyclerViewProduct=(RecyclerView)findViewById(R.id.rcyScienceProjectList);
        //mProductAdapter = new ProductsAdapter(this, ProductModal.GetNewsList());
        recyclerViewProduct.setAdapter(mProductAdapter);
        recyclerViewProduct.addItemDecoration(
                new DividerItemDecoration(this, null));
        recyclerViewProduct.setLayoutManager(new LinearLayoutManager(this));
        setToolbar();
        setupCollapsingToolbarLayout();

    }

    private void setupCollapsingToolbarLayout(){

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        if(collapsingToolbarLayout != null){
            collapsingToolbarLayout.setTitle(mtoolbar.getTitle());
            //collapsingToolbarLayout.setCollapsedTitleTextColor(0xED1C24);
            //collapsingToolbarLayout.setExpandedTitleColor(0xED1C24);
        }
    }

    public void setToolbar()
    {
        mtoolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mtoolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Science Projects");

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(new IconDrawable(this, FontAwesomeIcons.fa_arrow_left)
                .colorRes(R.color.white)
                .actionBarSize());
        ab.setDisplayHomeAsUpEnabled(true);
    }

}
