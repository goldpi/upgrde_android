package com.goldpi.upgrade.Landing.HomeSlider;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Utility.SlidingSorter;

import java.util.ArrayList;

/**
 * Created by Yusuf on 5/6/2016.
 */
public class TextSliderView extends BaseSliderView {
    String Title;
    public TextSliderView(Context context) {
        super(context);
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getTitle()
    {
        return Title;
    }

    public BaseSliderView Title(String title){
        Title = title;
        return this;
    }
    @Override
    public View getView() {
        View v =android.view.LayoutInflater.from(getContext()).inflate(R.layout.slider,null);
        ImageView target = (ImageView)v.findViewById(R.id.imgSliderImageURL);
        TextView title = (TextView)v.findViewById(R.id.txtSliderTitle);
        TextView description = (TextView)v.findViewById(R.id.txtSliderdescription);
        description.setText(getDescription());
        title.setText(getTitle());
        bindEventAndShow(v, target);
        SlidingSorter slide=new SlidingSorter();
        ArrayList<SlicesModel> SliceList=new ArrayList<>();
        for (int i=0;i<=10;i++)
        {
            int randommval=i;
            if(i==2)
            {
                randommval=20;
            }
            else if(i==7)
            {
            randommval=12;
            }
            SlicesModel NewSlice=new SlicesModel("Title "+i,"Description "+i, "Imgage URL "+i,randommval);
            SliceList.add(NewSlice);
        }
        slide.SortSlideBySliderOrder(SliceList);
        return v;
    }
}