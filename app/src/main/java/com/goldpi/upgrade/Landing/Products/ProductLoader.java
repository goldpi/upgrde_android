package com.goldpi.upgrade.Landing.Products;

import com.android.volley.RequestQueue;
import com.goldpi.upgrade.Network.HTTPJSONRequestor;
import com.goldpi.upgrade.Parser.ProductParser;
import com.goldpi.upgrade.UpgradeApplication;

import org.json.JSONArray;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import java.util.ArrayList;

import static com.goldpi.upgrade.Network.EndPointURL.APP_PRODUCT_URL;


/**
 * Created by Digambar on 5/20/2016.
 */
public class ProductLoader {
    final static Logger logger= LoggerFactory.getLogger(ProductLoader.class);
    //load Product
    public static ArrayList<ProductModal> loadProduct(RequestQueue requestQueue)
    {
        JSONArray response= HTTPJSONRequestor.requestJSON(requestQueue,APP_PRODUCT_URL);
        logger.info("Load Product", response);
        ArrayList<ProductModal> listproduct = ProductParser.parseProductJSON(response);
        if(listproduct.size()>0)
            UpgradeApplication.getWritableDatabase().insertProduct(listproduct, true);
        else
            listproduct= UpgradeApplication.getWritableDatabase().readProduct();
        return listproduct;

    }

}
