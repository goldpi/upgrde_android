package com.goldpi.upgrade.Landing.News;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Landing.Activity.VerticleSwipActivity;
import com.goldpi.upgrade.Network.VolleySingleton;
import com.goldpi.upgrade.Utility.IConstants;
import com.goldpi.upgrade.Utility.StringTrimer;
import com.goldpi.upgrade.anim.AnimationUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {
    final static Logger logger = LoggerFactory.getLogger(NewsAdapter.class);
    private static ArrayList<NewsModal> dataSet;
    private  LayoutInflater inflator;
    private View view;
    private int mPreviousPosition=0;

    private VolleySingleton mVolleySingleton;
    private ImageLoader mImageLoader;
    public NewsAdapter(Context contex) {

        inflator=LayoutInflater.from(contex);
        mVolleySingleton = VolleySingleton.getInstance();
        mImageLoader = mVolleySingleton.getImageLoader();
    }

    public void setNews(ArrayList<NewsModal> data) {
        this.dataSet = data;
        //update the adapter to reflect the new set of news
        notifyDataSetChanged();
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {

        view =inflator.inflate(R.layout.landingnewslayout,parent,false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        holder.textNewsTitle.setText(dataSet.get(listPosition).getTitle());
        holder.textViewDesc.setText(StringTrimer.Trim(dataSet.get(listPosition).getContent(), 50));
        holder.imageViewIcon.setImageResource(R.drawable.img_profile_cover);
        holder.txtNewsId.setText(Integer.toString(dataSet.get(listPosition).getId()));
        String ThumbURL=dataSet.get(listPosition).getImageUrl();
        loadImages(ThumbURL,holder);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        holder.txtPostedOn.setText(dateFormat.format(dataSet.get(listPosition).getPostedOn()));

        if (listPosition > mPreviousPosition) {
            AnimationUtils.animateSunblind(holder, true);
        } else {
            AnimationUtils.animateSunblind(holder, false);
        }
        mPreviousPosition = listPosition;

    }
    private void loadImages(String urlThumbnail, final MyViewHolder holder) {
        if (!urlThumbnail.equals(IConstants.NA)) {
            mImageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.Thumbnail.setImageBitmap(response.getBitmap());
                    logger.info("Loading Image:-"+response.getRequestUrl());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.error(error.toString());
                }


            });
        }
    }
    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context mContext;
        ImageView Thumbnail;
        TextView txtPostedOn;
        TextView textNewsTitle;
        ImageView imageViewIcon;
        TextView textViewDesc;
        TextView txtNewsId;
        public MyViewHolder(View itemView) {
            super(itemView);
            mContext=itemView.getContext();
            this.textNewsTitle = (TextView) itemView.findViewById(R.id.txtfragmentoneNewsTitle);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.imgfragmentoneNewsImage);
            this.textViewDesc=(TextView)itemView.findViewById(R.id.txtfragmentoneNewsDescription);
            this.txtPostedOn=(TextView)itemView.findViewById(R.id.txtfragmentoneNewsPostedOn);
            this.Thumbnail=(ImageView)itemView.findViewById(R.id.imgfragmentoneNewsImage);
            this.txtNewsId=(TextView)itemView.findViewById(R.id.NewsId);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {



            TextView NewsId=(TextView) v.findViewById(R.id.NewsId);

            Toast.makeText(mContext,"News ID:- "+NewsId.getText(),Toast.LENGTH_LONG).show();
          // Intent NewsDetail=new Intent(mContext,NewsDetailActivity.class);
            Intent NewsDetail=new Intent(mContext, VerticleSwipActivity.class);
            //NewsDetail.putExtra("NewsID",NewsId.getText());
            NewsDetail.putExtra("NewsID",getPosition());
            NewsDetail.putParcelableArrayListExtra("News",dataSet);
            mContext.startActivity(NewsDetail);

        }
    }
}
