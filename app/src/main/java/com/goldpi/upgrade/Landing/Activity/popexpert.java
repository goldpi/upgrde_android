package com.goldpi.upgrade.Landing.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.TextView;

import com.goldpi.materialui.R;

public class popexpert extends AppCompatActivity {

    private TextView textview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popexpert);
        Intent intent=getIntent();
        String str=intent.getStringExtra("id");
        textview=(TextView)findViewById(R.id.txtalertbox);
        textview.setText(str);
        DisplayMetrics dp=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dp);
        int height=dp.heightPixels;
        int width=dp.widthPixels;
        getWindow().setLayout((int)(width*.8),(int)(height*.6));

    }
}
