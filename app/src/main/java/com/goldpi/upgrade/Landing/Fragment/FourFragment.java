package com.goldpi.upgrade.Landing.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Landing.Activity.TabActivity;
import com.goldpi.upgrade.Landing.Articles.ArticleModal;
import com.goldpi.upgrade.Landing.Articles.ArticlesAdapter;
import com.goldpi.upgrade.Landing.HomeSlider.Slider;
import com.goldpi.upgrade.Listner.ILoadedArticlesListener;
import com.goldpi.upgrade.Task.TaskLoadArticles;
import com.goldpi.upgrade.UIHelper.DividerItemDecoration;
import com.goldpi.upgrade.UpgradeApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static com.goldpi.upgrade.Landing.HomeSlider.SliderName.SliderName_AndroidComputer_Creative;

/**
 * `
 * A simple {@link Fragment} subclass.
 */
public class FourFragment extends Fragment implements ILoadedArticlesListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {


    //The key used to store arraylist of news objects to and from parcelable
    private static final String STATE_ARTICLE = "state_Articles";
    final Logger logger = LoggerFactory.getLogger(OneFragment.class);
    private View view;
    private RecyclerView recyclerViewArticle;
    private ArticlesAdapter mArticleAdapter;
    private SliderLayout mSlider;
    private ArrayList<ArticleModal> mArticleList = new ArrayList<>();

    public FourFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_four, container, false);


        //region=============slider===============
        mSlider = (SliderLayout) view.findViewById(R.id.slider);
        Slider.Start(mSlider, this, SliderName_AndroidComputer_Creative);
        //endregion


        recyclerViewArticle = (RecyclerView) view.findViewById(R.id.rcyViewFrgmFourArticleContainer);
        //Log.d("Yusuf", recyclerViewArticle.toString());
        mArticleAdapter = new ArticlesAdapter(getActivity());
        recyclerViewArticle.setAdapter(mArticleAdapter);
        recyclerViewArticle.addItemDecoration(new DividerItemDecoration(getActivity(), null));
        recyclerViewArticle.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (savedInstanceState != null) {
            logger.info("Loading Articles from Saved Instance State");
            //if this fragment starts after a rotation or configuration change, load the existing news from a parcelable class
            mArticleList = savedInstanceState.getParcelableArrayList(STATE_ARTICLE);
        }
        else
        {
                //if this fragment starts for the first time, load the list of news from the database
                mArticleList = UpgradeApplication.getWritableDatabase().readArticles();
                //if the database is empty, trigger an AsycnTask to download movie list from the web
                if (mArticleList.isEmpty()) {
                    //load news from web server
                    new TaskLoadArticles(this).execute();
                }
            }

    mArticleAdapter.setArticle(mArticleList);
        return view;
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //save the movie list to a parcelable prior to rotation or configuration change
        outState.putParcelableArrayList(STATE_ARTICLE, mArticleList);
    }

    @Override
    public void onResume() {
        super.onResume();
            }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onArticlesLoaded(ArrayList<ArticleModal> listArticles) {
        mArticleAdapter.setArticle(listArticles);

    }
}
