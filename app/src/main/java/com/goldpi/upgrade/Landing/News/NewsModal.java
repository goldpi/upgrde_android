package com.goldpi.upgrade.Landing.News;


import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.goldpi.upgrade.Network.HttpStringRequestor;
import com.goldpi.upgrade.Network.VolleySingleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NewsModal implements Parcelable{
    final static Logger logger = LoggerFactory.getLogger(NewsModal.class);

    private int Id;
    private String Title;
    private String ImageUrl;
    private String VideoUrl;
    private String Content;
    private Date PostedOn;


    public NewsModal()
    {

    }

    public NewsModal(Parcel input)
    {
        Id=input.readInt();
        Title=input.readString();
        ImageUrl=input.readString();
        VideoUrl=input.readString();
        Content=input.readString();
        PostedOn=new Date(input.readLong());
    }
    public NewsModal(int id,String Title,String ImageUrl,String VideoUrl,String Content, Date On)
    {
        this.Id=id;
        this.Title=Title;
        this.ImageUrl=ImageUrl;
        this.VideoUrl=VideoUrl;
        this.Content=Content;
        this.PostedOn=On;


    }

    public static final Parcelable.Creator<NewsModal> CREATOR
            = new Parcelable.Creator<NewsModal>() {
        public NewsModal createFromParcel(Parcel in) {
            logger.error("create from parcel :News");
            return new NewsModal(in);
        }

        public NewsModal[] newArray(int size) {
            return new NewsModal[size];
        }
    };
    public static ArrayList<NewsModal> GetNewsList() {
        ArrayList<NewsModal> NewsCollection=new ArrayList<>();

        for (int i = 1; i <= 3; i++) {
            NewsModal news= new NewsModal();
           //NewsModal news= new NewsModal("News "," ", "VideoUrl ","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever....","23/2/2017");

            try {
                news.setId(i);
                news.setTitle("Lorem Ipsum is simply dummy text of the printing " + i);
                news.setContent("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever....\"");
                news.setImageUrl("http://picture.upgrde.net/content/logo.png");
                news.setVideoUrl("https://www.youtube.com/watch?v=2c1L19ZP5Qg");
                Date poston=null;
                String PostDate="2016-05-"+i;
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    poston = dateFormat.parse(PostDate);
                } catch (ParseException e) {
                    //a parse exception generated here will store null in the release date, be sure to handle it
                }
                news.setPostedOn(poston);
                NewsCollection.add(news);
            }
            catch (UnsupportedOperationException ex)
            {
                logger.error("Error", ex.getMessage(), ex);
            }

        }

        return NewsCollection;
    }



    public int getId() {
        return Id;
    }

    public String getTitle() {
        return Title;
    }


    public String getVideoUrl() {
        return VideoUrl;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public String getContent() {
        return Content;
    }

    public Date getPostedOn() {
        return PostedOn;
    }

    public void setId(int id) {
        Id = id;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setImageUrl(String Thumb) {
       this.ImageUrl=Thumb;
    }



    public void setVideoUrl(String videoUrl) {
        VideoUrl = videoUrl;
    }


    public void setPostedOn(Date on) {
        PostedOn = on;
    }

    public void setContent(String content) {
        Content = content;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Title);
        dest.writeString(ImageUrl);
        dest.writeString(VideoUrl);
        dest.writeString(Content);
        dest.writeLong(PostedOn == null ? -1 : PostedOn.getTime());

    }
}


