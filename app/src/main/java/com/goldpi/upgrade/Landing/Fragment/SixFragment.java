package com.goldpi.upgrade.Landing.Fragment;



import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;

import com.beardedhen.androidbootstrap.BootstrapProgressBar;
import com.goldpi.materialui.R;
import com.joanzapata.iconify.widget.IconTextView;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class SixFragment extends Fragment {


    private View view;
    @Bind(R.id.link_switchGole_frgmOne) IconTextView buttonNext;
@Bind(R.id.progressbarMyGoal) BootstrapProgressBar mMyGoalProgress;
    private ViewFlipper viewSwitcher;
    private Animation slide_in_left,slide_out_right;

    public SixFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_six, container, false);
        buttonNext = (IconTextView) view.findViewById(R.id.link_switchGole_frgmOne);
        viewSwitcher = (ViewFlipper) view.findViewById(R.id.viewswitcherSetMyGole_frgmOne);

        slide_in_left = AnimationUtils.loadAnimation(view.getContext(),
                android.R.anim.slide_in_left);
        slide_out_right = AnimationUtils.loadAnimation(view.getContext(),
                android.R.anim.slide_out_right);


        viewSwitcher.setInAnimation(slide_in_left);
        viewSwitcher.setOutAnimation(slide_out_right);

        ButterKnife.bind(this, view);
        mMyGoalProgress.setProgress(90);
        return view;
    }


    @OnClick(R.id.link_switchGole_frgmOne)
    public void OnClick(View v)
    {
        viewSwitcher.showNext();
        //viewSwitcher.startFlipping();
    }

}
