package com.goldpi.upgrade.Landing.Activity;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.VideoView;

import com.goldpi.materialui.R;
import com.goldpi.upgrade.Icon.Iconicfy;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import butterknife.ButterKnife;
public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener{

    Toolbar mtoolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private VideoView videoView;
    private WebView myWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        Iconicfy.SetIconify();
        ButterKnife.bind(this);
        setToolbar();
        setWebview();
        setupCollapsingToolbarLayout();


        // videoView = (VideoView)findViewById(R.id.VideoView);
        //MediaController mediaController = new MediaController(this);
        // mediaController.setAnchorView(videoView);
        //videoView.setMediaController(mediaController);

        //videoView.setVideoPath("/sdcard/Download/VID-20151003-WA0002-1.mp4");

       // videoView.seekTo(100);
//videoView.setOnClickListener(this);
    }

    public void setWebview()
    {
        String VimeoVedio="<iframe src=\"https://player.vimeo.com/video/50540814?byline=0&portrait=0\" width=\"100%\" height=\"100%\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>";
        String myVedio="<html><body style=\"padding:0px; margin:0px;\"> <iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/Ks2L6XFLAeA?autoplay=1&controls=0&enablejsapi=1&fs=0&modestbranding=1&playsinline=1&showinfo=0&autohide=1&color=white&theme=light\" frameborder=\"0\"></iframe></body></html>";
        myWebView = (WebView) findViewById( R.id.VideoWebView );
        String playVideo= "<html><body>Youtube video .. <br> <iframe class=\"youtube-player\" type=\"text/html\" width=\"640\" height=\"385\" src=\"http://www.youtube.com/embed/bIPcobKMB94\" frameborder=\"0\"></body></html>";
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.loadData(myVedio, "text/html", "utf-8");
        //https://youtu.be/Ks2L6XFLAeA
        //myWebView.loadUrl("http://www.youtube.com/embed/Ks2L6XFLAeA?autoplay=1");
    }

    public void setToolbar()
    {
        mtoolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(mtoolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sign Up");

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(new IconDrawable(this, FontAwesomeIcons.fa_arrow_left)
                .colorRes(R.color.colorPrimaryDark)
                .actionBarSize());
        ab.setDisplayHomeAsUpEnabled(true);
    }

    private void setupCollapsingToolbarLayout(){

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        if(collapsingToolbarLayout != null){
            collapsingToolbarLayout.setTitle(mtoolbar.getTitle());
            //collapsingToolbarLayout.setCollapsedTitleTextColor(0xED1C24);
            //collapsingToolbarLayout.setExpandedTitleColor(0xED1C24);
        }
    }

    @Override
    public void onClick(View v) {
        videoView.start();
    }
}
