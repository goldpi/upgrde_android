package com.goldpi.upgrade.Landing.Articles;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.goldpi.upgrade.Network.VolleySingleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Yusuf on 4/11/2016.
 */
public class ArticleModal implements Parcelable {
    final static Logger logger = LoggerFactory.getLogger(ArticleModal.class);
    public static final Parcelable.Creator<ArticleModal> CREATOR
            = new Parcelable.Creator<ArticleModal>() {


        @Override
        public ArticleModal createFromParcel(Parcel in) {
            logger.error("create from parcel :Article");
            return new ArticleModal(in);
        }

        @Override
        public ArticleModal[] newArray(int size) {
            return new ArticleModal[size];
        }
    };
    private int Id;
    private String Title;
    private String ImageUrl;
    private String VideoUrl;
    private String Content;
    private Date PostedOn;

    public ArticleModal() {

    }

    public ArticleModal(Parcel input) {
        Id = input.readInt();
        Title = input.readString();
        ImageUrl = input.readString();
        VideoUrl = input.readString();
        Content = input.readString();
        PostedOn = new Date(input.readLong());
    }

    public ArticleModal(int id, String Title, String ImageUrl, String VideoUrl, String Content, Date On) {
        this.Id = id;
        this.Title = Title;
        this.ImageUrl = ImageUrl;
        this.VideoUrl = VideoUrl;
        this.Content = Content;
        this.PostedOn = On;


    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getVideoUrl() {
        return VideoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        VideoUrl = videoUrl;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String Thumb) {
        this.ImageUrl = Thumb;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public Date getPostedOn() {
        return PostedOn;
    }

    public void setPostedOn(Date on) {
        PostedOn = on;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Title);
        dest.writeString(ImageUrl);
        dest.writeString(VideoUrl);
        dest.writeString(Content);
        dest.writeLong(PostedOn == null ? -1 : PostedOn.getTime());

    }
}
