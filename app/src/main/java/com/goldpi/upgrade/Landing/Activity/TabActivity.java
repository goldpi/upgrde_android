package com.goldpi.upgrade.Landing.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.android.volley.toolbox.ImageLoader;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ActionViewTarget;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Account.AccountLandlingActivity;
import com.goldpi.upgrade.Account.LoginActivity;
import com.goldpi.upgrade.DataModal.Authorization;
import com.goldpi.upgrade.Home.HomeActivity;
import com.goldpi.upgrade.Landing.Fragment.FiveFragment;
import com.goldpi.upgrade.Landing.Fragment.FourFragment;
import com.goldpi.upgrade.Landing.Fragment.OneFragment;
import com.goldpi.upgrade.Landing.Fragment.ThreeFragment;
import com.goldpi.upgrade.Landing.Fragment.TwoFragment;
import com.goldpi.upgrade.Network.VolleySingleton;
import com.goldpi.upgrade.PrefranceUtility.AccessTokenPrefrance;
import com.goldpi.upgrade.Service.NotifyService;
import com.goldpi.upgrade.Service.ServiceNews;
import com.goldpi.upgrade.UIHelper.FullScreen;
import com.goldpi.upgrade.UpgradeApplication;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.joanzapata.iconify.widget.IconButton;
import com.joanzapata.iconify.widget.IconTextView;
import com.viewpagerindicator.LinePageIndicator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import me.tatarka.support.job.JobInfo;
import me.tatarka.support.job.JobScheduler;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;


public class TabActivity extends AppCompatActivity  implements ViewPager.OnPageChangeListener
{

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private IconTextView iconTextViewNext;
    IconButton btnNext;
    private boolean UserISAuthenticated;
    private JobScheduler mJobScheduler;

    //int corresponding to the id of our JobSchedulerService
    private static final int JOB_ID = 100;
    //Run the JobSchedulerService every 2 minutes
    private static final long POLL_FREQUENCY = 28800000;
    final Logger logger = LoggerFactory.getLogger(TabActivity.class);
    private VolleySingleton mVolleySingleton;
    private ImageLoader mImageLoader;
    private LinePageIndicator titleIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);
       // setToolbar();// to set action bar
        ButterKnife.bind(this);
        //to hide status bar
        FullScreen.Hide(this);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        iconTextViewNext=(IconTextView)findViewById(R.id.txtskip);

       // Authorization SavedAuthorization= AccessTokenPrefrance.getCurrentUser(getBaseContext());
       //Bind the title indicator to the adapter
        //TitlePageIndicator titleIndicator = (TitlePageIndicator)findViewById(R.id.titles);
        //titleIndicator.setViewPager(viewPager);
        setupViewPager(viewPager);
        setupJob();

        viewPager.addOnPageChangeListener(this);
        iconTextViewNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login=new Intent(TabActivity.this, AccountLandlingActivity.class);
                startActivity(login);
            }
        });
    }
    @Override
    protected void onResume()
    {
        super.onResume();
        FullScreen.Hide(this);
    }

    private void setupJob() {
        mJobScheduler = JobScheduler.getInstance(this);
        //set an initial delay with a Handler so that the data loading by the JobScheduler does not clash with the loading inside the Fragment
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //schedule the job after the delay has been elapsed
                logger.info("Executing Build Job Method");
                buildJob();

            }
        }, 30000);
    }
    private void buildJob() {

        //attach the job ID and the name of the Service that will work in the background
        JobInfo.Builder builder = new JobInfo.Builder(JOB_ID, new ComponentName(this, ServiceNews.class));
        //set periodic polling that needs net connection and works across device reboots
        builder.setPeriodic(POLL_FREQUENCY)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                .setPersisted(true);
        mJobScheduler.schedule(builder.build());
        logger.info("Executing Build Job Method END");
    }

    public void setToolbar()
    {
      //  toolbar = (Toolbar) findViewById(R.id.app_bar);
       // setSupportActionBar(toolbar);
       // getSupportActionBar().setTitle("Upgrde Dashboard");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.muenu_landing, menu);

        return true;
    }
    ///
    ///Upper navigation click bindings
    ///
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_skip_landingActivity) {
//To:Do bug
            try {
                Authorization auth = AccessTokenPrefrance.getCurrentUser(UpgradeApplication.getAppContext());
                if(auth.getAccess_token().length()>0&&auth.getAccess_token()!=""&&auth!=null) {
                    this.finish();
                    Intent i = new Intent(this, HomeActivity.class);
                    startActivity(i);

                    InvokeService();
                }
                else {

                    Intent i = new Intent(this, AccountLandlingActivity.class);
                    startActivity(i);
                }
            }
            catch (Exception e){
                Intent i = new Intent(this, AccountLandlingActivity.class);
                startActivity(i);
            }

            return true;
        }
            return super.onOptionsItemSelected(item);
    }

    private void InvokeService() {

        Intent service= new Intent(TabActivity.this, NotifyService.class);
        startService(service);
    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());


//Bind the title indicator to the adapter

        adapter.addFragment(new OneFragment(), "ONE");
        adapter.addFragment(new TwoFragment(), "TWO");
        adapter.addFragment(new ThreeFragment(), "THREE");
        adapter.addFragment(new FourFragment(), "Four");
        adapter.addFragment(new FiveFragment(), "Five");
       // adapter.addFragment(new SixFragment(), "Six");
        viewPager.setAdapter(adapter);

        titleIndicator = (LinePageIndicator)findViewById(R.id.indicator);
        titleIndicator.setViewPager(viewPager);
    }
    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(new IconDrawable(this, FontAwesomeIcons.fa_bell));
        //tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        //tabLayout.getTabAt(2).setIcon(tabIcons[2]);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if(position==0) {


        }

   else if(position==1)
    {


    }
     else if(position==3) {


        }

        else if(position==4) {


        }

        else
        {

        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    // get the slider view


    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
}



}
