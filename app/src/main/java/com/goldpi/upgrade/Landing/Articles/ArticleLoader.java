package com.goldpi.upgrade.Landing.Articles;

import com.android.volley.RequestQueue;
import com.goldpi.upgrade.Network.HTTPJSONRequestor;
import com.goldpi.upgrade.Parser.ArticleParser;
import com.goldpi.upgrade.UpgradeApplication;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static com.goldpi.upgrade.Network.EndPointURL.APP_COMPUTER_CREATIVE_URL;
/**
 * Created by Digambar on 5/11/2016.
 */
public class ArticleLoader {
    final static Logger logger = LoggerFactory.getLogger(ArticleLoader.class);
    public static ArrayList<ArticleModal> loadArticle (RequestQueue requestQueue) {
        JSONArray response= HTTPJSONRequestor.requestJSON(requestQueue, APP_COMPUTER_CREATIVE_URL);
        logger.info("load Article",response);
        ArrayList<ArticleModal> listArticles=ArticleParser.parseArticalJSON(response);
        if(listArticles.size()>0)
        {
            UpgradeApplication.getWritableDatabase().insertArticles(listArticles,true);
        }
        else
        {
            listArticles=UpgradeApplication.getWritableDatabase().readArticles();
        }

        return listArticles;

    }
}
