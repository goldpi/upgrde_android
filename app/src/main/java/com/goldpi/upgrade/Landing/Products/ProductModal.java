package com.goldpi.upgrade.Landing.Products;

import android.os.Parcel;
import android.os.Parcelable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Yusuf on 4/11/2016.
 */
public class ProductModal implements Parcelable{
    final static Logger logger = LoggerFactory.getLogger(ProductModal.class);

    private int Id;
    private String Name;
    private Double Price;
    private int CategoryId;
    private String ShortDesc;
    private String LongDesc;
    private String ImageUrl;
    private String VideoUrl;
    private Date Addedon;
    public ProductModal()
    {

    }
    public ProductModal(Parcel input)
    {

        Id=input.readInt();
        Name=input.readString();
        Price=input.readDouble();
        CategoryId=input.readInt();
        ShortDesc=input.readString();
        LongDesc=input.readString();
        ImageUrl=input.readString();
        VideoUrl=input.readString();
        //Addedon=input.r
    }
    public ProductModal(int id,String Name, Double Price, int CategoryId,String ShortDesc, String LongDesc, String ImgUrl,String VideoUrl,Date Addedon)
    {
        this.Id=id;
        this.Name=Name;
        this.Price=Price;
        this.CategoryId=CategoryId;
        this.ShortDesc=ShortDesc;
        this.LongDesc=LongDesc;
        this.ImageUrl=ImgUrl;
        this.VideoUrl=VideoUrl;
        this.Addedon=Addedon;
    }

    public static final Creator<ProductModal> CREATOR = new Creator<ProductModal>() {
        @Override
        public ProductModal createFromParcel(Parcel in) {
            return new ProductModal(in);
        }

        @Override
        public ProductModal[] newArray(int size) {
            return new ProductModal[size];
        }
    };

    public void setId(int id) {
        Id = id;
    }

    public int getId() {
        return Id;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getName() {
        return Name;
    }

    public void setPrice(Double price) {
        Price = price;
    }

    public Double getPrice() {
        return Price;
    }

    public void setShortDesc(String shortDesc) {
        ShortDesc = shortDesc;
    }

    public String getShortDesc() {
        return ShortDesc;
    }

    public void setLongDesc(String longDesc) {
        LongDesc = longDesc;
    }

    public String getLongDesc() {
        return LongDesc;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public String getVideoUrl() {
        return VideoUrl;
    }

    public Date getAddedon() {
        return Addedon;
    }

    public void setAddedon(Date addedon) {
        Addedon = addedon;
    }

    public void setVideoUrl(String videoUrl) {
        VideoUrl = videoUrl;
    }

    public int getCategoryId() {
        return CategoryId;
    }

    public void setCategoryId(int categoryId) {
        CategoryId = categoryId;
    }

    // TODO Code to Load Product List From Server
    public static List<ProductModal> GetNewsList() {
        List<ProductModal> ProductCollection = new ArrayList<>();

        for (int i = 1; i <= 20; i++) {
            ProductModal Product = new ProductModal();

            try {
                Product.setId(i);
                Product.setName("Title " + i);
                Product.setPrice(400.45);
                Product.setCategoryId(i);
                Product.setShortDesc("Short Description");
                //  Product.setLongDesc("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever....\"");
                // Product.setImageUrl("http://picture.upgrde.net/content/logo.png");
                //Product.setVideoUrl("https://www.youtube.com/watch?v=2c1L19ZP5Qg");
                Date poston = null;
                String PostDate = "2016-05-" + i;
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    poston = dateFormat.parse(PostDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Product.setAddedon(poston);
                ProductCollection.add(Product);
            }
            catch (UnsupportedOperationException ex)
            {
                logger.error("Error", ex.getMessage(), ex);
            }

        }
        return ProductCollection;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Name);
        dest.writeDouble(Price);
        dest.writeInt(CategoryId);
        dest.writeString(ShortDesc);
        dest.writeString(LongDesc);
        dest.writeString(ImageUrl);
        dest.writeString(VideoUrl);
    }
}