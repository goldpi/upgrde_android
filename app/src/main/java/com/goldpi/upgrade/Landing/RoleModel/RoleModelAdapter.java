package com.goldpi.upgrade.Landing.RoleModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Network.VolleySingleton;
import com.goldpi.upgrade.Utility.IConstants;
import com.goldpi.upgrade.Utility.StringTrimer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by Digambar on 5/20/2016.
 */
public class RoleModelAdapter extends RecyclerView.Adapter<RoleModelAdapter.MyViewHolder> {

    final static Logger logger = LoggerFactory.getLogger(RoleModelAdapter.class);
    private List<RoleModelModel> dataSet;
    private LayoutInflater inflator;
    private View view;
    private int mPreviousPosition=0;

    private VolleySingleton mVolleySingleton;
    private ImageLoader mImageLoader;
    public RoleModelAdapter(Context context)
    {

        inflator=LayoutInflater.from(context);
        mVolleySingleton = VolleySingleton.getInstance();
        mImageLoader = mVolleySingleton.getImageLoader();
    }
    public void setRoleModel(List<RoleModelModel> data) {
        this.dataSet = data;
        //update the adapter to reflect the new set of news
        notifyDataSetChanged();
    }
    @Override
    public RoleModelAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view =inflator.inflate(R.layout.landingrolemodellayout,parent,false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(RoleModelAdapter.MyViewHolder holder, int listPosition) {
        holder.textViewName.setText(dataSet.get(listPosition).getTitle());
        holder.textViewDesc.setText(StringTrimer.Trim(dataSet.get(listPosition).getContent(), 50));
        holder.imageViewIcon.setImageResource(R.drawable.img_profile_cover);
        holder.txtNewsId.setText(Integer.toString(dataSet.get(listPosition).getId()));
        String ThumbURL=dataSet.get(listPosition).getImageUrl();
        loadImages(ThumbURL,holder);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        holder.txtPostedOn.setText(dateFormat.format(dataSet.get(listPosition).getPostedOn()));
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    private void loadImages(String urlThumbnail, final MyViewHolder holder) {
        if (!urlThumbnail.equals(IConstants.NA)) {
            mImageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.Thumbnail.setImageBitmap(response.getBitmap());
                    logger.info("Loading Image:-"+response.getRequestUrl());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.error(error.toString());
                }


            });
        }
    }
    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Context mContext;
        ImageView Thumbnail;
        TextView txtPostedOn;
        TextView textViewName;
        //TextView textViewprice;
        ImageView imageViewIcon;
        TextView textViewDesc;
        TextView txtNewsId;

        public MyViewHolder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            this.textViewName = (TextView) itemView.findViewById(R.id.txtfragmentoneRoleModelTitle);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.imgfragmentoneRoleModelImage);
            this.textViewDesc = (TextView) itemView.findViewById(R.id.txtfragmentoneRoleModelDescription);
            this.txtPostedOn = (TextView) itemView.findViewById(R.id.txtfragmentoneRoleModelPostedOn);
            this.Thumbnail = (ImageView) itemView.findViewById(R.id.imgfragmentoneRoleModelImage);
            this.txtNewsId = (TextView) itemView.findViewById(R.id.RoleModelId);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
