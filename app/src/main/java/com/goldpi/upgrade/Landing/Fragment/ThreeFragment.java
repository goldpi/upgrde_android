package com.goldpi.upgrade.Landing.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.tibolte.agendacalendarview.AgendaCalendarView;
import com.github.tibolte.agendacalendarview.CalendarPickerController;
import com.github.tibolte.agendacalendarview.models.CalendarEvent;
import com.github.tibolte.agendacalendarview.models.DayItem;

import com.goldpi.upgrade.Landing.Calender.AgendaViewCalenderEventCollection;
import com.goldpi.materialui.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class ThreeFragment extends Fragment implements CalendarPickerController {

    private View view;

    public ThreeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
      view=inflater.inflate(R.layout.fragment_three, container, false);
        AgendaCalendarView mAgendaCalendarView=(AgendaCalendarView)view.findViewById(R.id.agenda_calendar_view);
        setUpCalender(mAgendaCalendarView);
        return view;
    }

public void setUpCalender( AgendaCalendarView mAgendaCalendarView)
{
    Calendar minDate = Calendar.getInstance();
    Calendar maxDate = Calendar.getInstance();

    minDate.add(Calendar.MONTH, -2);
    minDate.set(Calendar.DAY_OF_MONTH, 1);
    maxDate.add(Calendar.YEAR, 1);

    List<CalendarEvent> eventList = new ArrayList<>();

    AgendaViewCalenderEventCollection eventCollection= new AgendaViewCalenderEventCollection(getContext());
    eventList=eventCollection.FuncEventList();
    mAgendaCalendarView.init(eventList, minDate, maxDate, Locale.getDefault(), this);
}




    // region Interface - CalendarPickerController

    @Override
    public void onDaySelected(DayItem dayItem) {
      //  Log.d("LOG_TAG", String.format("Selected day: %s", dayItem));
    }

    @Override
    public void onEventSelected(CalendarEvent event) {
       // Log.d("LOG_TAG", String.format("Selected event: %s", event));
    }

    @Override
    public void onScrollToDate(Calendar calendar) {
      /*  if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault()));
        }
        */
    }

}
