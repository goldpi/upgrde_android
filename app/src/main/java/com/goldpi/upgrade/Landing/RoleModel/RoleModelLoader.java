package com.goldpi.upgrade.Landing.RoleModel;

import com.android.volley.RequestQueue;
import com.goldpi.upgrade.Network.HTTPJSONRequestor;
import com.goldpi.upgrade.Parser.RoleModelParser;
import com.goldpi.upgrade.UpgradeApplication;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static com.goldpi.upgrade.Network.EndPointURL.APP_ROLEMODEL_URL;

/**
 * Created by Digambar on 5/20/2016.
 */
public class RoleModelLoader {
    final static Logger logger = LoggerFactory.getLogger(RoleModelModel.class);
    //Load News
    //
    public static ArrayList<RoleModelModel> loadRoleModel(RequestQueue requestQueue) {
        JSONArray response = HTTPJSONRequestor.requestJSON(requestQueue, APP_ROLEMODEL_URL);
        logger.info("Load News", response);
        ArrayList<RoleModelModel> listRoleModel = RoleModelParser.parseRoleModelJSON(response);
       // RoleModelModel roleModelModel=RoleModelParser.parseRoleModelJSON(response)
        if(listRoleModel.size()>0)

            //need to be craete sql statement od insert value
           UpgradeApplication.getWritableDatabase().insertRoleModel(listRoleModel, true);
        else
        //need to be create read statement for a single value
            listRoleModel= UpgradeApplication.getWritableDatabase().readRoleModel();
        return listRoleModel;
    }
}
