package com.goldpi.upgrade.Landing.Calender;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import com.github.tibolte.agendacalendarview.models.BaseCalendarEvent;
import com.github.tibolte.agendacalendarview.models.CalendarEvent;
import com.goldpi.materialui.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Yusuf on 4/9/2016.
 */
public class AgendaViewCalenderEventCollection {

    private final Context contex;

    public AgendaViewCalenderEventCollection(Context mContext) {

        contex=mContext;

    }

    public  List<CalendarEvent> FuncEventList()
    {
        List<CalendarEvent> EventList =new ArrayList();
        Calendar startTime1 = Calendar.getInstance();
        Calendar endTime1 = Calendar.getInstance();
        endTime1.add(Calendar.MONTH, 1);
        BaseCalendarEvent event1 = new BaseCalendarEvent("Thibault travels in Iceland", "A wonderful journey!", "Iceland",
                ContextCompat.getColor(contex, R.color.colorPrimaryDark), startTime1, endTime1, false);
        EventList.add(event1);

        Calendar startTime2 = Calendar.getInstance();
        startTime2.add(Calendar.DAY_OF_YEAR, 1);
        Calendar endTime2 = Calendar.getInstance();
        endTime2.add(Calendar.DAY_OF_YEAR, 3);
        BaseCalendarEvent event2 = new BaseCalendarEvent("Visit to Dalvík", "A beautiful small town", "Dalvík",
                ContextCompat.getColor(contex, R.color.colorPrimary), startTime2, endTime2, true);
        EventList.add(event2);

        return EventList;
    }
}



/**
     * Initializes the event
     *
     * @param id          The id of the event.
     * @param color       The color of the event.
     * @param title       The title of the event.
     * @param description The description of the event.
     * @param location    The location of the event.
     * @param dateStart   The start date of the event.
     * @param dateEnd     The end date of the event.
     * @param allDay      Int that can be equal to 0 or 1.
     * @param duration    The duration of the event in RFC2445 format.
     */
/*public BaseCalendarEvent(long id, int color, String title, String description, String location, long dateStart, long dateEnd, int allDay, String duration) {
    this.mId = id;
    this.mColor = color;
    this.mAllDay = (allDay == 1) ? true : false;
    this.mDuration = duration;
    this.mTitle = title;
    this.mDescription = description;
    this.mLocation = location;

    this.mStartTime = Calendar.getInstance();
    this.mStartTime.setTimeInMillis(dateStart);
    this.mEndTime = Calendar.getInstance();
    this.mEndTime.setTimeInMillis(dateEnd);
}
 */