package com.goldpi.upgrade.Landing.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.goldpi.materialui.R;
import com.goldpi.upgrade.Landing.Products.ProductModal;
import com.goldpi.upgrade.Landing.Products.ProductsAdapter;
import com.goldpi.upgrade.Listner.ILoadedProductsListener;
import com.goldpi.upgrade.Task.TaskLoadProducts;
import com.goldpi.upgrade.UIHelper.DividerItemDecoration;
import com.goldpi.upgrade.UpgradeApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/26/2016.
 */
public class ProductListActivity extends AppCompatActivity implements ILoadedProductsListener {
    private static final String PRODUCT = "product";
    final Logger logger = LoggerFactory.getLogger(ProductListActivity.class);
    private ArrayList<ProductModal> mlistproduct = new ArrayList<>();
    private RecyclerView mrecyclerView;
    private ProductsAdapter mpeoductAdapter;

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_productlist);
        mrecyclerView = (RecyclerView) findViewById(R.id.recycler_product);
        mpeoductAdapter = new ProductsAdapter(getApplicationContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mrecyclerView.setLayoutManager(mLayoutManager);
        mrecyclerView.setAdapter(mpeoductAdapter);
        if (savedInstance != null) {
            logger.info("Loading Product  from Saved Instance State");

            //if this fragment starts after a rotation or configuration change, load the existing news from a parcelable class
            mlistproduct = savedInstance.getParcelableArrayList(PRODUCT);

        } else
        {
            int CategoryID = Integer.parseInt(getIntent().getExtras().getString("CategoryID"));
            mlistproduct = UpgradeApplication.getWritableDatabase().readProductbyCategory(CategoryID);
        }
        //update your Adapter to containg the retrieved news
        mpeoductAdapter.setProducts(mlistproduct);

        //endregion


        //return view;

    }

    @Override
    public void onProductLoaded(ArrayList<ProductModal> listproduct) {
        mpeoductAdapter.setProducts(listproduct);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //save the movie list to a parcelable prior to rotation or configuration change
        outState.putParcelableArrayList(PRODUCT, mlistproduct);
    }
}
