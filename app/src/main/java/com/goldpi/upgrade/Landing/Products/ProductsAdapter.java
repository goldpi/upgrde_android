package com.goldpi.upgrade.Landing.Products;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Landing.Activity.ProductDetailActivity;
import com.goldpi.upgrade.Network.VolleySingleton;
import com.goldpi.upgrade.Utility.IConstants;
import com.goldpi.upgrade.anim.AnimationUtils;

import org.slf4j.LoggerFactory;

import java.util.ArrayList;


public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.MyViewHolder> {
    final static org.slf4j.Logger logger = LoggerFactory.getLogger(ProductsAdapter.class);
    private ArrayList<ProductModal> dataSet;
    private LayoutInflater inflator;
    private View view;
    private int mPreviousPosition = 0;
    private VolleySingleton mVolleySingleton;
    private ImageLoader mImageLoader;

    public ProductsAdapter(Context context) {
        mVolleySingleton = VolleySingleton.getInstance();
        mImageLoader = mVolleySingleton.getImageLoader();
        inflator = LayoutInflater.from(context);
    }

    public void setProducts(ArrayList<ProductModal> data) {
        this.dataSet = data;
        //update the adapter to reflect the Product Category set of Product Category
        notifyDataSetChanged();
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        view = inflator.inflate(R.layout.offlineprocuctsitem, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int Position) {
        holder.txtProjectId.setText(Integer.toString(dataSet.get(Position).getId()));
        holder.txtProductName.setText(dataSet.get(Position).getName());
        holder.txtProductPrice.setText("₹ " + Integer.toString(dataSet.get(Position).getPrice().intValue()) + "/-");
        holder.imgProductImage.setImageResource(R.drawable.img_profile_cover);
        holder.txtShortDesc.setText(dataSet.get(Position).getShortDesc());
        holder.txtCategoryId.setText(Integer.toString(dataSet.get(Position).getCategoryId()));
        String ThumbURL = dataSet.get(Position).getImageUrl();
        loadImages(ThumbURL, holder);


        if (Position > mPreviousPosition) {
            AnimationUtils.animateScatter(holder, true);
        } else {
            AnimationUtils.animateScatter(holder, false);
        }
        mPreviousPosition = Position;

    }

    private void loadImages(String urlThumbnail, final MyViewHolder holder) {
        if (!urlThumbnail.equals(IConstants.NA)) {
            mImageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.imgProductImage.setImageBitmap(response.getBitmap());
                    logger.info("Loading Image:-" + response.getRequestUrl());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.error(error.toString());
                }


            });
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgProductImage;
        TextView txtProductName;
        TextView txtProductPrice;
        TextView txtCategoryId;
        TextView txtShortDesc;
        TextView txtProjectId;


        private Context mcontex;

        public MyViewHolder(View itemView) {
            super(itemView);
            mcontex = itemView.getContext();
            this.txtProjectId = (TextView) itemView.findViewById(R.id.ProjectId);
            this.txtProductName = (TextView) itemView.findViewById(R.id.txtFrgmFourProductName);
            this.txtProductPrice = (TextView) itemView.findViewById(R.id.txtFrgmFourProductPrice);
            this.imgProductImage = (ImageView) itemView.findViewById(R.id.imgFrgmFourProductImage);
            this.txtShortDesc = (TextView) itemView.findViewById(R.id.txtFrgmFourShortDesc);
            this.txtCategoryId = (TextView) itemView.findViewById(R.id.CategoryId);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            TextView txtProjectId = (TextView) v.findViewById(R.id.ProjectId);
            Intent m = new Intent(mcontex, ProductDetailActivity.class);
            m.putExtra("ProjectId", txtProjectId.getText());
            m.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mcontex.startActivity(m);
        }
    }
}

