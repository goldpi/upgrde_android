package com.goldpi.upgrade.Landing.RoleModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.goldpi.upgrade.Network.VolleySingleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Created by Digambar on 5/20/2016.
 */
public class RoleModelModel  implements Parcelable {
    final static Logger logger = LoggerFactory.getLogger(RoleModelModel.class);
    private int Id;
    private String Title;
    private String ImageUrl;
    private String VideoUrl;
    private String Content;
    private Date PostedOn;

    public RoleModelModel()
    {

    }
    public RoleModelModel(Parcel input)
    {
        Id=input.readInt();
        Title=input.readString();
        ImageUrl=input.readString();
        VideoUrl=input.readString();
        Content=input.readString();
        PostedOn=new Date(input.readLong());
    }
    public RoleModelModel(int id,String Title,String ImageUrl,String VideoUrl,String Content,Date PostedOn)
    {
        this.Id=id;
        this.Title=Title;
        this.ImageUrl=ImageUrl;
        this.VideoUrl=VideoUrl;
        this.Content=Content;
        this.PostedOn=PostedOn;

    }

    public static final Creator<RoleModelModel> CREATOR = new Creator<RoleModelModel>() {
        @Override
        public RoleModelModel createFromParcel(Parcel in) {
            return new RoleModelModel(in);
        }

        @Override
        public RoleModelModel[] newArray(int size) {
            logger.error("create from parcel :Movie");
            return new RoleModelModel[size];
        }
    };

    public int getId() {
        return Id;
    }
    public String getTitle() {
        return Title;
    }
    public String getVideoUrl() {
        return VideoUrl;
    }
    public String getImageUrl() {
        return ImageUrl;
    }
    public String getContent() {
        return Content;
    }
    public Date getPostedOn() {
        return PostedOn;
    }
    public void setId(int id) {
        Id = id;
    }
    public void setTitle(String title) {
        Title = title;
    }
    public void setImageUrl(String Thumb) {
        this.ImageUrl=Thumb;
    }
    public void setVideoUrl(String videoUrl) {
        VideoUrl = videoUrl;
    }
    public void setPostedOn(Date on) {
        PostedOn = on;
    }
    public void setContent(String content) {
        Content = content;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Title);
        dest.writeString(ImageUrl);
        dest.writeString(VideoUrl);
        dest.writeString(Content);
        dest.writeLong(PostedOn == null ? -1 : PostedOn.getTime());



    }
}
