package com.goldpi.upgrade.Landing.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Landing.HomeSlider.Slider;
import com.goldpi.upgrade.Landing.ProductCategory.ProductCategoryAdapter;
import com.goldpi.upgrade.Landing.ProductCategory.ProductCategoryModel;
import com.goldpi.upgrade.Listner.ILoadedProductCategoryListener;
import com.goldpi.upgrade.Task.TaskLoadProductCategory;
import com.goldpi.upgrade.UIHelper.DividerItemDecoration;
import com.goldpi.upgrade.UpgradeApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import static com.goldpi.upgrade.Landing.HomeSlider.SliderName.SliderName_AndroidProductCategory;

/**
 * A simple {@link Fragment} subclass.
 */
public class FiveFragment extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener, ILoadedProductCategoryListener {

    // Logget used
    final Logger logger = LoggerFactory.getLogger(OneFragment.class);
    private View view;
    private ProductCategoryAdapter mProductAdapter;
    private RecyclerView recyclerViewProduct;
    private SliderLayout mSlider;
    private static final String PRODUCT_CATEGORY = "product_category";
    private ArrayList<ProductCategoryModel> mproductList = new ArrayList<>();

    public FiveFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_five, container, false);
        //region=============slider===============
        mSlider = (SliderLayout) view.findViewById(R.id.slider);
        Slider.Start(mSlider, this, SliderName_AndroidProductCategory);
        //endregion
        //Product Category Start
        recyclerViewProduct = (RecyclerView) view.findViewById(R.id.rcyViewFrgmFiveProductContainer);
        //Log.d("Yusuf", recyclerViewProduct.toString());
        mProductAdapter = new ProductCategoryAdapter(getActivity());
        recyclerViewProduct.setAdapter(mProductAdapter);
        recyclerViewProduct.addItemDecoration(
                new DividerItemDecoration(getActivity(), null));
        recyclerViewProduct.setLayoutManager(new LinearLayoutManager(getActivity()));


        if (savedInstanceState != null) {
            logger.info("Loading Product Category from Saved Instance State");

            //if this fragment starts after a rotation or configuration change, load the existing news from a parcelable class
            // mproductList = savedInstanceState.getParcelableArrayList(PRODUCT_CATEGORY);
            mproductList = savedInstanceState.getParcelableArrayList(PRODUCT_CATEGORY);
            if (mproductList.isEmpty()) {
                //load Product Category from web server
                new TaskLoadProductCategory(this).execute();
            }

        } else {
            //if this fragment starts for the first time, load the list of Product Category from the database
            mproductList = UpgradeApplication.getWritableDatabase().readProductCategory();
            //if the database is empty, trigger an AsycnTask to download Product Category list from the web
            if (mproductList.isEmpty()) {
                //load Product Category from web server
                new TaskLoadProductCategory(this).execute();
            }
        }
        //update your Adapter to containg the retrieved news
        mProductAdapter.setProductCategory(mproductList);

        //endregion


        return view;
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onProductCategoryLoaded(ArrayList<ProductCategoryModel> listproductCategory) {
        mProductAdapter.setProductCategory(listproductCategory);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //save the movie list to a parcelable prior to rotation or configuration change
        outState.putParcelableArrayList(PRODUCT_CATEGORY, mproductList);
    }
}
