package com.goldpi.upgrade.Landing.ProductCategory;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Landing.Activity.ProductListActivity;
import com.goldpi.upgrade.Network.VolleySingleton;
import com.goldpi.upgrade.Utility.IConstants;
import com.goldpi.upgrade.anim.AnimationUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by Digambar on 5/23/2016.
 */
public class ProductCategoryAdapter extends RecyclerView.Adapter<ProductCategoryAdapter.MyViewHolder> {
    final static Logger logger = LoggerFactory.getLogger(ProductCategoryAdapter.class);
    private static List<ProductCategoryModel> productCategoryModels;
    private LayoutInflater layoutInflater;
    private View view;
    private int mPreviousPosition = 0;
    private VolleySingleton mVolleySingleton;
    private ImageLoader mImageLoader;

    public ProductCategoryAdapter(Context context) {
        mVolleySingleton = VolleySingleton.getInstance();
        mImageLoader = mVolleySingleton.getImageLoader();
        layoutInflater = LayoutInflater.from(context);
    }

    public void setProductCategory(List<ProductCategoryModel> data) {
        this.productCategoryModels = data;
        //update the adapter to reflect the Product Category set of Product Category
        notifyDataSetChanged();
    }

    @Override
    public ProductCategoryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = layoutInflater.inflate(R.layout.landingproductcategory, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(ProductCategoryAdapter.MyViewHolder holder, int Position) {
        holder.txtProductCategoryId.setText(Integer.toString(productCategoryModels.get(Position).getId()));
        holder.txtProductCategoryTitle.setText(productCategoryModels.get(Position).getTitle());
        holder.txtProductCategoryDesp.setText(productCategoryModels.get(Position).getDescripton());
        String ThumbURL = productCategoryModels.get(Position).getImage();
        loadImages(ThumbURL, holder);
        // holder.txtProductCategoryTitle.setText(productCategoryModels.get(Position).getTitle());
        //holder.txtProductCategoryShortDecp.setText("Short Product Category Description");
        // holder.txtProductCategoryDesp.setText("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever");
        if (Position > mPreviousPosition) {
            AnimationUtils.animateScatter(holder, true);
        } else {
            AnimationUtils.animateScatter(holder, false);
        }
        mPreviousPosition = Position;

    }

    private void loadImages(String urlThumbnail, final MyViewHolder holder) {
        if (!urlThumbnail.equals(IConstants.NA)) {
            mImageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.imgProductCategoryImage.setImageBitmap(response.getBitmap());
                    logger.info("Loading Image:-" + response.getRequestUrl());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.error(error.toString());
                }


            });
        }
    }

    @Override
    public int getItemCount() {
        return productCategoryModels.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imgProductCategoryImage;
        TextView txtProductCategoryTitle;
        TextView txtProductCategoryDesp;
        TextView txtProductCategoryId;
        Context mPCcontext;

        public MyViewHolder(View PCitemView) {
            super(PCitemView);

            this.txtProductCategoryId = (TextView) PCitemView.findViewById(R.id.txtproductcategoryId);
            this.imgProductCategoryImage = (ImageView) PCitemView.findViewById(R.id.imgfragmentProductCategoryImage);
            this.txtProductCategoryTitle = (TextView) PCitemView.findViewById(R.id.txtfragmentProductCategoryTitle);
            //this.txtProductCategoryShortDecp=(TextView)itemView.findViewById(R.id.txtfragmentoneProductCategoryShortDescription);
            this.txtProductCategoryDesp = (TextView) PCitemView.findViewById(R.id.txtfragmentoneProductCategoryDescription);
            mPCcontext = PCitemView.getContext();
            PCitemView.setOnClickListener(this);
            logger.info("Click event bind");

        }

        @Override
        public void onClick(View v) {
            logger.info("Item clicked");
            TextView txtProductCategoryId = (TextView) v.findViewById(R.id.txtproductcategoryId);
            Intent ProductList = new Intent(mPCcontext, ProductListActivity.class);
            ProductList.putExtra("CategoryID", txtProductCategoryId.getText());
            mPCcontext.startActivity(ProductList);


        }
    }
}
