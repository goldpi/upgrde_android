package com.goldpi.upgrade.Landing.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.goldpi.upgrade.KDC.VideoListDemoActivity;
import com.goldpi.upgrade.Landing.Activity.YouTubeActivity;
import com.goldpi.upgrade.Landing.HomeSlider.Slider;
import com.goldpi.upgrade.Landing.Activity.ProductDetailActivity;

import com.goldpi.materialui.R;
import com.goldpi.upgrade.Landing.Activity.popexpert;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

import static com.goldpi.upgrade.Landing.HomeSlider.SliderName.SliderName_AndroidDashboard;

/**
 * A simple {@link Fragment} subclass.
 */
public class OneFragment extends Fragment implements View.OnClickListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    SliderLayout mSlider;
    private TextView tv;
    private Button btn2;
    private View btn;
    @Bind(R.id.linkBtnContainer)
    LinearLayout linkBtnContainer;
    final Logger logger = LoggerFactory.getLogger(OneFragment.class);

    public OneFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_one, container, false);
        ButterKnife.bind(this, view);
        //==================Image Slider=====================
        mSlider = (SliderLayout) view.findViewById(R.id.slider);
        Slider.Start(mSlider, this, SliderName_AndroidDashboard);
        //==================Image Slider END=====================

       /*
      ArrayList<SlicesModel> cr= shortList();
        for(SlicesModel s:cr){
            Log.i("t",""+s.getTitle()+"-"+s.getDescription()+"--"+s.getSlidingOrder());
            //Log.i("t",""+s.getSlidingOrder());
        }*/
        new MaterialShowcaseView.Builder(getActivity())
                .setTarget(linkBtnContainer)
                .setDismissText("GOT IT")
                .setContentText("This is dashboard")
                .setDelay(300) // optional but starting animations immediately in onCreate can make them choppy
                        //.singleUse("z") // provide a unique ID used to ensure it is only shown once
                .show();
        return view;

    }

    /*
public  ArrayList<SlicesModel> shortList()
{
    SlidingSorter slide=new SlidingSorter();
    ArrayList<SlicesModel> SliceList=new ArrayList<>();
    for (int i=0;i<=10;i++)
    {
        int randommval=i;
        if(i==2)
        {
            randommval=20;
        }
        else if(i==7)
        {
            randommval=12;
        }
        SlicesModel NewSlice=new SlicesModel("Title "+i,"Description "+i, "Imgage URL "+i,randommval);
        SliceList.add(NewSlice);
    }
    slide.SortSlideBySliderOrder(SliceList);
    return SliceList;

    }
*/

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return super.onCreateAnimation(transit, enter, nextAnim);
    }

    @OnClick({R.id.btn_kdc,R.id.btn_connected_school,R.id.btn_brain_breakfast})
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.button1: {
                Intent i = new Intent(getActivity(), popexpert.class);
                i.putExtra("id", "mxmndnmnfmnfndnd");
                startActivity(i);
                break;
            }
            case R.id.button2: {
                Intent i = new Intent(getActivity(), ProductDetailActivity.class);
                i.putExtra("id", "mxmndnmnfmnfndnd");
                startActivity(i);
                break;
            }
            case R.id.btn_kdc:
            {
                Intent i=new Intent(getActivity(), VideoListDemoActivity.class);
                i.putExtra("id","mxmndnmnfmnfndnd");
                startActivity(i);
                break;
            }
            case R.id.btn_brain_breakfast:
            {
                Intent i=new Intent(getActivity(), YouTubeActivity.class);
                i.putExtra("id","mxmndnmnfmnfndnd");
                startActivity(i);
                break;
            }
            default:
            {

          
                break;
            }
        }


    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}