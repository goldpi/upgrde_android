package com.goldpi.upgrade.Landing.ProductCategory;

import android.os.Parcel;
import android.os.Parcelable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Digambar on 5/23/2016.
 */
public class ProductCategoryModel implements Parcelable{
    final static Logger logger = LoggerFactory.getLogger(ProductCategoryModel.class);
    private int Id;
    private String Title;
    private String Descripton;
    private String ShortDescription;
    private String image;
    public ProductCategoryModel()
    {

    }

    protected ProductCategoryModel(Parcel in) {
        Id = in.readInt();
        Title = in.readString();
        Descripton = in.readString();
        ShortDescription = in.readString();
        image = in.readString();
    }


    public static final Creator<ProductCategoryModel> CREATOR = new Creator<ProductCategoryModel>() {
        @Override
        public ProductCategoryModel createFromParcel(Parcel in) {
            return new ProductCategoryModel(in);
        }

        @Override
        public ProductCategoryModel[] newArray(int size) {
            return new ProductCategoryModel[size];
        }
    };

    public int getId() {
        return Id;
    }

    public String getDescripton() {
        return Descripton;
    }

    public String getShortDescription() {
        return ShortDescription;
    }

    public String getTitle() {
        return Title;
    }

    public String getImage() {
        return image;
    }

    public void setId(int id) {
        Id = id;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setDescripton(String descripton) {
        Descripton = descripton;
    }

    public void setShortDescription(String shortDescription) {
        ShortDescription = shortDescription;
    }

    public void setImage(String image) {
        this.image = image;
    }
    public  static List<ProductCategoryModel> getProductCategoryList()
    {
        List<ProductCategoryModel> listproductCategoryModel=new ArrayList<>();
        for(int i=1;i<=20;i++) {
            ProductCategoryModel productCategoryModel=new ProductCategoryModel();
            try {

                productCategoryModel.setTitle(" Title " + i);
                productCategoryModel.setId(i);
                productCategoryModel.setShortDescription(" Product Category short Decription");
                productCategoryModel.setDescripton("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever....\"");
                productCategoryModel.setImage("http://picture.upgrde.net/content/logo.png");
            }  catch (UnsupportedOperationException ex)
            {
                logger.error("Error", ex.getMessage(), ex);
            }
        }
            return listproductCategoryModel;

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(Id);
        dest.writeString(Title);
        dest.writeString(Descripton);
        dest.writeString(ShortDescription);
        dest.writeString(image);
    }
}
