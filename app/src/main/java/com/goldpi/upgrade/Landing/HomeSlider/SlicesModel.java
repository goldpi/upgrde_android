package com.goldpi.upgrade.Landing.HomeSlider;

public class SlicesModel {

    public String Title;
    public String Description;
    public String Image;
    public int SlidingOrder;

    public SlicesModel() {

    }

    public SlicesModel(String title, String description, String image, int slidingOrder) {
        this.Title = title;
        this.Description = description;
        this.Image = image;
        this.SlidingOrder = slidingOrder;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setImage(String image) {
        Image = image;
    }

    public void setSlidingOrder(int slidingOrder) {
        SlidingOrder = slidingOrder;
    }

    public String getTitle() {
        return Title;
    }

    public String getDescription() {
        return Description;
    }

    public String getImage() {
        return Image;
    }

    public int getSlidingOrder() {
        return SlidingOrder;
    }
}
