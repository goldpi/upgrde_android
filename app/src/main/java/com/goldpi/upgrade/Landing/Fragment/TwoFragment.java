package com.goldpi.upgrade.Landing.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.beardedhen.androidbootstrap.BootstrapProgressBar;
import com.daimajia.easing.Glider;
import com.daimajia.easing.Skill;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Icon.Iconicfy;
import com.goldpi.upgrade.Landing.HomeSlider.Slider;
import com.goldpi.upgrade.Landing.News.NewsAdapter;
import com.goldpi.upgrade.Landing.News.NewsModal;
import com.goldpi.upgrade.Landing.RoleModel.RoleModelModel;
import com.goldpi.upgrade.Listner.ILoadedNewsListener;
import com.goldpi.upgrade.Listner.ILoadedRoleModelListener;
import com.goldpi.upgrade.Network.VolleySingleton;
import com.goldpi.upgrade.Task.TaskLoadNews;
import com.goldpi.upgrade.Task.TaskLoadRoleModel;
import com.goldpi.upgrade.UIHelper.DividerItemDecoration;
import com.goldpi.upgrade.UpgradeApplication;
import com.goldpi.upgrade.Utility.IConstants;
import com.joanzapata.iconify.widget.IconTextView;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.goldpi.upgrade.Landing.HomeSlider.SliderName.SliderName_AndroidRoleModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class TwoFragment extends Fragment implements ILoadedNewsListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener,ILoadedRoleModelListener {


    //The key used to store arraylist of news objects to and from parcelable
    private static final String STATE_NEWS = "state_news";
    final Logger logger = LoggerFactory.getLogger(OneFragment.class);
    //=============================
    IconTextView buttonNext;
    @Bind(R.id.progressbarMyGoal) BootstrapProgressBar mMyGoalProgress;
    @Bind(R.id.imgRoleImage) ImageView imgRoleModel;
    @Bind(R.id.txtRoleModelDescription)TextView txtRoleModelDescription;
    private ViewFlipper viewSwitcher;
    private Animation slide_in_left,slide_out_right;

    //===============================
    SliderLayout mSlider;
    private View view;
    private TextView tv;
    private RecyclerView recyclerViewNews;
    private LinearLayoutManager layoutManager;
    private NewsAdapter mNewsAdapter;
    private Button btn2;
    private View btn;
    private ArrayList<NewsModal> mNewsList = new ArrayList<>();
    private RoleModelModel MyRoleModel;
    private VolleySingleton mVolleySingleton;
    private ImageLoader mImageLoader;
    public TwoFragment() {
        // Required empty public constructor
        mVolleySingleton = VolleySingleton.getInstance();
        mImageLoader = mVolleySingleton.getImageLoader();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_two, container, false);
        Iconicfy.SetIconify(); // to enable fa(fontawsome Icone)
        ButterKnife.bind(this, view);

        //region=============slider===============
        mSlider = (SliderLayout) view.findViewById(R.id.slider);
        Slider.Start(mSlider, this, SliderName_AndroidRoleModel);
        //endregion


        //region=============Goal==================
        buttonNext = (IconTextView) view.findViewById(R.id.link_switchGole_frgmOne);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewSwitcher.showNext();
            }
        });
        viewSwitcher = (ViewFlipper) view.findViewById(R.id.viewswitcherSetMyGole_frgmOne);

        slide_in_left = AnimationUtils.loadAnimation(view.getContext(),
                android.R.anim.slide_in_left);
        slide_out_right = AnimationUtils.loadAnimation(view.getContext(),
                android.R.anim.slide_out_right);
        viewSwitcher.setInAnimation(slide_in_left);
        viewSwitcher.setOutAnimation(slide_out_right);
        mMyGoalProgress.setProgress(90);

        //endregion


        //region==============NEWS===================
        recyclerViewNews = (RecyclerView) view.findViewById(R.id.recycler_view_FragmentOne_News);
        mNewsAdapter = new NewsAdapter(getActivity());
        recyclerViewNews.setAdapter(mNewsAdapter);
        recyclerViewNews.addItemDecoration(
                new DividerItemDecoration(getActivity(), R.drawable.itemdivider));
        recyclerViewNews.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (savedInstanceState != null) {
            logger.info("Loading News from Saved Instance State");

            //if this fragment starts after a rotation or configuration change, load the existing news from a parcelable class
            mNewsList = savedInstanceState.getParcelableArrayList(STATE_NEWS);
            if (mNewsList.isEmpty()) {
                //load news from web server
                new TaskLoadNews(this).execute();
            }

        } else {
           //if this fragment starts for the first time, load the list of news from the database
            mNewsList = UpgradeApplication.getWritableDatabase().readNews();
            //if the database is empty, trigger an AsycnTask to download movie list from the web
            if (mNewsList.isEmpty()) {
                //load news from web server
                new TaskLoadNews(this).execute();
            }
        }
        //update your Adapter to containg the retrieved news
        mNewsAdapter.setNews(mNewsList);

        //endregion
        setupRoleModel();
        return view;
    }





    public void setupRoleModel()
    {

        //if this fragment starts for the first time, load the list of news from the database
        MyRoleModel = UpgradeApplication.getWritableDatabase().readRoleModel().get(0);
        if(MyRoleModel!=null)
        {
            txtRoleModelDescription.setText(MyRoleModel.getContent());
            loadImages(MyRoleModel.getImageUrl());
        }
        else
        {
         new  TaskLoadRoleModel(this).execute();
        }

    }

    private void loadImages(String urlThumbnail) {
        if (!urlThumbnail.equals(IConstants.NA)) {
            mImageLoader.get(urlThumbnail, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                  imgRoleModel.setImageBitmap(response.getBitmap());
                    logger.info("Loading Image:-" + response.getRequestUrl());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    logger.error(error.toString());
                }


            });
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //save the movie list to a parcelable prior to rotation or configuration change
        outState.putParcelableArrayList(STATE_NEWS, mNewsList);
    }


    @Override
    public void onResume() {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                Glider.glide(Skill.BounceEaseIn, 1200, ObjectAnimator.ofFloat(view.findViewById(R.id.card_viewRoleModel), "translationY", 100, 0)),
                Glider.glide(Skill.BackEaseIn, 1200, ObjectAnimator.ofFloat(view.findViewById(R.id.card_viewFrgOneNewsHolder), "translationY", 2000, 0))
        );

        set.setDuration(800);
        set.start();
        super.onResume();
    }


    @Override
    public void onNewsLoaded(ArrayList<NewsModal> listNews) {
        mNewsAdapter.setNews(listNews);
    }

    //@OnClick(R.id.link_switchGole_frgmOne)
    public void onClick(View v) {
        viewSwitcher.showNext();
        int id = v.getId();
        switch (id) {
            case R.id.link_switchGole_frgmOne: {

                break;
            }
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onRoleModelLoaded(ArrayList<RoleModelModel> listRoleModel) {
        setupRoleModel();
    }
}
