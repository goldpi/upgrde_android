package com.goldpi.upgrade.Landing.Calender;

import android.graphics.RectF;

import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Yusuf on 4/7/2016.
 */
public class CalenderMonthChangeListner implements MonthLoader.MonthChangeListener {

  
    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        return Collections.emptyList();
    }
}

