package com.goldpi.upgrade.Listner;

import com.goldpi.upgrade.Landing.News.NewsModal;

import java.util.ArrayList;

/**
 * Created by Yusuf on 5/1/2016.
 */
public interface ILoadedNewsListener {
    public void onNewsLoaded(ArrayList<NewsModal> listNews);
}
