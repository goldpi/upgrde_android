package com.goldpi.upgrade.Listner;

import com.goldpi.upgrade.Landing.Articles.ArticleModal;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/11/2016.
 */
public interface ILoadedArticlesListener {
    public void onArticlesLoaded(ArrayList<ArticleModal> listArticles);
}
