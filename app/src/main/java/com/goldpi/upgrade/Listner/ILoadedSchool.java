package com.goldpi.upgrade.Listner;

import com.goldpi.upgrade.Account.Registration.School.SchoolModel;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/29/2016.
 */
public interface ILoadedSchool {
    public void onSchoolLoaded(ArrayList<SchoolModel> listschool);
}

