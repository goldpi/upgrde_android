package com.goldpi.upgrade.Listner;

import com.goldpi.upgrade.Landing.Products.ProductModal;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/20/2016.
 */
public interface  ILoadedProductsListener {
    public void onProductLoaded(ArrayList<ProductModal> listproduct);
}
