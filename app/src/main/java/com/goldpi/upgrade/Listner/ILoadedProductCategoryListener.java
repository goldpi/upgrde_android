package com.goldpi.upgrade.Listner;

import com.goldpi.upgrade.Landing.ProductCategory.ProductCategoryModel;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/23/2016.
 */
public interface ILoadedProductCategoryListener{
        public void onProductCategoryLoaded(ArrayList<ProductCategoryModel> listproductCategory);
}
