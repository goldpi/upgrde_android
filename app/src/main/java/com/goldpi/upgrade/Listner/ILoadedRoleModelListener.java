package com.goldpi.upgrade.Listner;

import com.goldpi.upgrade.Landing.RoleModel.RoleModelModel;

import java.util.ArrayList;

/**
 * Created by Digambar on 5/20/2016.
 */
public interface ILoadedRoleModelListener {
    public void onRoleModelLoaded(ArrayList<RoleModelModel> listRoleModel);
}
