package com.goldpi.upgrade.Listner;

import com.goldpi.materialui.R;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;

import java.util.Map;

/**
 * Created by Yusuf on 5/28/2016.
 */
public class ThumbnailListener implements
        YouTubeThumbnailView.OnInitializedListener,
        YouTubeThumbnailLoader.OnThumbnailLoadedListener {
    Map<YouTubeThumbnailView, YouTubeThumbnailLoader> thumbnailViewToLoaderMap;


    public ThumbnailListener(Map<YouTubeThumbnailView, YouTubeThumbnailLoader> _lcalthumbnailViewToLoaderMap) {
        thumbnailViewToLoaderMap=_lcalthumbnailViewToLoaderMap;
    }
    @Override
    public void onInitializationSuccess(
            YouTubeThumbnailView view, YouTubeThumbnailLoader loader) {
        loader.setOnThumbnailLoadedListener(this);
        thumbnailViewToLoaderMap.put(view, loader);
        view.setImageResource(R.drawable.loading_thumbnail);
        String videoId = (String) view.getTag();
        loader.setVideo(videoId);
    }

    @Override
    public void onInitializationFailure(
            YouTubeThumbnailView view, YouTubeInitializationResult loader) {
        view.setImageResource(R.drawable.no_thumbnail);
    }

    @Override
    public void onThumbnailLoaded(YouTubeThumbnailView view, String videoId) {
    }

    @Override
    public void onThumbnailError(YouTubeThumbnailView view, YouTubeThumbnailLoader.ErrorReason errorReason) {
        view.setImageResource(R.drawable.no_thumbnail);
    }
}


