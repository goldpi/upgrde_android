package com.goldpi.upgrade;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.goldpi.materialui.R;
import com.goldpi.upgrade.Icon.Iconicfy;
import com.goldpi.upgrade.Landing.HomeSlider.Slider;
import com.goldpi.upgrade.Landing.News.NewsModal;
import com.goldpi.upgrade.Listner.ILoadedNewsListener;
import com.goldpi.upgrade.Utility.Utils;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;


import java.util.ArrayList;

import butterknife.ButterKnife;

public class NavSliderActivity extends AppCompatActivity implements View.OnClickListener,ILoadedNewsListener, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{

    SliderLayout mSlider;
    CollapsingToolbarLayout collapsingToolbarLayout;
    Toolbar mtoolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_slider);
        Utils.FullScreen(this);
        Iconicfy.SetIconify();
        ButterKnife.bind(this);


        ButterKnife.bind(this);

        setToolbar();

       setupCollapsingToolbarLayout();
    }
    private void setupCollapsingToolbarLayout(){

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        if(collapsingToolbarLayout != null){
            collapsingToolbarLayout.setTitle(mtoolbar.getTitle());
            //collapsingToolbarLayout.setCollapsedTitleTextColor(0xED1C24);
            //collapsingToolbarLayout.setExpandedTitleColor(0xED1C24);
        }
    }

    public void setToolbar()
    {
        mtoolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mtoolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sign Up");

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(new IconDrawable(this, FontAwesomeIcons.fa_arrow_left)
                .colorRes(R.color.white)
                .actionBarSize());
        ab.setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public void onNewsLoaded(ArrayList<NewsModal> listNews) {

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }
}
