package com.goldpi.upgrade.Utility;

import android.app.Activity;
import android.os.Build;
import android.view.View;
import android.view.WindowManager;

import org.json.JSONObject;

/**
 * Created by Yusuf on 4/29/2016.
 */
public class Utils {
    public static boolean contains(JSONObject jsonObject, String key) {
        return jsonObject != null && jsonObject.has(key) && !jsonObject.isNull(key) ? true : false;
    }

    public static void FullScreen(Activity decor)
    {
        if (Build.VERSION.SDK_INT < 16) {
             decor.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                     WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else
        {
            View decorView = decor.getWindow().getDecorView();
            // Hide the status bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
            // Remember that you should never show the action bar if the
            // status bar is hidden, so hide that too if necessary.
            // ActionBar actionBar = getActionBar();
            //actionBar.hide();
        }
    }
}