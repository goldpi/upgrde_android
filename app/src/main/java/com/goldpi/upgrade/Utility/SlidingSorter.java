package com.goldpi.upgrade.Utility;

import com.goldpi.upgrade.Landing.HomeSlider.SlicesModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Digambar on 5/25/2016.
 */
public class SlidingSorter
{
    public void SortSlideBySliderOrder(ArrayList<SlicesModel> slicesModels)
    {
        Collections.sort(slicesModels, new Comparator<SlicesModel>() {
            @Override
            public int compare(SlicesModel lhs, SlicesModel rhs) {
                int SlidingOrderLhs=lhs.getSlidingOrder();
                int SlidingOrderrhs=rhs.getSlidingOrder();
                if(SlidingOrderLhs>SlidingOrderrhs)
                {
                    return 1;

                }
                else if(SlidingOrderLhs<SlidingOrderrhs)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }


        });



    }
}
